﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEditor;
using UnityEngine;

namespace Pluliter.Editor.Config
{
    /// <summary>
    /// Pluliter配置编辑器
    /// </summary>
    [CustomEditor(typeof(PluliterConfig))]
    public class PluliterConfigEditor : UnityEditor.Editor
    {
        // 添加自定义图标
        //public override Texture2D RenderStaticPreview(string assetPath, Object[] subAssets, int width, int height)
        //{
        //    PluliterConfig config = (PluliterConfig)target;

        //    if (config == null || config.PreviewIcon == null)
        //        return null;

        //    Texture2D tex = new Texture2D(width, height);
        //    EditorUtility.CopySerialized(config.PreviewIcon, tex);

        //    return tex;
        //}
        public override void OnInspectorGUI()
        {
            //serializedObject.Update();
            //EditorGUILayout.HelpBox("请勿删除此文件，否则会导致Pluliter无法正常工作", MessageType.Warning);
            //EditorGUILayout.HelpBox("请在项目设置修改配置文件", MessageType.Warning);
            
            //// 禁止修改
            //GUI.enabled = false;
            //base.OnInspectorGUI();
            //serializedObject.ApplyModifiedProperties();

        }


        [InitializeOnLoadMethod]
        static void Create()
        {
            EditorApplication.delayCall += () =>
            {
                var config = AssetDatabase.LoadAssetAtPath<PluliterConfig>("Assets/Resources/PluliterConfig.asset");
                if (config == null)
                {
                    // 如果没有创建Pluliter文件夹
                    if (!AssetDatabase.IsValidFolder("Assets/Resources"))
                    {
                        AssetDatabase.CreateFolder("Assets", "Resources");
                    }

                    config = ScriptableObject.CreateInstance<PluliterConfig>();
                    AssetDatabase.CreateAsset(config, "Assets/Resources/PluliterConfig.asset");
                    AssetDatabase.Refresh();
                }
            };
        }
    }
}
