﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEditor;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using Pluliter.Dialogue;
using Pluliter.Character;
using System;
using Pluliter.Background;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 对话节点编辑器窗口
    /// </summary>
    public class DialogueNodeEditorWindow : EditorWindow
    {
        //[SerializeField]
        //private VisualTreeAsset visualTreeAsset = default;

        /// <summary>
        /// 对话章节
        /// </summary>
        private ObjectField dialogueChapterField = null;

        /// <summary>
        /// 对话章节
        /// </summary>
        private DialogueChapter dialogueChapter = null;

        /// <summary>
        /// 节点树查看器
        /// </summary>
        private DialogueNodeTreeViewer nodeTreeViewer = null;

        /// <summary>
        /// 节点检查器
        /// </summary>
        private DialogueNodeInspector nodeInspector = null;

        [MenuItem(PluliterEditorGlobal.RootMenuName + "对话节点编辑器 %g", false, 10)]
        public static void ShowEditorWindow()
        {
            DialogueNodeEditorWindow window = GetWindow<DialogueNodeEditorWindow>();
            window.titleContent = new GUIContent("对话节点编辑器");
            window.minSize = new Vector2(800, 600);
            float screenWidth = Screen.width;
            float screenHeight = Screen.height;
            window.position = new Rect((screenWidth - 800) / 2, (screenHeight - 600) / 2, 800, 600);
        }

        /// <summary>
        /// 打开对话节点编辑器窗口
        /// </summary>
        /// <param name="dialogueChapter">对话章节</param>
        public static void Open(DialogueChapter dialogueChapter)
        {
            DialogueNodeEditorWindow window = GetWindow<DialogueNodeEditorWindow>();
            window.titleContent = new GUIContent("对话节点编辑器");
            window.minSize = new Vector2(800, 600);
            window.dialogueChapter = dialogueChapter;
            window.dialogueChapterField.value = dialogueChapter;
            // window.titleContent.image = 还没设计图标
            window.Show();
        }

        public void CreateGUI()
        {
            //VisualElement root = rootVisualElement;

            //string visualTreePath = "Packages/com.zhanlehall.pluliter/Editor/NodeEditor/DialogueNodeEditorWindow/DialogueNodeEditorWindow.uxml";
            //var nodeTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(visualTreePath);
            //nodeTree.CloneTree(root);

            //nodeTreeViewer = root.Q<DialogueNodeTreeViewer>();
            //if (nodeTreeViewer != null)
            //{
            //    nodeTreeViewer.OnDialogueNodeSelected = OnDialogueNodeSelected;
            //    nodeTreeViewer.OnDialogueNodeUnselected = OnDialogueNodeUnselected;
            //}
            //else
            //{
            //    Debug.LogError("初始化错误");
            //}

            //nodeInspector = root.Q<DialogueNodeInspector>();

            //dialogueChapterField = root.Q<ObjectField>("DialogueChapterField");
            //if (dialogueChapterField != null)
            //{
            //    dialogueChapterField.objectType = typeof(DialogueChapter);
            //    dialogueChapterField.RegisterValueChangedCallback(OnDialogueChapterChanged);
            //}

            // 此处不使用visualTree.Instantiate() 为了保证行为树的单例防止重复实例化，以及需要将此root作为传参实时更新编辑器状态
            //// Instantiate UXML
            //VisualElement labelFromUXML = visualTreeAsset.Instantiate();
            //root.Add(labelFromUXML);

            VisualElement root = rootVisualElement;

            // 创建根容器
            VisualElement rootContainer = new VisualElement();
            rootContainer.style.flexGrow = 1;
            rootContainer.style.flexDirection = FlexDirection.Column;
            root.Add(rootContainer);

            // 创建菜单栏
            CreateMenuSection(rootContainer);

            // 创建编辑区域
            CreateEditorSection(rootContainer);

            // 初始化回调
            InitializeCallbacks();
        }

        private void CreateMenuSection(VisualElement parent)
        {
            // 菜单容器
            VisualElement menu = new VisualElement();
            menu.style.flexGrow = 0.01f;
            parent.Add(menu);

            // 横向布局
            VisualElement menuRow = new VisualElement();
            menuRow.style.flexGrow = 1;
            menuRow.style.flexDirection = FlexDirection.Row;
            menuRow.style.height = 30;
            menu.Add(menuRow);

            // 标题标签
            Label titleLabel = new Label("多文对话节点编辑器");
            titleLabel.style.unityFontStyleAndWeight = FontStyle.Bold;
            titleLabel.style.justifyContent = Justify.FlexStart;
            titleLabel.style.alignSelf = Align.Center;
            titleLabel.style.marginLeft = 15;
            titleLabel.style.marginRight = 15;
            titleLabel.style.fontSize = 16;
            menuRow.Add(titleLabel);

            Label chapterLabel = new Label("章节：");
            chapterLabel.style.justifyContent = Justify.FlexStart;
            chapterLabel.style.alignSelf = Align.Center;
            chapterLabel.style.marginLeft = 25;
            chapterLabel.style.fontSize = 15;
            menuRow.Add(chapterLabel);

            // 对话章节选择字段
            dialogueChapterField = new ObjectField
            {
                tooltip = "选择对话章节",
                name = "DialogueChapterField",
                objectType = typeof(DialogueChapter)
            };
            dialogueChapterField.style.width = 350;
            dialogueChapterField.style.marginTop = 7;
            dialogueChapterField.style.marginBottom = 7;
            dialogueChapterField.style.marginLeft = 15;
            dialogueChapterField.style.marginRight = 15;
            menuRow.Add(dialogueChapterField);
        }

        private void CreateEditorSection(VisualElement parent)
        {
            // 编辑区域容器
            VisualElement editor = new VisualElement();
            editor.style.flexGrow = 1;
            editor.style.flexDirection = FlexDirection.Row;
            parent.Add(editor);

            // 左侧节点树视图
            VisualElement leftPanel = new VisualElement();
            leftPanel.style.flexGrow = 3.5f;
            editor.Add(leftPanel);

            nodeTreeViewer = new DialogueNodeTreeViewer();
            nodeTreeViewer.style.flexGrow = 1;
            leftPanel.Add(nodeTreeViewer);

            // 右侧检视面板
            VisualElement rightPanel = new VisualElement();
            rightPanel.style.maxWidth = 400;
            rightPanel.style.flexGrow = 1f;
            editor.Add(rightPanel);

            nodeInspector = new DialogueNodeInspector();
            nodeInspector.style.flexGrow = 1;
            rightPanel.Add(nodeInspector);
        }

        private void InitializeCallbacks()
        {
            // 节点选择回调
            if (nodeTreeViewer != null)
            {
                nodeTreeViewer.OnDialogueNodeSelected = OnDialogueNodeSelected;
                nodeTreeViewer.OnDialogueNodeUnselected = OnDialogueNodeUnselected;
            }
            else
            {
                Debug.LogError("DialogueNodeTreeViewer 初始化失败");
            }

            // 对话章节变更回调
            if (dialogueChapterField != null)
            {
                dialogueChapterField.RegisterValueChangedCallback(OnDialogueChapterChanged);
            }
            else
            {
                Debug.LogError("DialogueChapterField 初始化失败");
            }
        }


        /// <summary>
        /// 当对话节点取消选中时调用此方法
        /// </summary>
        /// <param name="node1"></param>
        /// <param name="node2"></param>
        private void OnDialogueNodeUnselected(DialogueNode node1, Node node2)
        {
            nodeInspector?.SetDialogueNode(null, null);
        }

        /// <summary>
        /// 当对话节点被选中时调用此方法
        /// </summary>
        /// <param name="node"></param>
        /// <param name="nodeView"></param>
        private void OnDialogueNodeSelected(DialogueNode node, Node nodeView)
        {
            nodeInspector?.SetDialogueNode(node, nodeView, dialogueChapter.characterTable, dialogueChapter.backgroundTable, dialogueChapter.BgmTable, dialogueChapter.soundEffectTable, dialogueChapter.owner);
        }

        /// <summary>
        /// 当对话章节改变时调用此方法
        /// </summary>
        /// <param name="evt"></param>
        private void OnDialogueChapterChanged(ChangeEvent<UnityEngine.Object> evt)
        {
            if (evt.newValue != null)
            {
                dialogueChapter = evt.newValue as DialogueChapter;
                nodeTreeViewer.SetDialogueChapter(evt.newValue as DialogueChapter);
            }
            if (evt.newValue == null)
            {
                dialogueChapter = null;
                nodeTreeViewer.SetDialogueChapter(null);
            }
        }
    }
}
