﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Character;
using Pluliter.Dialogue;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;


namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 节点树节点网格视图
    /// </summary>
#if UNITY_2023
    [UxmlElement]
#endif
    public partial class DialogueNodeTreeViewer : GraphView
    {
#if UNITY_2022
        public new class UxmlFactory : UxmlFactory<DialogueNodeTreeViewer, UxmlTraits> { }
#endif
        /// <summary>
        /// 鼠标位置
        /// </summary>
        private Vector2 mousePosition = Vector2.zero;

        /// <summary>
        /// 当对话节点被选中时触发
        /// </summary>
        public Action<DialogueNode, Node> OnDialogueNodeSelected = null;

        /// <summary>
        /// 当对话节点被取消选中时触发
        /// </summary>
        public Action<DialogueNode, Node> OnDialogueNodeUnselected = null;

        /// <summary>
        /// 当前对话章节
        /// </summary>
        private DialogueChapter dialogueChapter = null;

        /// <summary>
        /// 对话节点视图字典
        /// </summary>
        private Dictionary<DialogueNode, Node> nodeViewMap = new();

        public DialogueNodeTreeViewer()
        {
            Insert(0, new GridBackground());
            this.AddManipulator(new ContentDragger()); // 视图拖动
            this.AddManipulator(new SelectionDragger()); // 节点拖动
            this.AddManipulator(new RectangleSelector()); // 框选
            this.AddManipulator(new ContentZoomer()); // 缩放

            // 添加网格背景
            string styleSheetPath = "Packages/com.zhanlehall.pluliter/Editor/NodeEditor/DialogueNodeTreeViewer/DialogueNodeTreeViewer.uss";
            StyleSheet styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(styleSheetPath);
            if (styleSheet != null)
            {
                styleSheets.Add(styleSheet);
            }
            else
            {
                Debug.LogError("无法加载样式表: " + styleSheetPath);
            }
        }

        /// <summary>
        /// 当GraphView改变时触发，删除和添加以及连接等操作
        /// </summary>
        /// <param name="change"></param>
        /// <returns></returns>
        private GraphViewChange OnGraphViewChanged(GraphViewChange change)
        {
            // 删除
            if (change.elementsToRemove != null)
            {
                change.elementsToRemove.ForEach(element =>
                {
                    if (element is Node node)
                    {
                        DeleteDialogueNode(node);
                    }
                    if (element is Edge edge)
                    {
                        if (edge.input.node != null && edge.output.node != null)
                        {
                            var outputNode = edge.output.node;
                            var inputNode = edge.input.node;

                            // 如果输出节点是选择节点
                            if (outputNode is OptionNodeView)
                            {
                                var outChoiceNode = outputNode as OptionNodeView;
                                if (inputNode is DialogueNodeView)
                                {
                                    var inDialogueNode = inputNode as DialogueNodeView;

                                    for (var i = 0; i < outChoiceNode.dialogueNode.DialogueOptions.Count; i++)
                                    {
                                        if (outChoiceNode.choicePorts[outChoiceNode.dialogueNode.DialogueOptions[i].optionID].portName == edge.output.portName)
                                        {
                                            outChoiceNode.dialogueNode.DialogueOptions[i].OptionNextDialogueNode = null;
                                            outChoiceNode.choicePorts[outChoiceNode.dialogueNode.DialogueOptions[i].optionID].Disconnect(edge);
                                            inDialogueNode.inputPort.Disconnect(edge);
                                        }
                                    }
                                }
                            }
                            else if (outputNode is ConditionBranchNodeView)
                            {
                                var outConditionNode = outputNode as ConditionBranchNodeView;
                                if (inputNode is DialogueNodeView)
                                {
                                    var inDialogueNode = inputNode as DialogueNodeView;

                                    if (outConditionNode.choicePorts["trueBranch"].portName == edge.output.portName)
                                    {
                                        outConditionNode.dialogueNode.ConditionBranch.TrueNextNode = null;
                                        outConditionNode.choicePorts["trueBranch"].Disconnect(edge);
                                        inDialogueNode.inputPort.Disconnect(edge);
                                    }
                                    else if (outConditionNode.choicePorts["falseBranch"].portName == edge.output.portName)
                                    {
                                        outConditionNode.dialogueNode.ConditionBranch.FalseNextNode = null;
                                        outConditionNode.choicePorts["falseBranch"].Disconnect(edge);
                                        inDialogueNode.inputPort.Disconnect(edge);
                                    }
                                }
                            }
                            // 如果输出节点是普通对话节点
                            else if (outputNode is DialogueNodeView)
                            {
                                var outDialogueNode = outputNode as DialogueNodeView;
                                if (inputNode is DialogueNodeView)
                                {
                                    var inDialogueNode = inputNode as DialogueNodeView;
                                    outDialogueNode.dialogueNode.NextDialogueNode = null;
                                    outDialogueNode.outputPort.Disconnect(edge);
                                    inDialogueNode.inputPort.Disconnect(edge);
                                }
                            }
                        }
                    }
                });
            }
            // 添加连线
            else
            {
                change.edgesToCreate?.ForEach(edge =>
                {
                    if (edge.output.node != null && edge.input.node != null)
                    {
                        var outputNode = edge.output.node;
                        var inputNode = edge.input.node;

                        // 如果输出节点是普通对话节点
                        if (outputNode is OptionNodeView)
                        {
                            var outChoiceNode = outputNode as OptionNodeView;
                            if (inputNode is DialogueNodeView)
                            {
                                var inDialogueNode = inputNode as DialogueNodeView;
                                for (var i = 0; i < outChoiceNode.dialogueNode.DialogueOptions.Count; i++)
                                {
                                    if (outChoiceNode.choicePorts[outChoiceNode.dialogueNode.DialogueOptions[i].optionID].portName == edge.output.portName)
                                    {
                                        outChoiceNode.dialogueNode.DialogueOptions[i].OptionNextDialogueNode = inDialogueNode.dialogueNode;
                                        outChoiceNode.choicePorts[outChoiceNode.dialogueNode.DialogueOptions[i].optionID].Disconnect(edge);
                                        inDialogueNode.inputPort.Disconnect(edge);
                                        outChoiceNode.choicePorts[outChoiceNode.dialogueNode.DialogueOptions[i].optionID].Connect(edge);
                                        inDialogueNode.inputPort.Connect(edge);
                                    }
                                }
                            }
                        }
                        else if (outputNode is ConditionBranchNodeView)
                        {
                            var outConditionNode = outputNode as ConditionBranchNodeView;
                            if (inputNode is DialogueNodeView)
                            {
                                var inDialogueNode = inputNode as DialogueNodeView;
                                if (outConditionNode.choicePorts["trueBranch"].portName == edge.output.portName)
                                {
                                    outConditionNode.dialogueNode.ConditionBranch.TrueNextNode = inDialogueNode.dialogueNode;
                                    outConditionNode.choicePorts["trueBranch"].Disconnect(edge);
                                    inDialogueNode.inputPort.Disconnect(edge);
                                    outConditionNode.choicePorts["trueBranch"].Connect(edge);
                                    inDialogueNode.inputPort.Connect(edge);
                                }
                                else if (outConditionNode.choicePorts["falseBranch"].portName == edge.output.portName)
                                {
                                    outConditionNode.dialogueNode.ConditionBranch.FalseNextNode = inDialogueNode.dialogueNode;
                                    outConditionNode.choicePorts["falseBranch"].Disconnect(edge);
                                    inDialogueNode.inputPort.Disconnect(edge);
                                    outConditionNode.choicePorts["falseBranch"].Connect(edge);
                                    inDialogueNode.inputPort.Connect(edge);
                                }
                            }
                        }
                        else if (outputNode is DialogueNodeView)
                        {
                            var outDialogueNode = outputNode as DialogueNodeView;
                            if (inputNode is DialogueNodeView)
                            {
                                var inDialogueNode = inputNode as DialogueNodeView;

                                // 清除原有输出连接
                                if (outDialogueNode.outputPort.connected)
                                {
                                    RemoveExistingConnections(outDialogueNode.outputPort);
                                }

                                outDialogueNode.dialogueNode.NextDialogueNode = inDialogueNode.dialogueNode;

                                outDialogueNode.outputPort.Connect(edge);
                                inDialogueNode.inputPort.Connect(edge);

                                // 刷新检查器
                                OnDialogueNodeSelected?.Invoke(outDialogueNode.dialogueNode, outDialogueNode);
                            }
                        }
                    }
                });
            }
            return change;
        }

        private void RemoveExistingConnections(Port port)
        {
            var edgesToRemove = port.connections.ToList();
            foreach (var edge in edgesToRemove)
            {
                edge.input.Disconnect(edge);
                edge.output.Disconnect(edge);
                RemoveElement(edge);
            }
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            // 获取鼠标在屏幕上的位置
            VisualElement contentViewContainer = ElementAt(1);
            Vector3 screenMousePosition = evt.localMousePosition;
            Vector2 worldMousePosition = screenMousePosition - contentViewContainer.transform.position;
            worldMousePosition *= 1 / contentViewContainer.transform.scale.x;
            mousePosition = worldMousePosition;

            // 遍历枚举值DialogueNodeType
            foreach (DialogueNodeType type in Enum.GetValues(typeof(DialogueNodeType)))
            {
                // 获取枚举值的类型 获取中文名称
                var memberInfo = type.GetType().GetMember(type.ToString());
                if (memberInfo != null && memberInfo.Length > 0)
                {
                    var attrs = memberInfo[0].GetCustomAttributes(typeof(InspectorNameAttribute), false);
                    if (attrs.Length > 0)
                    {
                        if (attrs[0] is InspectorNameAttribute attr)
                        {
                            var name = attr.displayName;
                            // 判空
                            DropdownMenuAction.Status status = DropdownMenuAction.Status.Normal;
                            if (dialogueChapter == null)
                            {
                                status = DropdownMenuAction.Status.Disabled;
                            }
                            evt.menu.AppendAction(name.ToString(), (e) => CreateDialogueNode(type), status);
                        }
                    }
                }
            }

        }

        /// <summary>
        /// 创建对话节点
        /// </summary>
        /// <param name="type"></param>
        private void CreateDialogueNode(DialogueNodeType type)
        {
            if (dialogueChapter == null)
            {
                // 弹出提示
                EditorUtility.DisplayDialog("多文 Pluliter", "请先选择对话章节", "确认");
                return;
            }

            // 只允许有一个对话起点
            if (type == DialogueNodeType.StartSingle)
            {
                if (dialogueChapter.StartNode != null ||
                    dialogueChapter.DialogueNodes.Any(n => n.dialogueType == DialogueNodeType.StartSingle))
                {
                    EditorUtility.DisplayDialog("多文 Pluliter", "该章节已有起点", "确认");
                    return;
                }
            }

            // 实例化一个DialogueNode ScriptableObject
            var dialogueNode = ScriptableObject.CreateInstance<DialogueNode>();
            dialogueNode.dialogueType = type;

            // 起点
            if (type == DialogueNodeType.StartSingle)
            {
                dialogueChapter.StartNode = dialogueNode;
            }

            // 设置节点位置
            dialogueNode.NodeViewPosition = mousePosition;
            dialogueNode.name = "DialogueNode_" + dialogueNode.GetInstanceID();

            // 作为子资源保存到节点树
            AssetDatabase.AddObjectToAsset(dialogueNode, dialogueChapter);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            dialogueChapter.DialogueNodes.Add(dialogueNode);

            Node nodeView = DialogueNodeViewFactory.CreateNodeView(this, dialogueNode);

            AddElement(nodeView);
        }



        /// <summary>
        /// 删除对话节点
        /// </summary>
        /// <param name="nodeView"></param>
        public void DeleteDialogueNode(Node nodeView)
        {
            if (nodeView is DialogueNodeView dialogueNodeView)
            {
                DisconnectAllPorts(nodeView);
                var dialogueNode = dialogueNodeView.dialogueNode;
                // 从节点树中移除节点
                RemoveElement(nodeView);
                // 从dialogueChapter中移除DialogueNode
                if (dialogueChapter.DialogueNodes.Contains(dialogueNode))
                {
                    if (dialogueNode.dialogueType == DialogueNodeType.StartSingle && dialogueNode == dialogueChapter.StartNode)
                    {
                        dialogueChapter.StartNode = null;
                        EditorUtility.DisplayDialog("多文 Pluliter", "该对话章节没有对话起点", "确认");
                    }
                    dialogueChapter.DialogueNodes.Remove(dialogueNode);
                    // 从资产数据库中移除DialogueNode
                    AssetDatabase.RemoveObjectFromAsset(dialogueNode);
                }
                // 更新资产数据库
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }

        /// <summary>
        /// 断开所有连线
        /// </summary>
        /// <param name="node"></param>
        private void DisconnectAllPorts(Node node)
        {
            foreach (Port port in node.inputContainer.Children().Concat(node.outputContainer.Children()).Cast<Port>())
            {
                port.DisconnectAll();
            }
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            // 获取所有兼容的端口
            var compatiblePorts = ports.ToList().Where(
                endPort => endPort.direction != startPort.direction
                && endPort.node != startPort.node
            ).ToList();
            return compatiblePorts;
        }

        /// <summary>
        /// 设置对话章节
        /// </summary>
        /// <param name="dialogueChapter"></param>
        public void SetDialogueChapter(DialogueChapter dialogueChapter)
        {
            if (dialogueChapter == null)
            {
                this.dialogueChapter = null;
                // 取消订阅，防止在GenerateNodes函数中清空对话节点
                graphViewChanged -= OnGraphViewChanged;
                ClearAll();
            }
            else
            {
                // 取消订阅，防止在GenerateNodes函数中清空对话节点
                graphViewChanged -= OnGraphViewChanged;
                this.dialogueChapter = dialogueChapter;
                if (dialogueChapter.characterTable != null)
                {
                    CheckCharacterTable();
                }
                else
                {
                    // 在Assets目录中寻找查找所有CharacterTable资产
                    var characterTables = AssetDatabase.FindAssets("t:CharacterTable");
                    if (characterTables.Length > 0)
                    {
                        // 选择第一个
                        var characterTablePath = AssetDatabase.GUIDToAssetPath(characterTables[0]);
                        var tmpCharacterTable = AssetDatabase.LoadAssetAtPath<CharacterTable>(characterTablePath);
                        var autoSet = EditorUtility.DisplayDialog("错误", $"对话章节没有角色表，是否自动添加角色表: { tmpCharacterTable.name }", "确定", "稍后手动添加");
                        if (autoSet)
                        {
                            dialogueChapter.characterTable = tmpCharacterTable;
                            AssetDatabase.SaveAssets();
                            EditorUtility.DisplayDialog("提示", "角色表已添加", "确定");
                            CheckCharacterTable();
                        }
                        else
                        {
                            Debug.LogWarning("对话章节没有角色表，请手动添加角色表");
                        }
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("错误", "没有找到任何角色表，请手动创建并添加角色表", "确定");
                    }
                }
                GenerateNodes();
                GenerateEdges();

                // 选择并聚焦起始节点
                if (dialogueChapter.StartNode != null)
                {
                    EditorApplication.delayCall += () =>
                    {
                        if (nodeViewMap.TryGetValue(dialogueChapter.StartNode, out var startNodeView))
                        {
                            ClearSelection();
                            AddToSelection(startNodeView);
                            FrameSelection();
                        }
                    };
                }

                // 订阅节点事件
                graphViewChanged += OnGraphViewChanged;
            }
        }

        /// <summary>
        /// 检查角色表
        /// </summary>
        public void CheckCharacterTable()
        {
            if (dialogueChapter.characterTable.characters.Count <= 0)
            {
                var autoAdd = EditorUtility.DisplayDialog("提示", "角色表没有角色，是否前往编辑角色表", "确定", "稍后手动编辑");
                if (autoAdd)
                {
                    AssetDatabase.OpenAsset(dialogueChapter.characterTable);
                }
                else
                {
                    Debug.LogWarning("角色表没有角色，请前往编辑角色表");
                }
            }
        }

        /// <summary>
        /// 清除节点
        /// </summary>
        public void ClearAll()
        {
            DeleteElements(graphElements);
        }

        /// <summary>
        /// 生成所有节点
        /// </summary>
        public void GenerateNodes()
        {
            if (dialogueChapter == null)
            {
                return;
            }
            DeleteElements(graphElements); // 清空节点

            // 创建一个字典来存储节点和它们的视图
            Dictionary<DialogueNode, Node> nodeViews = new();

            foreach (var node in dialogueChapter.DialogueNodes)
            {
                var nodeView = GenerateNode(node) as Node;
                nodeViews[node] = nodeView;
                if (nodeViews != null)
                {
                    AddElement(nodeView);
                }
            }

            // 保存节点视图字典，以便在GenerateNodeLinks中使用
            this.nodeViewMap = nodeViews;
        }

        /// <summary>
        /// 生成节点
        /// </summary>
        /// <param name="dialogueNode"></param>
        /// <returns></returns>
        public GraphElement GenerateNode(DialogueNode dialogueNode)
        {
            var nodeView = DialogueNodeViewFactory.CreateNodeView(this, dialogueNode);
            return nodeView;

        }

        /// <summary>
        /// 生成节点之间的连线
        /// </summary>
        public void GenerateEdges()
        {
            if (dialogueChapter == null)
            {
                return;
            }

            // 遍历所有节点
            foreach (var node in dialogueChapter.DialogueNodes)
            {
                // 获取节点的视图
                var nodeView = nodeViewMap[node];

                // 如果节点有下一个节点
                if (node.NextDialogueNode != null)
                {
                    // 获取下一个节点的视图
                    var nextNodeView = nodeViewMap[node.NextDialogueNode];
                    CreateAndConnectEdge(nodeView.outputContainer[0] as Port, nextNodeView.inputContainer[0] as Port);
                }
                // 如果是选项节点
                if (node.DialogueOptions != null && node.DialogueOptions.Count > 0)
                {
                    foreach (var option in node.DialogueOptions)
                    {
                        if (option.OptionNextDialogueNode == null)
                        {
                            continue;
                        }
                        // 获取下一个节点的视图
                        var nextNodeView = nodeViewMap[option.OptionNextDialogueNode];

                        var choiceNodeView = nodeView as OptionNodeView;

                        // 创建连线
                        CreateAndConnectEdge(choiceNodeView.choicePorts[option.optionID], nextNodeView.inputContainer[0] as Port);
                    }
                }
                if (node.ConditionBranch != null)
                {
                    if (node.ConditionBranch.TrueNextNode != null)
                    {
                        var nextNodeView = nodeViewMap[node.ConditionBranch.TrueNextNode];
                        var conditionNodeView = nodeView as ConditionBranchNodeView;
                        CreateAndConnectEdge(conditionNodeView.choicePorts["trueBranch"], nextNodeView.inputContainer[0] as Port);
                    }

                    if (node.ConditionBranch.FalseNextNode != null)
                    {
                        var nextNodeView = nodeViewMap[node.ConditionBranch.FalseNextNode];
                        var conditionNodeView = nodeView as ConditionBranchNodeView;
                        CreateAndConnectEdge(conditionNodeView.choicePorts["falseBranch"], nextNodeView.inputContainer[0] as Port);
                    }
                }
            }
        }

        /// <summary>
        /// 创建并连线
        /// </summary>
        /// <param name="outputPort"></param>
        /// <param name="inputPort"></param>
        private void CreateAndConnectEdge(Port outputPort, Port inputPort)
        {
            var edge = new Edge { output = outputPort, input = inputPort };
            edge.input.Connect(edge);
            edge.output.Connect(edge);
            AddElement(edge);
        }

        
    }
}
