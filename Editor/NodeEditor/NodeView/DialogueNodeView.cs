﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Dialogue;
using System;
using UnityEditor.Experimental.GraphView;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 对话节点视图抽象基类，所有对话节点视图类都继承自该类
    /// </summary>
    public abstract class DialogueNodeView : Node
    {
        /// <summary>
        /// 输入端口
        /// </summary>
        public Port inputPort;

        /// <summary>
        /// 输出端口
        /// </summary>
        public Port outputPort;

        /// <summary>
        /// 节点选中事件
        /// </summary>
        public Action<DialogueNodeView> OnNodeSelected;

        /// <summary>
        /// 节点取消选中事件
        /// </summary>
        public Action<DialogueNodeView> OnNodeUnselected;

        /// <summary>
        /// 对话节点数据
        /// </summary>
        public DialogueNode dialogueNode;

        /// <summary>
        /// 
        /// </summary>
        public string GUID;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dialogueNode"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public DialogueNodeView(DialogueNode dialogueNode)
        {
            if (dialogueNode == null)
            {
                throw new ArgumentNullException(nameof(dialogueNode));
            }
            this.dialogueNode = dialogueNode;
            style.left = dialogueNode.NodeViewPosition.x;
            style.top = dialogueNode.NodeViewPosition.y;
        }
    }
}
