﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine;
using UnityEditor.Experimental.GraphView;
using Pluliter.Dialogue;
using UnityEditor;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 对话节点视图
    /// </summary>
    public class StartSingleNodeView : DialogueNodeView
    {
        public StartSingleNodeView(DialogueNode dialogueNode) : base(dialogueNode)
        {
            CreateOutputPorts();
            UpdateNodeTitle();
        }

        /// <summary>
        /// 创建输出端口
        /// </summary>
        private void CreateOutputPorts()
        {
            outputPort = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, typeof(bool));
            if (outputPort != null)
            {
                outputPort.portName = "起点";
                outputContainer.Add(outputPort);
            }
        }

        // 设置节点在节点树视图中的位置
        public override void SetPosition(Rect newPos)
        {
            // 将视图中节点位置设置为最新位置newPos
            base.SetPosition(newPos);
            // 将最新位置记录到运行时节点树中持久化存储
            dialogueNode.NodeViewPosition.x = newPos.xMin;
            dialogueNode.NodeViewPosition.y = newPos.yMin;
            EditorUtility.SetDirty(dialogueNode);
        }


        public override void OnSelected()
        {
            base.OnSelected();
            // 如果当前OnNodeSelected选中部位空则将该节点视图传递到OnNodeSelected方法中视为选中
            OnNodeSelected?.Invoke(this);
        }


        public override void OnUnselected()
        {
            base.OnUnselected();
            OnNodeUnselected?.Invoke(this);
        }

        /// <summary>
        /// 更新节点标题
        /// </summary>
        public void UpdateNodeTitle()
        {
            title = "对话开始";
        }
    }
}
