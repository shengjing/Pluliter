﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine;
using UnityEditor.Experimental.GraphView;
using Pluliter.Dialogue;
using UnityEditor;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 晃动角色节点视图
    /// </summary>
    public class ShakeActorNodeView : DialogueNodeView
    {
        public ShakeActorNodeView(DialogueNode dialogueNode) : base(dialogueNode)
        {
            CreateInputPorts();
            CreateOutputPorts();

            UpdateNodeTitle();
        }

        /// <summary>
        /// 创建输入端口
        /// </summary>
        private void CreateInputPorts()
        {
            /*将节点入口设置为 
                接口链接方向 横向Orientation.Vertical  竖向Orientation.Horizontal
                接口可链接数量 Port.Capacity.Single
                接口类型 typeof(bool)
            */
            // 默认所有节点为多入口类型
            inputPort = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Multi, typeof(bool));

            if (inputPort != null)
            {
                inputPort.portName = "";
                inputContainer.Add(inputPort);
            }
        }

        /// <summary>
        /// 创建输出端口
        /// </summary>
        private void CreateOutputPorts()
        {
            outputPort = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, typeof(bool));
            if (outputPort != null)
            {
                outputPort.portName = "";
                outputContainer.Add(outputPort);
            }
        }


        // 设置节点在节点树视图中的位置
        public override void SetPosition(Rect newPos)
        {
            // 将视图中节点位置设置为最新位置newPos
            base.SetPosition(newPos);
            // 将最新位置记录到运行时节点树中持久化存储
            dialogueNode.NodeViewPosition.x = newPos.xMin;
            dialogueNode.NodeViewPosition.y = newPos.yMin;
            EditorUtility.SetDirty(dialogueNode);
        }


        public override void OnSelected()
        {
            base.OnSelected();
            // 如果当前OnNodeSelected选中部位空则将该节点视图传递到OnNodeSelected方法中视为选中
            OnNodeSelected?.Invoke(this);
        }

        public override void OnUnselected()
        {
            base.OnUnselected();
            OnNodeUnselected?.Invoke(this);
        }

        public void UpdateNodeTitle()
        {
            if (dialogueNode.CharacterId != null)
            {
                title = "晃动演员 " + dialogueNode.CharacterId + " 时间：" + dialogueNode.AnimationDuration.ToString();
            }
            else
            {
                title = "晃动演员";
            }
        }
    }
}
