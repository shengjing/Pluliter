﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Dialogue;
using System;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 对话节点视图工厂
    /// </summary>
    public static class DialogueNodeViewFactory
    {
        /// <summary>
        /// 绑定对话节点视图
        /// </summary>
        /// <param name="dialogueNodeView"></param>
        /// <param name="nodeTreeViewer"></param>
        public static void BindNodeView(ref DialogueNodeView dialogueNodeView, DialogueNodeTreeViewer nodeTreeViewer)
        {
            if (nodeTreeViewer == null)
            {
                Debug.LogError("DialogueNodeTreeViewer is null");
                return;
            }
            if (dialogueNodeView != null)
            {
                dialogueNodeView.OnNodeSelected = (nv) =>
                {
                    if (nodeTreeViewer.OnDialogueNodeSelected != null)
                    {
                        nodeTreeViewer.OnDialogueNodeSelected.Invoke(nv.dialogueNode, nv);
                    }
                };
                dialogueNodeView.OnNodeUnselected = (nv) =>
                {
                    if (nodeTreeViewer.OnDialogueNodeUnselected != null)
                    {
                        nodeTreeViewer.OnDialogueNodeUnselected.Invoke(nv.dialogueNode, nv);
                    }
                };
            }
        }
        
        /// <summary>
        /// 创建对话节点视图
        /// </summary>
        /// <param name="nodeTreeViewer"></param>
        /// <param name="dialogueNode"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static Node CreateNodeView(DialogueNodeTreeViewer nodeTreeViewer, DialogueNode dialogueNode)
        {
            DialogueNodeView nodeView;
            switch (dialogueNode.dialogueType)
            {
                case DialogueNodeType.NormalDialogue:
                    nodeView = new NormalDialogueNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.Choice:
                    nodeView = new OptionNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.StartSingle:
                    nodeView = new StartSingleNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.StopSingle:
                    nodeView = new StopSingleNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.FullScreenDialogue:
                    nodeView = new FullScreenDialogueNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.Narration:
                    nodeView = new NarrationNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.MoveInActor:
                    nodeView = new MoveInActorNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.ShakeActor:
                    nodeView = new ShakeActorNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.ModifyActor:
                    nodeView = new ModifyActorNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.MoveOutActor:
                    nodeView = new MoveOutActorNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.Transition:
                    nodeView = new TransitionNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.PlayBgm:
                    nodeView = new PlayBgmNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.StopBgm:
                    nodeView = new StopBgmNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.PlaySoundEffect:
                    nodeView = new PlaySoundEffectNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.StopSoundEffect:
                    nodeView = new StopSoundEffectNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.JumpToChapter:
                    nodeView = new JumpToChapterNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.ConditionBranch:
                    nodeView = new ConditionBranchNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                case DialogueNodeType.SetVariable:
                    nodeView = new SetVariableNodeView(dialogueNode);
                    BindNodeView(ref nodeView, nodeTreeViewer);
                    return nodeView;
                default:
                    Debug.LogError("未知的节点类型");
                    return null; 
            }
        }
    }
}
