﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine;
using UnityEditor.Experimental.GraphView;
using System;
using Pluliter.Dialogue;
using UnityEditor;
using System.Collections.Generic;


namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 选择节点视图
    /// </summary>
    public class OptionNodeView : DialogueNodeView
    {
        /// <summary>
        /// 选择端口
        /// </summary>
        public Dictionary<string, Port> choicePorts = new Dictionary<string, Port>();

        public OptionNodeView(DialogueNode dialogueNode) : base(dialogueNode)
        {
            UpdateNodeTitle();
            CreateInputPorts();
            if (dialogueNode.DialogueOptions != null)
            {
                foreach (var choice in dialogueNode.DialogueOptions)
                {
                    CreateOutputPorts(choice.optionID);
                }
            }
        }

        /// <summary>
        /// 创建输入端口
        /// </summary>
        private void CreateInputPorts()
        {
            /*将节点入口设置为 
                接口链接方向 横向Orientation.Vertical  竖向Orientation.Horizontal
                接口可链接数量 Port.Capacity.Single
                接口类型 typeof(bool)
            */
            // 默认所有节点为多入口类型
            inputPort = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Multi, typeof(bool));

            if (inputPort != null)
            {
                // 将端口名设置为空
                inputPort.portName = "";
                inputContainer.Add(inputPort);
            }
        }

        /// <summary>
        /// 创建输出端口
        /// </summary>
        public void CreateOutputPorts(string choiceId)
        {
            if (string.IsNullOrEmpty(choiceId))
            {
                Debug.LogWarning("ChoiceId is null or empty.");
                return;
            }
            if (choicePorts.ContainsKey(choiceId))
            {
                Debug.LogWarning($"ChoiceId {choiceId} already exists.");
                return;
            }
            var outputPort = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, typeof(bool));
            if (outputPort != null)
            {
                outputPort.portName = dialogueNode.DialogueOptions.Find(x => x.optionID == choiceId).optionText;
                outputContainer.Add(outputPort);
                choicePorts.Add(choiceId, outputPort);
            }
        }

        /// <summary>
        /// 删除输出端口
        /// </summary>
        /// <param name="choiceId"></param>
        public void DeleteOutputPorts(string choiceId)
        {
            var port = choicePorts[choiceId];
            outputContainer.Remove(port);
            choicePorts.Remove(choiceId);
        }


        // 设置节点在节点树视图中的位置
        public override void SetPosition(Rect newPos)
        {
            // 将视图中节点位置设置为最新位置newPos
            base.SetPosition(newPos);
            // 将最新位置记录到运行时节点树中持久化存储
            dialogueNode.NodeViewPosition.x = newPos.xMin;
            dialogueNode.NodeViewPosition.y = newPos.yMin;
            EditorUtility.SetDirty(dialogueNode);
        }


        public override void OnSelected()
        {
            base.OnSelected();
            // 如果当前OnNodeSelected选中部位空则将该节点视图传递到OnNodeSelected方法中视为选中
            OnNodeSelected?.Invoke(this);
        }

        public override void OnUnselected()
        {
            base.OnUnselected();
            OnNodeUnselected?.Invoke(this);
        }



        /// <summary>
        /// 更新节点标题
        /// </summary>
        public void UpdateNodeTitle()
        {
            var tempTitle = "对话选项";
            title = tempTitle;
        }

        public void ChangeOutputPortName(string newValue, string value)
        {
            if (choicePorts.ContainsKey(value))
            {
                var tempTitle = newValue;

                if (tempTitle.Length >= 15)
                {
                    tempTitle = tempTitle[..15] + "...";
                }
                if (tempTitle.Length <= 0)
                {
                    tempTitle = "无选项内容";
                    titleContainer.style.color = Color.yellow;
                }
                choicePorts[value].portName = newValue;
            }
        }
    }
}