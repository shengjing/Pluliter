﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Dialogue;
using System;
using UnityEngine;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 对话节点检视器工厂，根据根据对话节点类型创建对应的检视器
    /// </summary>
    public static class NodeInspectorFactory
    {
        /// <summary>
        /// 创建节点检视器
        /// </summary>
        /// <param name="nodeInspector">对话检查器元素</param>
        /// <returns>对话检查器</returns>
        /// <exception cref="NotImplementedException"></exception>
        public static INodeInspector CreateInspector(DialogueNodeInspector nodeInspector)
        {
            DialogueNode dialogueNode = nodeInspector.curDialogueNode;
            if (dialogueNode == null)
            {
                return new NullDialogueInspector(nodeInspector);
            }
            switch (dialogueNode.dialogueType)
            {
                case DialogueNodeType.NormalDialogue:
                    return new NormalDialogueInspector(nodeInspector);
                case DialogueNodeType.Choice:
                    return new OptionDialogueInspector(nodeInspector);
                case DialogueNodeType.StartSingle:
                    return new StartSingleInspector(nodeInspector);
                case DialogueNodeType.StopSingle:
                    return new StopSingleInspector(nodeInspector);
                case DialogueNodeType.FullScreenDialogue:
                    return new FullScreenDialogueInspector(nodeInspector);
                case DialogueNodeType.Narration:
                    return new NarrationlInspector(nodeInspector);
                case DialogueNodeType.MoveInActor:
                    return new MoveInActorInspector(nodeInspector);
                case DialogueNodeType.ShakeActor:
                    return new ShakeActorInspector(nodeInspector);
                case DialogueNodeType.ModifyActor:
                    return new ModifyActorInspector(nodeInspector);
                case DialogueNodeType.MoveOutActor:
                    return new MoveOutActorInspector(nodeInspector);
                case DialogueNodeType.Transition:
                    return new TransitionInspector(nodeInspector);
                case DialogueNodeType.PlayBgm:
                    return new PlayBgmInspector(nodeInspector);
                case DialogueNodeType.StopBgm:
                    return new StopBgmInspector(nodeInspector);
                case DialogueNodeType.PlaySoundEffect:
                    return new PlaySoundEffectInspector(nodeInspector);
                case DialogueNodeType.StopSoundEffect:
                    return new StopSoundEffectInspector(nodeInspector);
                case DialogueNodeType.JumpToChapter:
                    return new JumpToChapterInspector(nodeInspector);
                case DialogueNodeType.ConditionBranch:
                    return new ConditionBranchInspector(nodeInspector);
                case DialogueNodeType.SetVariable:
                    return new SetVariableInspector(nodeInspector);
                default:
                    Debug.LogError("未知的对话类型，无法创建节点检视器");
                    return null;
            }
        }
    }
}
