﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Pluliter.Background;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 背景切换检查器
    /// </summary>
    public class TransitionInspector : INodeInspector
    {
        private DialogueNodeInspector nodeInspector;
        public TransitionInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
        }

        public void OnInspectorGUI()
        {
            var dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as TransitionNodeView;

            //transition bg="bgid" anim="fadein" duration="0.5"

            var bgDropdown = new DropdownField("背景");
            bgDropdown.choices = nodeInspector.backgroundTable.GetBackgroundNames();
            bgDropdown.value = dialogueNode.BackgroundId;
            bgDropdown.RegisterValueChangedCallback(e =>
            {
                dialogueNode.BackgroundId = e.newValue;
                dialogueNodeView.UpdateNodeTitle();

            });
            nodeInspector.Add(bgDropdown);

            var animDropdown = new DropdownField("动画");
            var bgAnimations = new List<string>();
            // TransitionAnimType
            foreach (TransitionAnimType type in Enum.GetValues(typeof(TransitionAnimType)))
            {
                // 获取枚举值的类型 获取中文名称
                var memberInfo = type.GetType().GetMember(type.ToString());
                if (memberInfo != null && memberInfo.Length > 0)
                {
                    var attrs = memberInfo[0].GetCustomAttributes(typeof(InspectorNameAttribute), false);
                    if (attrs.Length > 0)
                    {
                        var attr = attrs[0] as InspectorNameAttribute;
                        if (attr != null)
                        {
                            var name = attr.displayName;
                            bgAnimations.Add(name);
                        }
                    }
                }
            }
            animDropdown.choices = bgAnimations;
            animDropdown.value = bgAnimations[(int)dialogueNode.BackgroundTransitionType];
            animDropdown.RegisterValueChangedCallback(e =>
            {
                dialogueNode.BackgroundTransitionType = (TransitionAnimType)bgAnimations.IndexOf(e.newValue);
            });

            nodeInspector.Add(animDropdown);

            var durationField = new FloatField("背景过度动画时间");
            durationField.value = dialogueNode.AnimationDuration;
            durationField.RegisterValueChangedCallback(e => 
            {
                dialogueNode.AnimationDuration = e.newValue;
            });
            nodeInspector.Add(durationField);
        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {
            
        }
    }
}
