﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Actor;
using Pluliter.Character;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 修改演员状态节点检视器
    /// </summary>
    public class ModifyActorInspector : INodeInspector
    {
        private readonly DialogueNodeInspector nodeInspector;
        private readonly CharacterTable characterTable;


        /// <summary>
        /// 是否预览编辑模式
        /// </summary>
        private bool isEditMode = false;

        /// <summary>
        /// 预览演员
        /// </summary>
        private GameObject currentPreviewActor;

        public ModifyActorInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
            this.characterTable = nodeInspector.characterTable;
        }

        public void OnInspectorGUI()
        {
            var dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as ModifyActorNodeView;

            var characterDropdown = new DropdownField("演员")
            {
                choices = characterTable.GetCharacterNames()
            };

            var modifyActorState = new Toggle("修改状态")
            {
                value = dialogueNode.ModifyActorState
            };
            modifyActorState.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.ModifyActorState = e.newValue;
            });
            modifyActorState.style.marginBottom = 10f;
            AddElement(modifyActorState);

            var stateDropdown = new DropdownField("状态");
            if (!string.IsNullOrEmpty(dialogueNode.CharacterId))
            {
                stateDropdown.choices = characterTable.GetCharacterByName(dialogueNode.CharacterId).GetCharacterStateNames();
                stateDropdown.value = dialogueNode.CharacterStateId;
            }
            stateDropdown.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.CharacterStateId = e.newValue;
                dialogueNodeView.UpdateNodeTitle();
            });
            stateDropdown.style.marginBottom = 10f;

            if (characterDropdown.choices.Count > 0)
            {
                if (dialogueNode.CharacterId == null)
                {
                    characterDropdown.value = characterTable.GetCharacterNames().First();
                    dialogueNode.CharacterId = characterTable.GetCharacterByName(characterDropdown.value).CharacterName;
                }
                else
                {
                    characterDropdown.value = dialogueNode.CharacterId;
                }
            }
            characterDropdown.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.CharacterId = characterTable.GetCharacterByName(characterDropdown.value).CharacterName;
                Debug.Log($"CharacterId: {dialogueNode.CharacterId}");
                stateDropdown.choices = characterTable.GetCharacterByName(characterDropdown.value).GetCharacterStateNames();
                Debug.Log($"StateChoices: {string.Join(",", stateDropdown.choices)}");
                if (stateDropdown.choices.Count > 0)
                {
                    stateDropdown.value = stateDropdown.choices.First();
                }
                else
                {
                    dialogueNode.CharacterId = null;
                    stateDropdown.value = null;
                }

                dialogueNodeView.UpdateNodeTitle();
            });
            characterDropdown.style.marginBottom = 10f;
            AddElement(characterDropdown);
            AddElement(stateDropdown);

            var scaleField = new FloatField("预览缩放")
            {
                value = dialogueNode.ActorScale
            };
            scaleField.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.ActorScale = e.newValue;
            });
            scaleField.style.marginBottom = 10f;
            AddElement(scaleField);

            var modifyActorPosition = new Toggle("修改位置")
            {
                value = dialogueNode.ModifyActorPosition
            };
            modifyActorPosition.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.ModifyActorPosition = e.newValue;

            });
            modifyActorPosition.style.marginBottom = 10f;
            AddElement(modifyActorPosition);

            var xField = new FloatField("X")
            {
                value = dialogueNode.ActorMoveTargetPositon.x
            };
            xField.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.ActorMoveTargetPositon = new Vector2(e.newValue, dialogueNode.ActorMoveTargetPositon.y);
            });

            xField.style.marginBottom = 10;
            AddElement(xField);

            var yField = new FloatField("Y")
            {
                value = dialogueNode.ActorMoveTargetPositon.y
            };
            yField.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.ActorMoveTargetPositon = new Vector2(dialogueNode.ActorMoveTargetPositon.x, e.newValue);
            });
            yField.style.marginBottom = 10;
            AddElement(yField);

            var durationField = new FloatField("移动时间")
            {
                value = dialogueNode.AnimationDuration
            };
            durationField.RegisterValueChangedCallback((e) => 
            { 
                dialogueNode.AnimationDuration = e.newValue;
                dialogueNodeView.UpdateNodeTitle();
            });
            durationField.style.marginBottom = 10f;
            AddElement(durationField);

            var previewEditElement = new VisualElement();

            var confirmPreviewPosBtn = new Button
            {
                text = "保存位置",
                style = { marginLeft = 10, marginRight = 10 }
            };
            confirmPreviewPosBtn.clicked += () =>
            {
                dialogueNode.ActorMoveTargetPositon = new Vector2(currentPreviewActor.transform.position.x, currentPreviewActor.transform.position.y);
                xField.value = dialogueNode.ActorMoveTargetPositon.x;
                yField.value = dialogueNode.ActorMoveTargetPositon.y;
            };
            confirmPreviewPosBtn.style.visibility = isEditMode ? Visibility.Visible : Visibility.Hidden;

            var btnText = isEditMode ? "关闭预览" : "预览编辑位置";

            var previewBtn = new Button
            {
                text = btnText,
                style = { marginTop = 10, marginLeft = 10, marginRight = 10 }
            };
            previewBtn.clicked += () =>
            {
                TogglePreviewEditMode();
                btnText = isEditMode ? "关闭预览" : "预览编辑位置";
                previewBtn.text = btnText;
                confirmPreviewPosBtn.style.visibility = isEditMode ? Visibility.Visible : Visibility.Hidden;
            };
            previewEditElement.Add(previewBtn);
            previewEditElement.Add(confirmPreviewPosBtn);

            AddElement(previewEditElement);

        }

        /// <summary>
        /// 切换编辑模式
        /// </summary>
        private void TogglePreviewEditMode()
        {
            isEditMode = !isEditMode;
            SceneView.RepaintAll();

            if (isEditMode)
            {
                CreatePreviewActor();
                SceneView.duringSceneGui += OnSceneGUI;
            }
            else
            {
                DestroyPreviewActor();
                SceneView.duringSceneGui -= OnSceneGUI;
            }
        }

        private void OnSceneGUI(SceneView sceneView)
        {
            if (!isEditMode || !currentPreviewActor)
            {
                return;
            }

            Event e = Event.current;
            Vector3 newWorldPos = Handles.PositionHandle(currentPreviewActor.transform.position, Quaternion.identity);
            if (e.type == EventType.Repaint)
            {
                currentPreviewActor.transform.position = newWorldPos;
            }
        }

        /// <summary>
        /// 创建预览演员
        /// </summary>
        private void CreatePreviewActor()
        {
#if UNITY_2023_1_OR_NEWER
            ActorManager actorManager = UnityEngine.Object.FindFirstObjectByType<ActorManager>();
#else
            ActorManager actorManager = UnityEngine.Object.FindObjectOfType<ActorManager>();
#endif

            if (actorManager != null)
            {
                var ActorLayer = actorManager.gameObject;

                if (ActorLayer.transform.position.x != 0 || ActorLayer.transform.position.y != 0)
                {
                    Debug.LogWarning("ActorManager组件的物体x和y位置不为(0,0)，可能会导致预览位置错误");
                }
                currentPreviewActor = new GameObject("预览演员");
                currentPreviewActor.transform.SetParent(ActorLayer.transform);

                currentPreviewActor.transform.position = new Vector3(nodeInspector.curDialogueNode.ActorMoveTargetPositon.x, nodeInspector.curDialogueNode.ActorMoveTargetPositon.y, 0);

                currentPreviewActor.transform.localScale = new Vector3(nodeInspector.curDialogueNode.ActorScale, nodeInspector.curDialogueNode.ActorScale, 0.5f);

                var renderer = currentPreviewActor.AddComponent<SpriteRenderer>();
                var tmpSprite = characterTable.GetCharacterByName(nodeInspector.curDialogueNode.CharacterId).GetCharacterState(nodeInspector.curDialogueNode.CharacterStateId).StateSprite;
                if (tmpSprite != null)
                {
                    renderer.sprite = tmpSprite;
                }
                else
                {
                    Debug.LogError("未找到演员Sprite");
                }

                // 在场景中选中
                Selection.activeGameObject = currentPreviewActor;

            }
            else
            {
                EditorUtility.DisplayDialog("多文", "未找到ActorManager组件的物体，无法预览编辑", "确认");
                Debug.Log("未找到ActorManager组件的物体，无法预览编辑");
            }
        }

        /// <summary>
        /// 销毁预览对象
        /// </summary>
        private void DestroyPreviewActor()
        {
            if (currentPreviewActor)
            {
                UnityEngine.Object.DestroyImmediate(currentPreviewActor);
            }
        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {
            DestroyPreviewActor();
        }
    }
}
