﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine.UIElements;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 节点检查器接口，每个节点检查器需要实现此接口
    /// </summary>
    public interface INodeInspector
    {
        /// <summary>
        /// 获取节点检查器
        /// </summary>
        /// <returns></returns>
        DialogueNodeInspector GetNodeInspector();

        /// <summary>
        /// 绘制检察器
        /// </summary>
        void OnInspectorGUI();

        /// <summary>
        /// 添加元素
        /// </summary>
        /// <param name="element">元素</param>
        void AddElement(VisualElement element);

        /// <summary>
        /// 当被销毁
        /// </summary>
        void OnDestroy();
    }
}
