﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine.UIElements;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 旁边对话检查器
    /// </summary>
    public class NarrationlInspector : INodeInspector
    {
        private DialogueNodeInspector nodeInspector;

        public NarrationlInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
        }

        public void OnInspectorGUI()
        {
            var dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as NarrationNodeView;

            var dialogueContentField = new TextField("旁边内容");
            dialogueContentField.value = dialogueNode.DialogueContent;
            dialogueContentField.RegisterValueChangedCallback(evt =>
            {
                dialogueNode.DialogueContent = evt.newValue;
                if (dialogueNodeView != null)
                {
                    dialogueNodeView.UpdateNodeTitle();
                }
            });
            AddElement(dialogueContentField);
        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {
            
        }
    }
}
