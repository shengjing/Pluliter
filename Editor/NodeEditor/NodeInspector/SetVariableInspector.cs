﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Blackboard;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;
using Pluliter.Dialogue;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 设置变量的节点检查器
    /// </summary>
    public class SetVariableInspector : INodeInspector
    {
        private readonly DialogueNodeInspector nodeInspector;

        /// <summary>
        /// 变量类型
        /// </summary>
        private enum VariableType 
        {
            [InspectorName("布尔值")]
            Bool,
            [InspectorName("浮点数")]
            Float,
            [InspectorName("整数")]
            Int 
        }

        // 临时存储新变量数据
        private string newVariableName = "";
        private string newVariableValue = "";
        private VariableType selectedType = VariableType.Bool;

        private VisualElement boolContainer;
        private VisualElement floatContainer;
        private VisualElement intContainer;

        private DialogueNode dialogueNode;

        public SetVariableInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
        }

        public void OnInspectorGUI()
        {
            dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as SetVariableNodeView;

            // 确保节点包含黑板数据
            if (dialogueNode.BlackboardDataVariables == null)
            {
                Debug.Log("节点未包含黑板数据，正在创建");
                dialogueNode.BlackboardDataVariables = new BlackboardDataVariables();
            }

            // 创建主容器
            var container = new VisualElement { style = { marginTop = 10 } };

            // 添加变量类型选择
            var typeField = new EnumField("变量类型", selectedType);
            container.Add(typeField);

            // 添加变量名称输入
            var nameField = new TextField("变量名称") { value = newVariableName };
            nameField.RegisterValueChangedCallback(e => newVariableName = e.newValue);
            container.Add(nameField);

            // 添加值输入
            var valueContainer = new VisualElement();
            UpdateValueField(valueContainer, selectedType);
            typeField.RegisterValueChangedCallback(e =>
            {
                selectedType = (VariableType)e.newValue;
                UpdateValueField(valueContainer, selectedType);
            });
            container.Add(valueContainer);

            // 添加创建按钮
            var addButton = new Button()
            {
                text = "添加变量",
                style = { marginTop = 5 }
            };


            // 显示现有变量
            boolContainer = AddVariableList("布尔变量", dialogueNode.BlackboardDataVariables.boolVariables);
            floatContainer = AddVariableList("浮点数变量", dialogueNode.BlackboardDataVariables.floatVariables);
            intContainer = AddVariableList("整数变量", dialogueNode.BlackboardDataVariables.intVariables);

            container.Add(boolContainer);
            container.Add(floatContainer);
            container.Add(intContainer);

            addButton.clicked += () =>
            {
                AddVariable(dialogueNode.BlackboardDataVariables);
                if (selectedType == VariableType.Bool)
                {
                    RefreshInspector(boolContainer, dialogueNode.BlackboardDataVariables.boolVariables);
                }
                else if (selectedType == VariableType.Float)
                {
                    RefreshInspector(floatContainer, dialogueNode.BlackboardDataVariables.floatVariables);
                }
                else if (selectedType == VariableType.Int)
                {
                    RefreshInspector(intContainer, dialogueNode.BlackboardDataVariables.intVariables);
                }
                nameField.value = "";
                UpdateValueField(valueContainer, selectedType);
            };
            container.Add(addButton);

            nodeInspector.Add(container);
        }

        private void UpdateValueField(VisualElement container, VariableType type)
        {
            container.Clear();

            switch (type)
            {
                case VariableType.Bool:
                    var boolField = new Toggle("值") { value = false };
                    boolField.RegisterValueChangedCallback(e => newVariableValue = e.newValue.ToString());
                    container.Add(boolField);
                    break;

                case VariableType.Float:
                    var floatField = new FloatField("值");
                    floatField.RegisterValueChangedCallback(e => newVariableValue = e.newValue.ToString());
                    container.Add(floatField);
                    break;

                case VariableType.Int:
                    var intField = new IntegerField("值");
                    intField.RegisterValueChangedCallback(e => newVariableValue = e.newValue.ToString());
                    container.Add(intField);
                    break;
            }
        }

        private void AddVariable(BlackboardDataVariables data)
        {
            if (string.IsNullOrEmpty(newVariableName))
            {
                EditorUtility.DisplayDialog("Error", "Variable name cannot be empty!", "OK");
                return;
            }

            switch (selectedType)
            {
                case VariableType.Bool:
                    foreach (var variable in data.boolVariables)
                    {
                        if (variable.variableName == newVariableName)
                        {
                            EditorUtility.DisplayDialog("Error", "Variable already exists!", "OK");
                            return;
                        }
                    }
                    if (bool.TryParse(newVariableValue, out bool boolValue))
                    {
                        data.boolVariables.Add(new BoolVariable { variableName = newVariableName, value = boolValue });
                    }
                    break;
                case VariableType.Float:
                    foreach (var variable in data.floatVariables)
                    {
                        if (variable.variableName == newVariableName)
                        {
                            EditorUtility.DisplayDialog("Error", "Variable already exists!", "OK");
                            return;
                        }
                    }
                    if (float.TryParse(newVariableValue, out float floatValue))
                    {
                        data.floatVariables.Add(new FloatVariable { variableName = newVariableName, value = floatValue });
                    }
                    break;
                case VariableType.Int:
                    foreach (var variable in data.intVariables)
                    {
                        if (variable.variableName == newVariableName)
                        {
                            EditorUtility.DisplayDialog("Error", "Variable already exists!", "OK");
                            return;
                        }
                    }
                    if (int.TryParse(newVariableValue, out int intValue))
                    {
                        data.intVariables.Add(new IntVariable { variableName = newVariableName, value = intValue });
                    }
                    break;
            }

            newVariableName = "";
            newVariableValue = "";

            nodeInspector.curDialogueNode.BlackboardDataVariables = data;
            EditorUtility.SetDirty(nodeInspector.curDialogueNode);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }


        public void RefreshInspector<T>(VisualElement container, List<T> variables)
        {
            container.Clear();
            foreach (var pair in variables)
            {
                var item = new VisualElement
                {
                    style = {
                        flexDirection = FlexDirection.Row,
                        marginTop = 2
                    }
                };

                if (pair is BoolVariable)
                {
                    BoolVariable tpair = pair as BoolVariable;
                    item.Add(new Label($"{tpair.variableName}: {tpair.value}"));
                }
                else if (pair is IntVariable)
                {
                    IntVariable intVariable = pair as IntVariable;
                    item.Add(new Label($"{intVariable.variableName}: {intVariable.value}"));
                }
                else if (pair is FloatVariable)
                {
                    FloatVariable floatVariable = pair as FloatVariable;
                    item.Add(new Label($"{floatVariable.variableName}: {floatVariable.value}"));
                }

                var deleteButton = new Button(() =>
                {
                    variables.Remove(pair);
                    
                    if (pair is BoolVariable)
                    {
                        RefreshInspector(boolContainer, dialogueNode.BlackboardDataVariables.boolVariables);
                    }
                    else if (pair is FloatVariable)
                    {
                        RefreshInspector(floatContainer, dialogueNode.BlackboardDataVariables.floatVariables);
                    }
                    else if (pair is IntVariable)
                    {
                        RefreshInspector(intContainer, dialogueNode.BlackboardDataVariables.intVariables);
                    }
                    EditorUtility.SetDirty(nodeInspector.curDialogueNode);
                })
                { text = "×", style = { width = 20, height = 20 } };

                item.Add(deleteButton);
                container.Add(item);
            }
        }


        private VisualElement AddVariableList<T>(string title, List<T> variables)
        {
            var foldout = new Foldout { text = title, value = false };

            foreach (var pair in variables)
            {
                var item = new VisualElement
                {
                    style = {
                        flexDirection = FlexDirection.Row,
                        marginTop = 2
                    }
                };

                if (pair is BoolVariable)
                {
                    BoolVariable tpair = pair as BoolVariable;
                    item.Add(new Label($"{tpair.variableName}: {tpair.value}"));
                }
                else if (pair is IntVariable)
                {
                    IntVariable intVariable = pair as IntVariable;
                    item.Add(new Label($"{intVariable.variableName}: {intVariable.value}"));
                }
                else if (pair is FloatVariable)
                {
                    FloatVariable floatVariable = pair as FloatVariable;
                    item.Add(new Label($"{floatVariable.variableName}: {floatVariable.value}"));
                }

                var deleteButton = new Button(() =>
                {
                    variables.Remove(pair);
                    if (selectedType == VariableType.Bool)
                    {
                        RefreshInspector(boolContainer, dialogueNode.BlackboardDataVariables.boolVariables);
                    }
                    else if (selectedType == VariableType.Float)
                    {
                        RefreshInspector(floatContainer, dialogueNode.BlackboardDataVariables.floatVariables);
                    }
                    else if (selectedType == VariableType.Int)
                    {
                        RefreshInspector(intContainer, dialogueNode.BlackboardDataVariables.intVariables);
                    }
                    EditorUtility.SetDirty(nodeInspector.curDialogueNode);
                })
                { text = "×", style = { width = 20, height = 20 } };

                item.Add(deleteButton);
                foldout.Add(item);
            }

            return foldout;
        }


        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {

        }
    }
}
