﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Pluliter.Background;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 背景切换检查器
    /// </summary>
    public class JumpToChapterInspector : INodeInspector
    {
        private DialogueNodeInspector nodeInspector;
        public JumpToChapterInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
        }

        public void OnInspectorGUI()
        {
            var dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as JumpToChapterNodeView;

            var bgDropdown = new DropdownField("章节");
            List<string> chapters = new List<string>();
            for (int i = 0; i < nodeInspector.config.dialogueDataList.Count; i++)
            {
                chapters.Add(nodeInspector.config.dialogueDataList[i].ChapterName);
            }
            bgDropdown.choices = chapters;
            bgDropdown.value = dialogueNode.JumpChapterId;
            bgDropdown.RegisterValueChangedCallback(e =>
            {
                dialogueNode.JumpChapterId = e.newValue;
                dialogueNodeView.UpdateNodeTitle();

            });
            nodeInspector.Add(bgDropdown);
        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {

        }
    }
}
