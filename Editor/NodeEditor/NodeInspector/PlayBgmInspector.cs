﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Audio;
using System.Linq;
using UnityEngine.UIElements;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 播放背景音乐检查器
    /// </summary>
    public class PlayBgmInspector : INodeInspector
    {
        private readonly DialogueNodeInspector nodeInspector;

        private readonly BgmTable bgmTable;

        public PlayBgmInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
            bgmTable = nodeInspector.bgmTable;
        }

        public void OnInspectorGUI()
        {
            var dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as PlayBgmNodeView;

            var bgmDropdown = new DropdownField("BGM ID");
            bgmDropdown.choices = bgmTable.GetBgmNames();

            if (string.IsNullOrEmpty(dialogueNode.BgmId))
            {
                bgmDropdown.value = nodeInspector.bgmTable.GetBgmNames().FirstOrDefault();
            }
            else
            {
                bgmDropdown.value = dialogueNode.BgmId;
            }
            dialogueNodeView.UpdateNodeTitle();

            bgmDropdown.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.BgmId = e.newValue;
                dialogueNodeView.UpdateNodeTitle();
            });

            AddElement(bgmDropdown);

            var loopField = new Toggle("背景音乐循环");
            loopField.value = dialogueNode.LoopBgm;
            loopField.RegisterValueChangedCallback(e => {
                dialogueNode.LoopBgm = e.newValue;
                dialogueNodeView.UpdateNodeTitle();
            });
            AddElement(loopField);

        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {
            
        }
    }
}
