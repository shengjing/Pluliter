﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Dialogue;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 选择对话节点检查器
    /// </summary>
    public class OptionDialogueInspector : INodeInspector
    {
        private readonly DialogueNodeInspector nodeInspector;

        /// <summary>
        /// 选项
        /// </summary>
        private ListView optionListView;

        /// <summary>
        /// 选项元素
        /// </summary>
        private List<DialogueOption> optItems = new();

        public OptionDialogueInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void OnInspectorGUI()
        {
            //nodeInspector.Clear();
            var curDialogueNode = nodeInspector.curDialogueNode;
            // 新建选项按钮
            var addButton = new Button(() =>
            {
                if (curDialogueNode.DialogueOptions == null || curDialogueNode.DialogueOptions.Count == 0)
                {
                    curDialogueNode.DialogueOptions = new();
                }
                var newOption = new DialogueOption
                {
                    optionText = "新选项" + curDialogueNode.DialogueOptions.Count.ToString(),
                    optionID = curDialogueNode.DialogueOptions.Count.ToString()
                };
                curDialogueNode.DialogueOptions.ForEach(option =>
                {
                    if (option.optionID == newOption.optionID)
                    {
                        newOption.optionID = (curDialogueNode.DialogueOptions.Count + 1).ToString();
                    }
                }
                );
                curDialogueNode.DialogueOptions.Add(newOption);

                optItems.Add(newOption);

                var element = MakeDialogueOptionsItem();
                BindDialogueOption(element, optItems.Count - 1);
                optionListView?.Rebuild();

                var dialogueNodeView = nodeInspector.curNodeview as OptionNodeView;
                dialogueNodeView.CreateOutputPorts(newOption.optionID);

            })
            {
                text = "添加选项"
            };
            AddElement(addButton);

            optionListView = CreateListView();

            optionListView.style.marginTop = 5f;
            AddElement(optionListView);
        }

        private ListView CreateListView()
        {
            // 定义每个列表项的高度
            int itemHeight = 100;

            var curDialogueNode = nodeInspector.curDialogueNode;
            ClearListView();
            optItems = new List<DialogueOption>();
            if (curDialogueNode.DialogueOptions == null || curDialogueNode.DialogueOptions.Count == 0)
            {
                Debug.Log("对话选项为空");
                //return new ListView(optItems, itemHeight, MakeDialogueOptionsItem, BindDialogueOption);
            }
            else
            {
                foreach (var option in curDialogueNode.DialogueOptions)
                {
                    optItems.Add(option);
                }
            }


            // 创建ListView并设置其属性
            optionListView = new ListView(optItems, itemHeight, MakeDialogueOptionsItem, BindDialogueOption)
            {
                showAlternatingRowBackgrounds = AlternatingRowBackground.None,
                // reorderable = true // 允许拖拽重新排序
            };

            // 添加选择改变事件处理
            //optionListView.selectionChanged += OnSelectionChanged;

            //optionListView.itemsChosen += objects => Debug.Log("Items chosen: " + objects.ToString());
            //// 将ListView添加到窗口的根视觉元素中
            ////Add(optionListView);
            return optionListView;
        }

        private void ClearListView()
        {
            optItems.Clear();
            optionListView?.Clear();

        }

        //private void OnSelectionChanged(IEnumerable<object> enumerable)
        //{
        //    Debug.Log("Selection changed" + enumerable.ToString());
        //    // 获取index
        //    var index = optionListView.selectedIndex;
        //    Debug.Log("index" + index);
        //}

        private VisualElement MakeDialogueOptionsItem()
        {
            var container = new VisualElement();

            // 设置横向布局
            container.style.flexDirection = FlexDirection.Column;

            var idContainer = new VisualElement();
            idContainer.style.flexDirection = FlexDirection.Row;

            var optLabel = new Label
            {
                name = "optionIDLabel",
                text = "选项ID",
                style = { width = 50, height = 20, marginRight = 2, marginLeft = 10 }
            };
            idContainer.Add(optLabel);

            var optionId = new Label
            {
                name = "optionID",
                text = "",
                style = { width = 50, height = 20, marginRight = 5, marginBottom = 5 }
            };
            idContainer.Add(optionId);


            var textContainer = new VisualElement();
            textContainer.style.flexDirection = FlexDirection.Row;

            var optLabel2 = new Label
            {
                name = "optionTextLabel",
                text = "选项文本",
                style = { width = 50, height = 20, marginRight = 10 }
            };
            textContainer.Add(optLabel2);

            var optionText = new TextField
            {
                name = "optionText",
                style = { width = 360, height = 20, marginRight = 20, marginBottom = 5 }
            };
            textContainer.Add(optionText);

            optionText.RegisterValueChangedCallback(e =>
            {
                nodeInspector.curDialogueNode.DialogueOptions.ForEach(option =>
                {
                    if (option.optionID == optionId.text)
                    {
                        option.optionText = e.newValue;
                    }
                });
                var dialogueNodeView = nodeInspector.curNodeview as OptionNodeView;
                dialogueNodeView.ChangeOutputPortName(e.newValue, optionId.text);
            });

            var dialogueNodeView = nodeInspector.curNodeview as OptionNodeView;

            var deleteButton = new Button(() =>
            {
                RemoveDialogueOptions(optionId);
                dialogueNodeView.DeleteOutputPorts(optionId.text);
            })
            {
                text = "删除"
            };

            container.Add(idContainer);
            container.Add(textContainer);
            container.Add(deleteButton);
            return container;
        }

        /// <summary>
        /// 从列表中移除选项
        /// </summary>
        /// <param name="textField"></param>
        private void RemoveDialogueOptions(Label label)
        {
            var curDialogueNode = nodeInspector.curDialogueNode;
            // 找到对应的DialogueOption并从列表中移除它
            var itemToRemove = optItems.FirstOrDefault(item => item.optionID == label.text);
            if (itemToRemove != null)
            {
                optItems.Remove(itemToRemove);
                optionListView.Rebuild(); // 刷新ListView以反映更改

                if (curDialogueNode != null)
                {
                    curDialogueNode.DialogueOptions.Remove(itemToRemove);
                }
            }
        }

        /// <summary>
        /// 将数据绑定到列表项上
        /// </summary>
        /// <param name="e"></param>
        /// <param name="index"></param>
        private void BindDialogueOption(VisualElement e, int index)
        {
            if (optItems != null && index >= 0 && index < optItems.Count)
            {
                var item = optItems[index];
                var idField = e.Query<Label>(nodeInspector.name = "optionID").First();
                var textField = e.Query<TextField>(nodeInspector.name = "optionText").First();

                if (item != null)
                {
                    // 绑定数据到TextField
                    idField.text = item.optionID.ToString();
                    textField.value = item.optionText;
                }
                else
                {
                    Debug.LogError("所选选项元素为空" + index);
                }
            }
            else
            {
                Debug.LogError("选项索引超出范围或项目列表为空");
            }
        }

        public void OnDestroy()
        {
            
        }
    }
}
