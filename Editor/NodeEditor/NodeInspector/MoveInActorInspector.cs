﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Actor;
using Pluliter.Character;
using Pluliter.Dialogue;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 演员入场节点检视器
    /// </summary>
    public class MoveInActorInspector : INodeInspector
    {
        private readonly DialogueNodeInspector nodeInspector;
        private readonly CharacterTable characterTable;

        /// <summary>
        /// 是否预览编辑模式
        /// </summary>
        private bool isEditMode = false;

        /// <summary>
        /// 预览演员
        /// </summary>
        private GameObject currentPreviewActor;

        public MoveInActorInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
            this.characterTable = nodeInspector.characterTable;

        }

        public void OnInspectorGUI()
        {
            var dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as MoveInActorNodeView;

            var characterDropdown = new DropdownField("角色")
            {
                choices = characterTable.GetCharacterNames()
            };


            var stateDropdown = new DropdownField("状态");
            if (!string.IsNullOrEmpty(dialogueNode.CharacterId))
            {
                stateDropdown.choices = characterTable.GetCharacterByName(dialogueNode.CharacterId).GetCharacterStateNames();
                stateDropdown.value = dialogueNode.CharacterStateId;
            }
            stateDropdown.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.CharacterStateId = e.newValue;
                dialogueNodeView.UpdateNodeTitle();
            });
            stateDropdown.style.marginBottom = 10f;

            if (characterDropdown.choices.Count > 0)
            {
                if (dialogueNode.CharacterId == null)
                {
                    characterDropdown.value = characterTable.GetCharacterNames().First();
                    dialogueNode.CharacterId = characterTable.GetCharacterByName(characterDropdown.value).CharacterName;
                }
                else
                {
                    characterDropdown.value = dialogueNode.CharacterId;
                }
            }
            characterDropdown.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.CharacterId = characterTable.GetCharacterByName(characterDropdown.value).CharacterName;
                Debug.Log($"CharacterId: {dialogueNode.CharacterId}");
                stateDropdown.choices = characterTable.GetCharacterByName(characterDropdown.value).GetCharacterStateNames();
                Debug.Log($"StateChoices: {string.Join(",", stateDropdown.choices)}");
                if (stateDropdown.choices.Count > 0)
                {
                    stateDropdown.value = stateDropdown.choices.First();
                }
                else
                {
                    dialogueNode.CharacterId = null;
                    stateDropdown.value = null;
                }

                dialogueNodeView.UpdateNodeTitle();
            });
            characterDropdown.style.marginBottom = 10f;
            AddElement(characterDropdown);
            AddElement(stateDropdown);


            var scaleField = new FloatField("缩放")
            {
                value = dialogueNode.ActorScale
            };
            scaleField.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.ActorScale = e.newValue;
            });
            scaleField.style.marginBottom = 10f;
            AddElement(scaleField);

            var xField = new FloatField("X")
            {
                value = dialogueNode.ActorPosition.x
            };
            xField.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.ActorPosition = new Vector2(e.newValue, dialogueNode.ActorPosition.y);
            });

            xField.style.marginBottom = 10;
            AddElement(xField);

            var yField = new FloatField("Y")
            {
                value = dialogueNode.ActorPosition.y
            };
            yField.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.ActorPosition = new Vector2(dialogueNode.ActorPosition.x, e.newValue);
            });
            yField.style.marginBottom = 10;
            AddElement(yField);

            var moveinAnimationDropdown = new DropdownField("入场动画");
            List<string> moveinAnimations = new();
            foreach (MoveInAnimation type in Enum.GetValues(typeof(MoveInAnimation)))
            {
                // 获取枚举值的类型 获取中文名称
                var memberInfo = type.GetType().GetMember(type.ToString());
                if (memberInfo != null && memberInfo.Length > 0)
                {
                    var attrs = memberInfo[0].GetCustomAttributes(typeof(InspectorNameAttribute), false);
                    if (attrs.Length > 0)
                    {
                        if (attrs[0] is InspectorNameAttribute attr)
                        {
                            var name = attr.displayName;
                            moveinAnimations.Add(name);
                        }
                    }
                }
            }
            moveinAnimationDropdown.choices = moveinAnimations;
            moveinAnimationDropdown.value = moveinAnimations[(int)dialogueNode.ActorMoveInAnimationType];
            moveinAnimationDropdown.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.ActorMoveInAnimationType = (MoveInAnimation)moveinAnimations.IndexOf(e.newValue);
            });
            moveinAnimationDropdown.style.marginBottom = 10f;
            AddElement(moveinAnimationDropdown);

            var durationField = new FloatField("入场时间")
            {
                value = dialogueNode.AnimationDuration
            };
            durationField.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.AnimationDuration = e.newValue;
                dialogueNodeView.UpdateNodeTitle();
            });
            durationField.style.marginBottom = 10f;
            AddElement(durationField);

            // 预览编辑位置

            var previewEditElement = new VisualElement();

            var confirmPreviewPosBtn = new Button
            {
                text = "保存位置",
                style = { marginLeft = 10, marginRight = 10 }
            };
            confirmPreviewPosBtn.clicked += () =>
            {
                dialogueNode.ActorPosition = new Vector2(currentPreviewActor.transform.position.x, currentPreviewActor.transform.position.y);
                yField.value = dialogueNode.ActorPosition.y;
                xField.value = dialogueNode.ActorPosition.x;
            };
            confirmPreviewPosBtn.style.visibility = isEditMode ? Visibility.Visible : Visibility.Hidden;

            var btnText = isEditMode ? "关闭预览" : "预览编辑位置";

            var previewBtn = new Button
            {
                text = btnText,
                style = { marginTop = 10 , marginLeft = 10, marginRight = 10 }
            };
            previewBtn.clicked += () =>
            {
                TogglePreviewEditMode();
                btnText = isEditMode ? "关闭预览" : "预览编辑位置";
                previewBtn.text = btnText;
                confirmPreviewPosBtn.style.visibility = isEditMode ? Visibility.Visible : Visibility.Hidden;
            };
            previewEditElement.Add(previewBtn);
            previewEditElement.Add(confirmPreviewPosBtn);

            AddElement(previewEditElement);
        }

        /// <summary>
        /// 切换编辑模式
        /// </summary>
        private void TogglePreviewEditMode()
        {
            isEditMode = !isEditMode;
            SceneView.RepaintAll();

            if (isEditMode)
            {
                CreatePreviewActor();
                SceneView.duringSceneGui += OnSceneGUI;
            }
            else
            {
                DestroyPreviewActor();
                SceneView.duringSceneGui -= OnSceneGUI;
            }
        }

        private void OnSceneGUI(SceneView sceneView)
        {
            if (!isEditMode || !currentPreviewActor)
            {
                return;
            }

            Event e = Event.current;
            //Vector2 mousePos = e.mousePosition;
            //float pixelsPerPoint = EditorGUIUtility.pixelsPerPoint;
            //mousePos.y = sceneView.camera.pixelHeight - mousePos.y * pixelsPerPoint;
            //mousePos /= pixelsPerPoint;

            //Vector3 worldPos = sceneView.camera.ScreenToWorldPoint(mousePos);
            //worldPos.z = 0;

            //if (e.type == EventType.MouseDrag)
            //{
            //    currentPreviewActor.transform.position = worldPos;
            //    SceneView.RepaintAll();
            //}

            Vector3 newWorldPos = Handles.PositionHandle(currentPreviewActor.transform.position, Quaternion.identity);
            if (e.type == EventType.Repaint)
            {
                currentPreviewActor.transform.position = newWorldPos;
            }
        }

        /// <summary>
        /// 创建预览演员
        /// </summary>
        private void CreatePreviewActor()
        {
#if UNITY_2023_1_OR_NEWER
            ActorManager actorManager = UnityEngine.Object.FindFirstObjectByType<ActorManager>();
#else
            ActorManager actorManager = UnityEngine.Object.FindObjectOfType<ActorManager>();
#endif

            if (actorManager != null)
            {
                var ActorLayer = actorManager.gameObject;

                if (ActorLayer.transform.position.x != 0 || ActorLayer.transform.position.y != 0)
                {
                    Debug.LogWarning("ActorManager组件的物体x和y位置不为(0,0)，可能会导致预览位置错误");
                }
                currentPreviewActor = new GameObject("预览演员");
                currentPreviewActor.transform.SetParent(ActorLayer.transform);

                currentPreviewActor.transform.position = new Vector3(nodeInspector.curDialogueNode.ActorPosition.x, nodeInspector.curDialogueNode.ActorPosition.y, 0);

                currentPreviewActor.transform.localScale = new Vector3(nodeInspector.curDialogueNode.ActorScale, nodeInspector.curDialogueNode.ActorScale, 0.5f);

                //currentPreviewActor.hideFlags = HideFlags.HideInHierarchy;

                var renderer = currentPreviewActor.AddComponent<SpriteRenderer>();
                var tmpSprite = characterTable.GetCharacterByName(nodeInspector.curDialogueNode.CharacterId).GetCharacterState(nodeInspector.curDialogueNode.CharacterStateId).StateSprite;
                if (tmpSprite != null)
                {
                    renderer.sprite = tmpSprite;
                }
                else
                {
                    Debug.LogError("未找到演员Sprite");
                }

                // 在场景中选中
                Selection.activeGameObject = currentPreviewActor;

            }
            else
            {
                EditorUtility.DisplayDialog("多文", "未找到ActorManager组件的物体，无法预览编辑", "确认");
                Debug.Log("未找到ActorManager组件的物体，无法预览编辑");
            }
        }

        /// <summary>
        /// 销毁预览对象
        /// </summary>
        private void DestroyPreviewActor()
        {
            if (currentPreviewActor)
            { 
                UnityEngine.Object.DestroyImmediate(currentPreviewActor); 
            }
        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {
            DestroyPreviewActor();
        }
    }
}
