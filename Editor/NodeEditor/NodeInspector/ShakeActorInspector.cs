﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Character;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 移动演员节点检视器
    /// </summary>
    public class ShakeActorInspector : INodeInspector
    {
        private readonly DialogueNodeInspector nodeInspector;
        private readonly CharacterTable characterTable;

        public ShakeActorInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
            this.characterTable = nodeInspector.characterTable;
        }

        public void OnInspectorGUI()
        {
            var dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as ShakeActorNodeView;
            // shake actor="id" duration="0.5" rate="2"

            var characterDropdown = new DropdownField("演员")
            {
                choices = characterTable.GetCharacterNames()
            };
            if (dialogueNode.CharacterId == null)
            {
                characterDropdown.value = characterTable.GetCharacterNames().First();
                dialogueNode.CharacterId = characterTable.GetCharacterByName(characterDropdown.value).CharacterName;
                dialogueNodeView.UpdateNodeTitle();
            }
            else
            {
                characterDropdown.value = dialogueNode.CharacterId;
                dialogueNodeView.UpdateNodeTitle();
            }
            characterDropdown.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.CharacterId = characterTable.GetCharacterByName(characterDropdown.value).CharacterName;
                dialogueNodeView.UpdateNodeTitle();
            });
            characterDropdown.style.marginBottom = 10f;

            AddElement(characterDropdown);

            var durationField = new FloatField("晃动时间")
            {
                value = dialogueNode.AnimationDuration
            };
            durationField.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.AnimationDuration = e.newValue;
                dialogueNodeView.UpdateNodeTitle();
            });
            durationField.style.marginBottom = 10f;
            AddElement(durationField);

            //var rateField = new FloatField("晃动速率");
            //rateField.value = dialogueNode.AnimationRate;
            //rateField.RegisterValueChangedCallback((e) =>
            //{
            //    dialogueNode.AnimationRate = e.newValue;
            //    dialogueNodeView.UpdateNodeTitle();
            //});
            //rateField.style.marginBottom = 10f;
            //AddElement(rateField);

        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {
            
        }
    }
}
