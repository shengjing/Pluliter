﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Audio;
using System.Linq;
using UnityEngine.UIElements;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 播放背景音乐检查器
    /// </summary>
    public class PlaySoundEffectInspector : INodeInspector
    {
        private readonly DialogueNodeInspector nodeInspector;

        private readonly SoundEffectTable soundEffectTable;

        public PlaySoundEffectInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
            soundEffectTable = nodeInspector.soundEffectTable;
        }

        public void OnInspectorGUI()
        {
            var dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as PlaySoundEffectNodeView;

            var seDropDown = new DropdownField("音效ID")
            {
                choices = soundEffectTable.GetSoundEffectNames()
            };

            if (string.IsNullOrEmpty(dialogueNode.SoundEffectId))
            {
                seDropDown.value = soundEffectTable.GetSoundEffectNames().FirstOrDefault();
            }
            else
            {
                seDropDown.value = dialogueNode.SoundEffectId;
            }
            dialogueNodeView.UpdateNodeTitle();

            seDropDown.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.SoundEffectId = e.newValue;
                dialogueNodeView.UpdateNodeTitle();
            });
            
            AddElement(seDropDown);
        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {

        }
    }
}
