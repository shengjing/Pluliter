﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Actor;
using Pluliter.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 演员入场节点检视器
    /// </summary>
    public class MoveOutActorInspector : INodeInspector
    {
        private readonly DialogueNodeInspector nodeInspector;
        private readonly CharacterTable characterTable;

        public MoveOutActorInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
            this.characterTable = nodeInspector.characterTable;
        }

        public void OnInspectorGUI()
        {
            var dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as MoveOutActorNodeView;

            dialogueNodeView.UpdateNodeTitle();

            // moveout actor = "id" anim="fadeout" duration="0.5"

            var characterDropdown = new DropdownField("角色")
            {
                choices = characterTable.GetCharacterNames()
            };
            if (dialogueNode.CharacterId == null)
            {
                characterDropdown.value = characterTable.GetCharacterNames().First();
                dialogueNode.CharacterId = characterTable.GetCharacterByName(characterDropdown.value).CharacterName;
                dialogueNodeView.UpdateNodeTitle();
            }
            else
            {
                characterDropdown.value = dialogueNode.CharacterId;
                dialogueNodeView.UpdateNodeTitle();
            }
            characterDropdown.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.CharacterId = characterTable.GetCharacterByName(characterDropdown.value).CharacterName;
                dialogueNodeView.UpdateNodeTitle();
            });
            characterDropdown.style.marginBottom = 10f;
            AddElement(characterDropdown);

            var moveoutAnimationDropdown = new DropdownField("动画");

            List<string> moveoutAnimations = new();

            // 遍历枚举值，获取中文名称
            foreach (MoveOutAnimation type in Enum.GetValues(typeof(MoveOutAnimation)))
            {
                // 获取枚举值的类型 获取中文名称
                var memberInfo = type.GetType().GetMember(type.ToString());
                if (memberInfo != null && memberInfo.Length > 0)
                {
                    var attrs = memberInfo[0].GetCustomAttributes(typeof(InspectorNameAttribute), false);
                    if (attrs.Length > 0)
                    {
                        if (attrs[0] is InspectorNameAttribute attr)
                        {
                            var name = attr.displayName;
                            moveoutAnimations.Add(name);
                        }
                    }
                }
            }
            moveoutAnimationDropdown.choices = moveoutAnimations;
            moveoutAnimationDropdown.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.ActorMoveOutAnimationType = (MoveOutAnimation)moveoutAnimations.IndexOf(e.newValue);
            });
            moveoutAnimationDropdown.value = moveoutAnimations[(int)dialogueNode.ActorMoveOutAnimationType];
            moveoutAnimationDropdown.style.marginBottom = 10f;
            AddElement(moveoutAnimationDropdown);

            var durationField = new FloatField("退场时间")
            {
                value = dialogueNode.AnimationDuration
            };
            durationField.RegisterValueChangedCallback((e) =>
            {
                dialogueNode.AnimationDuration = e.newValue;
                dialogueNodeView.UpdateNodeTitle();
            });
            durationField.style.marginBottom = 10f;
            AddElement(durationField);

        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {
            
        }
    }
}
