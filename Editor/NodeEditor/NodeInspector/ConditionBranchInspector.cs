﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine;
using UnityEngine.UIElements;
using System;
using System.Collections.Generic;
using UnityEditor;
using Pluliter.Blackboard;
using Pluliter.Dialogue;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 条件分支节点检视器
    /// </summary>
    public class ConditionBranchInspector : INodeInspector
    {
        private CompositeCondition target;
        private DialogueNodeInspector nodeInspector;

        private ScrollView scrollView;

        public ConditionBranchInspector(DialogueNodeInspector dialogueNodeInspector)
        {
            nodeInspector = dialogueNodeInspector;
            target = dialogueNodeInspector.curDialogueNode.ConditionBranch;

        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void OnInspectorGUI()
        {
            scrollView = new ScrollView();
            
            // 布尔条件
            CreateConditionSection(
                "布尔条件",
                target.boolConditions,
                () =>
                {
                    target.boolConditions.Add(new BoolCondition());
                },
                (index) => target.boolConditions.RemoveAt(index),
                (condition, index) => CreateBoolConditionUI(condition, index)
            );

            // 浮点条件
            CreateConditionSection(
                "浮点条件",
                target.floatConditions,
                () =>
                {
                    target.floatConditions.Add(new FloatCondition());
                },
                (index) =>
                {
                    target.floatConditions.RemoveAt(index);
                },
                (condition, index) => CreateFloatConditionUI(condition, index)
            );

            // 整数条件
            CreateConditionSection(
                "整数条件",
                target.intConditions,
                () => 
                { 
                    target.intConditions.Add(new IntegerCondition());
                },
                (index) => target.intConditions.RemoveAt(index),
                (condition, index) => CreateIntConditionUI(condition, index)
            );
        }

        private void CreateConditionSection<T>(
            string title,
            List<T> conditions,
            Action addAction,
            Action<int> removeAction,
            Func<T, int, VisualElement> createUI
        ) where T : IConditionChecker
        {
            Foldout section = new Foldout() { text = title };
            Button addButton = new Button(() =>
            {
                addAction();
            }) 
            { 
                text = $"添加 {title}" 
            };
            addButton.style.marginBottom = 10;
            VisualElement conditionsContainer = new VisualElement();
            conditionsContainer.style.marginBottom = 10;

            Action refresh = () =>
            {

            };
            // 刷新UI的方法
            refresh = () =>
            {
                conditionsContainer.Clear();
                for (int i = 0; i < conditions.Count; i++)
                {
                    VisualElement conditionUI = createUI(conditions[i], i);
                    Button deleteBtn = new Button(() =>
                    {
                        // 获取conditionUI中的indexLabel
                        Label indexLabel = conditionUI.Q<Label>("indexLabel");
                        int index = int.Parse(indexLabel.text);

                        removeAction(index);
                        refresh();
                    })
                    { text = "删除" };

                    conditionUI.Add(deleteBtn);
                    conditionsContainer.Add(conditionUI);
                }
            };

            addButton.clicked += () =>
            {
                refresh();
            };

            section.Add(addButton);
            section.Add(conditionsContainer);
            scrollView.Add(section);
            nodeInspector.Add(scrollView);
            refresh();


        }

        private VisualElement CreateBoolConditionUI(BoolCondition condition, int index)
        {
            VisualElement container = new VisualElement()
            { style = { flexDirection = FlexDirection.Column, marginTop = 10 } };

            VisualElement indexContainer = new VisualElement()
            { style = { flexDirection = FlexDirection.Row } };
            Label indexTitle = new Label("条件ID");
            indexContainer.Add(indexTitle);

            Label indexLabel = new Label(index.ToString());
            indexLabel.name = "indexLabel";
            indexContainer.Add(indexLabel);

            TextField variableField = new TextField() { label = "变量名", value = condition.variableName };
            variableField.RegisterValueChangedCallback(evt => condition.variableName = evt.newValue);

            Toggle valueToggle = new Toggle("期望值") { value = condition.expectedValue };
            valueToggle.RegisterValueChangedCallback(evt => condition.expectedValue = evt.newValue);

            container.Add(indexContainer);
            container.Add(variableField);
            container.Add(valueToggle);
            return container;
        }

        private VisualElement CreateFloatConditionUI(FloatCondition condition, int index)
        {
            VisualElement container = new VisualElement()
            { style = { flexDirection = FlexDirection.Column, marginTop = 10 } };

            VisualElement indexContainer = new VisualElement()
            { style = { flexDirection = FlexDirection.Row } };
            Label indexTitle = new Label("条件ID");
            indexContainer.Add(indexTitle);

            Label indexLabel = new Label(index.ToString());
            indexLabel.name = "indexLabel";
            indexContainer.Add(indexLabel);

            TextField variableField = new TextField() { label = "变量名", value = condition.variableName };
            variableField.RegisterValueChangedCallback(evt => condition.variableName = evt.newValue);

            EnumField compareTypeField = new EnumField("比较", condition.compareType);
            compareTypeField.RegisterValueChangedCallback(evt =>
                condition.compareType = (CompareType)evt.newValue);

            FloatField valueField = new FloatField("比较值") { value = condition.compareValue };
            valueField.RegisterValueChangedCallback(evt => condition.compareValue = evt.newValue);

            container.Add(indexContainer);
            container.Add(variableField);
            container.Add(compareTypeField);
            container.Add(valueField);
            return container;
        }

        private VisualElement CreateIntConditionUI(IntegerCondition condition, int index)
        {
            VisualElement container = new VisualElement()
            { style = { flexDirection = FlexDirection.Column, marginTop = 10 } };

            VisualElement indexContainer = new VisualElement()
            { style = { flexDirection = FlexDirection.Row } };
            Label indexTitle = new Label("条件ID");
            indexContainer.Add(indexTitle);

            Label indexLabel = new Label(index.ToString());
            indexLabel.name = "indexLabel";
            indexContainer.Add(indexLabel);

            TextField variableField = new TextField() { label = "变量名", value = condition.variableName };
            variableField.RegisterValueChangedCallback(evt => condition.variableName = evt.newValue);

            EnumField compareTypeField = new EnumField("比较", condition.compareType);
            compareTypeField.RegisterValueChangedCallback(evt =>
                condition.compareType = (CompareType)evt.newValue);

            IntegerField valueField = new IntegerField("比较值") { value = condition.compareValue };
            valueField.RegisterValueChangedCallback(evt => condition.compareValue = evt.newValue);

            container.Add(indexContainer);
            container.Add(variableField);
            container.Add(compareTypeField);
            container.Add(valueField);
            return container;
        }


        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {

        }
    }
}