﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Dialogue;
using UnityEngine.UIElements;
using UnityEditor.Experimental.GraphView;
using Pluliter.Character;
using Pluliter.Background;
using UnityEditor;
using UnityEngine;
using System;
using Pluliter.Audio;
namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 对话节点检查器元素
    /// </summary>
#if UNITY_2023
    [UxmlElement]
#endif
    public partial class DialogueNodeInspector : VisualElement
    {
#if UNITY_2022
        public new class UxmlFactory : UxmlFactory<DialogueNodeInspector> { }
#endif

        /// <summary>
        /// 当前编辑的对话节点
        /// </summary>
        public DialogueNode curDialogueNode = null;

        /// <summary>
        /// 角色表
        /// </summary>
        public CharacterTable characterTable = null;

        /// <summary>
        /// 背景表
        /// </summary>
        public BackgroundTable backgroundTable = null;

        /// <summary>
        /// 背景音乐表
        /// </summary>
        public BgmTable bgmTable = null;

        /// <summary>
        /// 音效表
        /// </summary>
        public SoundEffectTable soundEffectTable = null;

        /// <summary>
        /// 对话配置
        /// </summary>
        public DialogueConfig config = null;

        /// <summary>
        /// 当检查器被销毁时的回调
        /// </summary>
        public Action OnElementDestroyed;

        /// <summary>
        /// 当前编辑的节点视图
        /// </summary>
        public Node curNodeview;

        private INodeInspector nodeInspector;

        public DialogueNodeInspector()
        {
            style.maxWidth = 400;
            style.marginLeft = 5;
            style.marginRight = 5;
            SetDialogueNode(null, null); // 初始化为未选择对话节点
        }


        /// <summary>
        /// 设置当前对话节点
        /// </summary>
        /// <param name="node">对话节点</param>
        /// <param name="nodeView">对话节点视图</param>
        public void SetDialogueNode(DialogueNode node, Node nodeView, CharacterTable characterTable = null, BackgroundTable backgroundTable = null, BgmTable bgmTable = null, SoundEffectTable soundEffectTable = null,  DialogueConfig dialogueConfig = null)
        {

            if (nodeInspector != null)
            {
                nodeInspector.OnDestroy();
                nodeInspector = null;
            }

            curDialogueNode = node;
            this.characterTable = characterTable;
            this.backgroundTable = backgroundTable;
            this.bgmTable = bgmTable;
            this.soundEffectTable = soundEffectTable;
            this.config = dialogueConfig;

            Clear(); // 清除所有子元素

            var dialogueNodeView = nodeView;
            if (dialogueNodeView != null)
            {
                this.curNodeview = dialogueNodeView;
            }

            if (curDialogueNode == null)
            {
                Add(new Label("未选择对话节点"));
            }
            else
            {
                var nodeInfoBox = new VisualElement();
                nodeInfoBox.style.flexDirection = FlexDirection.Row;

                var title = new Label("节点信息");
                title.style.marginRight = 10;
                nodeInfoBox.Add(title);

                var nodeInfo = new Label($"节点ID: {curDialogueNode.name}");
                nodeInfo.style.marginRight = 10;
                nodeInfoBox.Add(nodeInfo);

                nodeInfoBox.style.marginBottom = 10;
                Add(nodeInfoBox);

                var nodeOperationBox = new VisualElement();
                nodeOperationBox.style.flexDirection = FlexDirection.Row;

                // 打开节点资源按钮
                var openNodeButton = new Button(() => { AssetDatabase.OpenAsset(curDialogueNode); })
                {
                    text = "打开节点资源"
                };
                openNodeButton.style.marginRight = 2;
                nodeOperationBox.Add(openNodeButton);

                // 打印为json按钮
                var printJsonButton = new Button(() => { Debug.Log(JsonUtility.ToJson(curDialogueNode)); })
                {
                    text = "打印为Json"
                };
                printJsonButton.style.marginRight = 2;
                nodeOperationBox.Add(printJsonButton);

                nodeOperationBox.style.marginBottom = 10;

                Add(nodeOperationBox);

                if (curDialogueNode.NextDialogueNode != null)
                {
                    var nextNodeInfo = new Label($"下一节点: {curDialogueNode.NextDialogueNode.name}");
                    nextNodeInfo.style.marginBottom = 10;
                    Add(nextNodeInfo);
                }

                // 创建并显示对话检察器
                nodeInspector = NodeInspectorFactory.CreateInspector(this);
                if (nodeInspector != null)
                {
                    nodeInspector.OnInspectorGUI();
                }
                else
                {
                    Add(new Label("未找到节点检视器"));
                }
            }
        }
    }
}
