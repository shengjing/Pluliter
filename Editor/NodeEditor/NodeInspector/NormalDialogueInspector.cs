﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine;
using UnityEngine.UIElements;

namespace Pluliter.Editor.NodeEditor
{
    /// <summary>
    /// 普通对话检查器
    /// </summary>
    public class NormalDialogueInspector : INodeInspector
    {
        private DialogueNodeInspector nodeInspector;
        public NormalDialogueInspector(DialogueNodeInspector nodeInspector)
        {
            this.nodeInspector = nodeInspector;
        }

        public void OnInspectorGUI()
        {
            var dialogueNode = nodeInspector.curDialogueNode;
            var dialogueNodeView = nodeInspector.curNodeview as NormalDialogueNodeView;

            var characterNameLabel = new TextField("角色名称");
            characterNameLabel.value = dialogueNode.DialogueCharacterName;
            characterNameLabel.RegisterValueChangedCallback(evt =>
            {
                dialogueNode.DialogueCharacterName = evt.newValue;
            });
            AddElement(characterNameLabel);

            var pastecharaButton = new Button(() => { characterNameLabel.value = GUIUtility.systemCopyBuffer; });
            pastecharaButton.text = "快速粘贴姓名";
            AddElement(pastecharaButton);

            var dialogueContentField = new TextField("对话内容");
            dialogueContentField.value = dialogueNode.DialogueContent;
            dialogueContentField.style.height = 60;
            dialogueContentField.style.marginTop = 10;
            dialogueContentField.multiline = true;
            dialogueContentField.style.whiteSpace = WhiteSpace.Normal;

            dialogueContentField.RegisterValueChangedCallback(evt =>
            {
                dialogueNode.DialogueContent = evt.newValue;
                if (dialogueNodeView != null)
                {
                    dialogueNodeView.UpdateNodeTitle();
                }
            });
            AddElement(dialogueContentField);

            // 快速粘贴按钮
            var pasteButton = new Button(() => { dialogueContentField.value = GUIUtility.systemCopyBuffer; });
            pasteButton.text = "快速粘贴对话内容";
            AddElement(pasteButton);
        }

        public DialogueNodeInspector GetNodeInspector()
        {
            return nodeInspector;
        }

        public void AddElement(VisualElement element)
        {
            nodeInspector.Add(element);
        }

        public void OnDestroy()
        {
            
        }
    }
}
