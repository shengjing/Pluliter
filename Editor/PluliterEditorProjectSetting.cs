﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEditor;
using UnityEngine;

namespace Pluliter.Editor.Config
{
    /// <summary>
    /// 多文编辑器项目设置
    /// </summary>
    public class PluliterEditorProjectSetting
    {
        private static PluliterConfig config;

        private static SerializedObject SerConfig;

        private static SerializedProperty EnableErrorReport;

        private static SerializedProperty AnimEngine;

        private static SerializedProperty TypewriterEffect;

        private static SerializedProperty TypewriterFadeRange;


        private static readonly string[] settingKeyWords = new string[] { "Basic", "Pluliter", "多文", "对话", "对话系统" };

        private static void LoadConfig()
        {
            config = AssetDatabase.LoadAssetAtPath<PluliterConfig>("Assets/Resources/PluliterConfig.asset");
            if (config == null)
            {
                // 如果没有创建Pluliter文件夹
                if (!AssetDatabase.IsValidFolder("Assets/Resources"))
                {
                    AssetDatabase.CreateFolder("Assets", "Resources");
                }

                config = ScriptableObject.CreateInstance<PluliterConfig>();
                AssetDatabase.CreateAsset(config, "Assets/Resources/PluliterConfig.asset");
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }

        [SettingsProvider]
        public static SettingsProvider DebugSettingsProvider()
        {
            var provider = new SettingsProvider("Project/Pluliter/Basic", SettingsScope.Project,
                settingKeyWords)
            {
                label = "基础设置 Basic",
                activateHandler = (searchContext, rootElement) =>
                {
                    LoadConfig();
                    SerConfig = new SerializedObject(config);
                    EnableErrorReport = SerConfig.FindProperty("EnableErrorReport");
                },
                guiHandler = (searchContext) =>
                {
                    SerConfig.Update();
                    EditorGUILayout.PropertyField(EnableErrorReport);
                    SerConfig.ApplyModifiedProperties();

                }
            };
            return provider;
        }

        [SettingsProvider]
        public static SettingsProvider AnimSettingsProvider()
        {
            var provider = new SettingsProvider("Project/Pluliter/Anim", SettingsScope.Project,
                settingKeyWords)
            {
                label = "动画设置 Animation",
                activateHandler = (searchContext, rootElement) =>
                {
                    LoadConfig();
                    SerConfig = new SerializedObject(config);
                    AnimEngine = SerConfig.FindProperty("animationEngine");
                    TypewriterEffect = SerConfig.FindProperty("typewriterEffect");
                    TypewriterFadeRange = SerConfig.FindProperty("TypewriterFadeRange");


                },
                guiHandler = (searchContext) =>
                {
                    if (config != null && SerConfig != null)
                    {
                        SerConfig.Update();
                        EditorGUILayout.PropertyField(AnimEngine);
                        EditorGUILayout.Space();
                        EditorGUILayout.PropertyField(TypewriterEffect);

                        if (TypewriterEffect.enumValueIndex == 1)
                        {
                            EditorGUILayout.PropertyField(TypewriterFadeRange);
                        }
                        SerConfig.ApplyModifiedProperties();
                    }
                }
            };
            return provider;

        }

        [SettingsProvider]
        public static SettingsProvider AboutSettingsProvider()
        {
            var provider = new SettingsProvider("Project/Pluliter/About", SettingsScope.Project,
                new[] { "About", "Pluliter", "多文" })
            {
                label = "关于 About",
                guiHandler = (searchContext) =>
                {
                    EditorGUILayout.LabelField("多文 Pluliter", EditorStyles.boldLabel);
                    EditorGUILayout.LabelField("盏乐堂 Zhanle Hall");
                    EditorGUILayout.LabelField("DSOE1024");

                    EditorGUILayout.Space();

                    EditorGUILayout.BeginHorizontal();
                    if (GUILayout.Button("文档 Documentation", new GUILayoutOption[] { GUILayout.MinWidth(200), GUILayout.Height(40) }))
                    {
                        Application.OpenURL("https://gitee.com/zhanle-hall/pluliter/wikis");
                    }
                    
                    if (GUILayout.Button("快速入门 Quick Start", new GUILayoutOption[] { GUILayout.MinWidth(200), GUILayout.Height(40) }))
                    {
                        Application.OpenURL("https://gitee.com/zhanle-hall/pluliter/wikis/pages/Quick-Start");
                    }
                    
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    // Git链接
                    if (GUILayout.Button("源代码 Source Code", new GUILayoutOption[] { GUILayout.MinWidth(200), GUILayout.Height(30) }))
                    {
                        Application.OpenURL("https://gitee.com/zhanle-hall/pluliter");
                    }
                    
                    // 反馈
                    if (GUILayout.Button("反馈 Issues", new GUILayoutOption[] { GUILayout.MinWidth(200), GUILayout.Height(30) }))
                    {
                        Application.OpenURL("https://gitee.com/zhanle-hall/pluliter/issues");
                    }
                    EditorGUILayout.EndHorizontal();
                }
            };
            return provider;
        }
        
    }
}
