//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using Pluliter.Blackboard;

namespace Pluliter.Editor.DialogueBlackboardEditor
{
    /// <summary>
    /// 自定义的对话黑板编辑器
    /// </summary>
    [CustomEditor(typeof(DialogueBlackboard))]
    public class DialogueBlackboardEditor : UnityEditor.Editor
    {
        private bool showBoolVars = true;
        private bool showFloatVars = true;
        private bool showIntVars = true;

        public override void OnInspectorGUI()
        {
            DialogueBlackboard blackboard = (DialogueBlackboard)target;

            // 绘制默认的序列化属性
            DrawDefaultInspector();

            // 显示字典数据的可视化界面
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("黑板数据", EditorStyles.boldLabel);

            if (blackboard.data == null)
            {
                EditorGUILayout.HelpBox("数据为空，请检查序列化数据", MessageType.Warning);
                return;
            }

            // 显示布尔变量
            showBoolVars = EditorGUILayout.Foldout(showBoolVars, $"布尔值组 ({blackboard.data.boolVariables.Count})");
            if (showBoolVars)
            {
                DrawDictionary(blackboard.data.boolVariables);
            }

            // 显示浮点变量
            showFloatVars = EditorGUILayout.Foldout(showFloatVars, $"浮点值组 ({blackboard.data.floatVariables.Count})");
            if (showFloatVars)
            {
                DrawDictionary(blackboard.data.floatVariables);
            }

            // 显示整型变量
            showIntVars = EditorGUILayout.Foldout(showIntVars, $"整数值组 ({blackboard.data.intVariables.Count})");
            if (showIntVars)
            {
                DrawDictionary(blackboard.data.intVariables);
            }
        }

        private void DrawDictionary<T>(Dictionary<string, T> dictionary)
        {
            EditorGUI.indentLevel++;

            if (dictionary.Count == 0)
            {
                EditorGUILayout.LabelField("无数据");
                EditorGUI.indentLevel--;
                return;
            }

            foreach (KeyValuePair<string, T> pair in dictionary)
            {
                EditorGUILayout.BeginHorizontal();

                // 显示键名
                EditorGUILayout.LabelField(pair.Key, GUILayout.Width(150));

                // 显示不可编辑的值
                GUI.enabled = false;
                if (pair.Value is bool)
                {
                    EditorGUILayout.Toggle((bool)(object)pair.Value);
                }
                else if (pair.Value is float)
                {
                    EditorGUILayout.FloatField((float)(object)pair.Value);
                }
                else if (pair.Value is int)
                {
                    EditorGUILayout.IntField((int)(object)pair.Value);
                }
                GUI.enabled = true;

                EditorGUILayout.EndHorizontal();
            }
            EditorGUI.indentLevel--;
        }
    }
}