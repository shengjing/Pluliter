﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;
using UnityEditor;
using Pluliter.Dialogue;

namespace Pluliter.Editor
{
    /// <summary>
    /// 菜单项
    /// </summary>
    public class PluliterMenuItem : MonoBehaviour
    {
        [MenuItem("GameObject/多文Pluliter/视觉小说模板", false, 0)]
        private static void CreateVisualNovelTemplate(MenuCommand menuCommand)
        {
            // 加载预制件AssetDatabase.LoadAssetAtPath
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/com.zhanlehall.pluliter/Runtime/Prefab/PluliterVisualNovelManager.prefab");
            if (prefab == null)
            {
                Debug.LogError("Prefab not found");
                return;
            }
            GameObject obj = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
            if (obj == null)
            {
                Debug.LogError("Prefab instantiation failed");
            }
            // 完全解压缩预制件
            PrefabUtility.UnpackPrefabInstance(obj, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
            obj.name = "PluliterVisualNovelManager";
            obj.transform.parent = null;
            GameObjectUtility.SetParentAndAlign(obj, menuCommand.context as GameObject);
            // 允许撤销
            Undo.RegisterCreatedObjectUndo(obj, "Create " + obj.name);
            Selection.activeObject = obj;
        }

        //[MenuItem("GameObject/Pluliter/Dialogue Manager", false, 10)]
        //private static void CreateDialogueManager(MenuCommand menuCommand)
        //{
        //    GameObject obj = new GameObject("DialogueManager");
        //    obj.AddComponent<DialogueManager>();
        //    GameObjectUtility.SetParentAndAlign(obj, menuCommand.context as GameObject);
        //    // 允许撤销
        //    Undo.RegisterCreatedObjectUndo(obj, "Create " + obj.name);
        //    Selection.activeObject = obj;
        //}

        //[MenuItem("GameObject/Pluliter/Dialogue Box", false, 10)]
        //private static void CreateDialogueBox(MenuCommand menuCommand)
        //{
        //    // 如果menuCommand context是Canvas，报错
        //    var parent = menuCommand.context as GameObject;
        //    //if (menuCommand.context == null && parent.GetComponent<Canvas>() != null)
        //    //{
        //    //    Debug.LogError("Cannot create Dialogue Box on Canvas");
        //    //}

        //    // 加载预制件AssetDatabase.LoadAssetAtPath
        //    GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/com.zhanlehall.pluliter/Runtime/Prefab/DialogueBox.prefab");
        //    GameObject obj = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
        //    // 完全解压缩预制件
        //    PrefabUtility.UnpackPrefabInstance(obj, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
        //    obj.name = "DialogueBox";
        //    GameObjectUtility.SetParentAndAlign(obj, parent);
        //    Selection.activeObject = obj;
        //}

        //[MenuItem("GameObject/Pluliter/Actor Layer", false, 10)]
        //private static void CreateActorLayer(MenuCommand menuCommand)
        //{
        //    GameObject obj = new GameObject("ActorLayer");
        //    obj.AddComponent<Actor.ActorManager>();

        //    Undo.RegisterCreatedObjectUndo(obj, "Create " + obj.name);
        //    // 获取场景主相机
        //    Camera mainCamera = Camera.main;
        //    if (mainCamera != null)
        //    {
        //        var objPosition = mainCamera.transform.position;
                
                
        //        //obj.transform.position = new Vector3(objPosition.x, objPosition.y, (objPosition.z + 10f));
        //        // 允许撤销位置
        //        Undo.RecordObject(obj, "Move(自动对齐) " + obj.name);
        //        obj.transform.LookAt(mainCamera.transform.position);
        //        // 如果obj的Y轴为180度，则将Y轴设为0
        //        //if (obj.transform.rotation.eulerAngles.y == 180f)
        //        //{
        //        //    obj.transform.rotation = Quaternion.Euler(obj.transform.rotation.eulerAngles.x, 0, obj.transform.eulerAngles.z);

        //        //}
        //        //else if( obj.transform.rotation.eulerAngles.y > 180f)
        //        //{
        //        //    obj.transform.rotation = Quaternion.Euler(obj.transform.rotation.eulerAngles.x, 360f - obj.transform.rotation.eulerAngles.y, obj.transform.eulerAngles.z);
        //        //}
        //        // 对齐
        //        obj.transform.position = new Vector3(objPosition.x, objPosition.y, (objPosition.z + 10f));
        //    }
        //    else
        //    {
        //        Debug.LogWarning("在场景中没有找到主相机，请确保场景中有一个相机，并手动设置对齐ActorLayer的位置");
        //    }
        //    GameObjectUtility.SetParentAndAlign(obj, menuCommand.context as GameObject);
            
            
        //    Selection.activeObject = obj;
        //}

        //[MenuItem("GameObject/Pluliter/Background Layer", false, 10)]
        //private static void CreateBackgroundLayer(MenuCommand menuCommand)
        //{
        //    GameObject obj = new GameObject("BackgroundLayer");
        //    obj.AddComponent<Background.BackgroundManager>();
        //    Undo.RegisterCreatedObjectUndo(obj, "Create " + obj.name);
        //    // 获取场景主相机
        //    Camera mainCamera = Camera.main;
        //    if (mainCamera != null)
        //    {
        //        var objPosition = mainCamera.transform.position;
        //        // 允许撤销位置
        //        Undo.RecordObject(obj, "Move(自动对齐) " + obj.name);
        //        obj.transform.LookAt(mainCamera.transform.position);
        //        // 对齐
        //        obj.transform.position = new Vector3(objPosition.x, objPosition.y, (objPosition.z + 20f));
        //    }
        //    else
        //    {
        //        Debug.LogWarning("在场景中没有找到主相机，请确保场景中有一个相机，并手动设置对齐BackgroundLayer的位置");
        //    }
        //    GameObjectUtility.SetParentAndAlign(obj, menuCommand.context as GameObject);
        //    Selection.activeObject = obj;
        //}
    }
}
