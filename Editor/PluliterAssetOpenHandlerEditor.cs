using Pluliter.Audio;
using Pluliter.Background;
using Pluliter.Character;
using Pluliter.Dialogue;
using Pluliter.Editor.NodeEditor;
using UnityEditor;

namespace Pluliter.Editor
{
    /// <summary>
    /// 多文资源打开处理器
    /// </summary>
    public class PluliterAssetOpenHandlerEditor
    {
        [UnityEditor.Callbacks.OnOpenAsset]
        public static bool OnOpenAssets(int instanceID, int line)
        {
            var asset = EditorUtility.InstanceIDToObject(instanceID);
            if (asset == null)
            {
                return false;
            }
            switch (asset)
            {
                case DialogueChapter dialogueChapter:
                    DialogueNodeEditorWindow.Open(dialogueChapter);
                    return true;
                case CharacterTable characterTable:
                    CharacterEditorWindow.Open(characterTable);
                    return true;
                case BackgroundTable backgroundTable:
                    BackgroundEditorWindow.Open(backgroundTable);
                    return true;
                case BgmTable bgmTable:
                    BgmEditorWindow.Open(bgmTable);
                    return true;
                case SoundEffectTable soundEffectTable:
                    SoundEffectEditorWindow.Open(soundEffectTable);
                    return true;
                case DialogueConfig dialogueConfig:
                    DialogueConfigEditorWindow.Open(dialogueConfig);
                    return true;
                default:
                    return false;
            }
        }
    }
}
