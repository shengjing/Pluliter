﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEditor;
using UnityEngine;

namespace Pluliter.Editor.Report
{
    /// <summary>
    /// 错误报告窗口，用于显示错误信息
    /// </summary>
    public class PluliterErrorReportWindow : EditorWindow
    {
        private static PluliterEditorErrorReport currentError;
        private Vector2 scrollPosition;

        public static void ShowWindow(PluliterEditorErrorReport error)
        {
            if (error == null)
            {
                return;
            }
            currentError = error;
            var window = GetWindow<PluliterErrorReportWindow>(true);
            window.titleContent = new GUIContent("(｡•́︿•̀｡) 哎呀，多文好像出错了！");
            window.minSize = new Vector2(540, 400);
        }

        void OnGUI()
        {
            if (currentError == null)
            {
                this.Close();
                return;
            }

            EditorGUILayout.LabelField("报错信息:", EditorStyles.boldLabel);

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            if (string.IsNullOrEmpty(currentError.Message))
            {
                currentError.Message = "未知错误";
            }
            EditorGUILayout.TextArea(currentError.Message, GUILayout.Height(100), GUILayout.ExpandWidth(true));
            EditorGUILayout.Space(10);
            EditorGUILayout.LabelField("堆栈跟踪:", EditorStyles.boldLabel);
            EditorGUILayout.TextArea(currentError.StackTrace, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

            EditorGUILayout.EndScrollView();

            EditorGUILayout.Space(15);

            // 一键复制
            if (GUILayout.Button("一键复制", GUILayout.Height(30)))
            {
                EditorGUIUtility.systemCopyBuffer = currentError.Message + "\n\n" + currentError.StackTrace;
            }


            EditorGUILayout.Space(5);

            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("提交错误报告", GUILayout.Height(30)))
                {
                    Application.OpenURL("https://gitcode.com/shengjing/Pluliter/issues/create");
                    this.Close();
                }

                if (GUILayout.Button("假装没看见", GUILayout.Height(30)))
                {
                    this.Close();
                }
            }
        }
    }
}