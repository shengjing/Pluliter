﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System.Collections.Generic;
using System;
using UnityEditor;
using UnityEngine;

namespace Pluliter.Editor.Report
{
    /// <summary>
    /// 多文错误处理程序
    /// </summary>
    public class PluliterEditorErrorHandler : ScriptableObject
    {
        private static PluliterEditorErrorHandler instance;
        private List<PluliterEditorErrorReport> errorReports = new List<PluliterEditorErrorReport>();


        [MenuItem(PluliterEditorGlobal.RootMenuName + "初始化错误处理程序")]
        public static void Initialize()
        {
            instance = CreateInstance<PluliterEditorErrorHandler>();
            Application.logMessageReceived += HandleLog;
        }

        private static void HandleLog(string logString, string stackTrace, LogType type)
        {
            if (type == LogType.Error || type == LogType.Exception)
            {
                if (IsPluliterError(stackTrace))
                {
                    var report = new PluliterEditorErrorReport
                    {
                        Timestamp = DateTime.Now,
                        Type = type,
                        Message = logString,
                        StackTrace = stackTrace
                    };

                    instance.errorReports.Add(report);
                    if (EditorApplication.isPlaying)
                    {
                        PluliterErrorReportWindow.ShowWindow(report);
                    }
                }
            }
        }

        private static bool IsPluliterError(string stackTrace)
        {
            if (string.IsNullOrEmpty(stackTrace)) return false;

            foreach (var frame in stackTrace.Split('\n'))
            {
                var trimmedFrame = frame.Trim();

                if (trimmedFrame.StartsWith("Pluliter."))
                {
                    return true;
                }
                // 根据需要添加更多检查条件
            }
            return false;
        }
    }
}