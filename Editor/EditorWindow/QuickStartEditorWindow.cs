﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace Pluliter.Editor
{
    public class QuickStartEditorWindow : EditorWindow
    {

        private const string ScenePath = "Assets/Scenes/PluliterQuickStart.unity";
        private const string FolderPath = "Assets/Scenes";

        // [MenuItem("多文Pluliter/快速开始（施工中）")]
        public static void ShowWindow()
        {
            GetWindow<QuickStartEditorWindow>("快速开始"); 
        }

        private void OnGUI()
        {
            GUILayout.Label("快速开始", EditorStyles.boldLabel);
            GUILayout.Space(10);

            #region 欢迎来到教程
            GUILayout.Label("欢迎来到快速开始教程", EditorStyles.boldLabel);
            GUILayout.Label("该教程会帮助你快速搭建一个对话场景并编写自己的对话剧本", EditorStyles.wordWrappedLabel);
            #endregion

            GUILayout.Space(5);

            #region 创建示例场景
            GUILayout.BeginVertical();

            GUILayout.Label("创建/打开示例场景", EditorStyles.boldLabel);
            Scene scene;

            bool isSceneCreated = AssetDatabase.LoadAssetAtPath<SceneAsset>(ScenePath) != null;

            if (isSceneCreated)
            {
                // 如果现在已经激活了当前场景
                if (EditorSceneManager.GetActiveScene().path != ScenePath)
                {
                    if (GUILayout.Button("打开示例场景"))
                    {
                        scene = EditorSceneManager.OpenScene(ScenePath);
                    }
                }
                
                if (GUILayout.Button("删除示例场景"))
                {
                    AssetDatabase.DeleteAsset(ScenePath);
                    AssetDatabase.Refresh();
                    // 激活一个新的空场景
                    scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);
                    EditorSceneManager.SetActiveScene(scene);
                }
            }
            else
            {
                // 创建新场景
                if (GUILayout.Button("新建示例场景"))
                {
                    scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);

                    if (!AssetDatabase.IsValidFolder(FolderPath))
                    {
                        AssetDatabase.CreateFolder("Assets", "Scenes");
                    }
                    EditorSceneManager.SaveScene(scene, ScenePath);
                    // 设置场景为激活状态
                    EditorSceneManager.SetActiveScene(scene);

                    // 往场景添加预制件
                    GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>("com.zhanlehall.pluliter/QuickStart/PluliterQuickStart.prefab");
                }
            }
            GUILayout.EndVertical();
            #endregion

            GUILayout.Space(5);


        }
    }
}
