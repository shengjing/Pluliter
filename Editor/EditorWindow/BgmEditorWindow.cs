﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Audio;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;


namespace Pluliter.Editor
{
    /// <summary>
    /// BGM编辑器
    /// </summary>
    public class BgmEditorWindow : EditorWindow
    {
        private BgmTable bgmTable;
        private Vector2 backgroundListScrollPos;
        private int selectedIndex = -1;
        private SerializedObject serializedTable;

        [MenuItem(PluliterEditorGlobal.RootMenuName + "背景音乐编辑器", false, 50)]
        private static void ShowWindow()
        {
            var window = GetWindow<BgmEditorWindow>("BGM编辑器");
            window.minSize = new Vector2(900, 620);
            window.Show();
        }

        /// <summary>
        /// 打开BGM表
        /// </summary>
        /// <param name="asset">BGM表Asset资源</param>
        public static void Open(BgmTable asset)
        {
            var window = GetWindow<BgmEditorWindow>("BGM编辑器");
            window.minSize = new Vector2(900, 620);
            window.Show();
            window.bgmTable = asset;
        }

        private void OnEnable()
        {
            Undo.undoRedoPerformed += OnUndoRedo;
        }

        private void OnDisable()
        {
            Undo.undoRedoPerformed -= OnUndoRedo;
        }

        private void OnUndoRedo()
        {
            Repaint();
            if (bgmTable != null)
            {
                serializedTable?.Update();
                selectedIndex = Mathf.Clamp(selectedIndex, 0, bgmTable.bgmList.Count - 1);
            }
        }

        private void InitializeSerializedObject()
        {
            if (bgmTable != null)
            {
                serializedTable = new SerializedObject(bgmTable);
                selectedIndex = Mathf.Clamp(selectedIndex, 0, bgmTable.bgmList.Count - 1);
            }
            else
            {
                serializedTable = null;
                selectedIndex = -1;
            }
        }

        /// <summary>
        /// 绘制顶部工具栏
        /// </summary>
        private void DrawTopbar()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                GUILayout.Label("多文BGM表编辑器", EditorStyles.whiteLabel, GUILayout.Width(150));

                GUILayout.Label("BGM表:", EditorStyles.whiteLabel, GUILayout.Width(50));
                var newTable = EditorGUILayout.ObjectField(bgmTable, typeof(BgmTable), false, GUILayout.Width(350)) as BgmTable;
                if (newTable != bgmTable)
                {
                    bgmTable = newTable;
                    InitializeSerializedObject();
                }

                if (GUILayout.Button("创建新BGM表", EditorStyles.toolbarButton))
                {
                    CreateNewBGMTable();
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 创建新BGM表
        /// </summary>
        private void CreateNewBGMTable()
        {
            var path = EditorUtility.SaveFilePanelInProject("创建新BGM表","NewBGMTable.asset","asset", "选择保存位置");

            if (!string.IsNullOrEmpty(path))
            {
                var newTable = CreateInstance<BgmTable>();
                AssetDatabase.CreateAsset(newTable, path);
                bgmTable = newTable;
                InitializeSerializedObject();
            }
        }

        /// <summary>
        /// 绘制
        /// </summary>
        private void DrawMainContent()
        {
            if (bgmTable == null)
            {
                EditorGUILayout.HelpBox("请选择或创建一个BGM表", MessageType.Info);
                return;
            }

            EditorGUILayout.BeginHorizontal();
            {
                DrawBGMList();
                GUILayout.Space(5);
                DrawEditArea();
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 绘制BGM列表
        /// </summary>
        private void DrawBGMList()
        {
            EditorGUILayout.BeginVertical(GUILayout.Width(300));
            {
                EditorGUILayout.LabelField("BGM列表", EditorStyles.boldLabel);
                backgroundListScrollPos = EditorGUILayout.BeginScrollView(backgroundListScrollPos, GUIStyle.none, GUI.skin.verticalScrollbar);

                if (bgmTable.bgmList == null)
                {
                    bgmTable.bgmList = new();
                }

                for (int i = 0; i < bgmTable.bgmList.Count; i++)
                {
                    var bgm = bgmTable.bgmList[i];
                    var bgmName = string.IsNullOrEmpty(bgm.BgmId) ? "未命名BGM" : bgm.BgmId;

                    EditorGUILayout.BeginHorizontal();
                    {
                        GUILayout.Label(bgmName, GUILayout.ExpandWidth(true));

                        if (GUILayout.Button("编辑", GUILayout.Width(50)))
                        {
                            selectedIndex = i;
                            // 失去焦点，防止刷新不及时误操作
                            GUI.FocusControl(null);
                        }

                        if (GUILayout.Button("删除", GUILayout.Width(50)))
                        {
                            RemoveBGMAtIndex(i);
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndScrollView();

                if (GUILayout.Button("添加BGM"))
                {
                    AddNewBGM();
                }
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// 绘制编辑区域
        /// </summary>
        private void DrawEditArea()
        {
            EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
            {
                if (selectedIndex >= 0 && selectedIndex < bgmTable.bgmList.Count)
                {
                    var bgm = bgmTable.bgmList[selectedIndex];
                    EditorGUILayout.LabelField("编辑BGM", EditorStyles.boldLabel);

                    EditorGUI.BeginChangeCheck();
                    var newId = EditorGUILayout.DelayedTextField("BGM ID", bgm.BgmId);
                    var newClip = EditorGUILayout.ObjectField("音频文件", bgm.BgmClip, typeof(AudioClip), false) as AudioClip;

                    if (EditorGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(bgmTable, "修改BGM属性");
                        bgm.BgmId = newId;
                        bgm.BgmClip = newClip;
                        EditorUtility.SetDirty(bgmTable);
                    }

                    DrawValidationWarnings(bgm);
                }
                else
                {
                    EditorGUILayout.HelpBox("请从列表中选择一个BGM进行编辑", MessageType.Info);
                }
            }
            EditorGUILayout.EndVertical();
        }

        private void DrawValidationWarnings(Bgm bgm)
        {
            var warnings = new List<string>();

            if (string.IsNullOrEmpty(bgm.BgmId))
            {
                warnings.Add("ID不能为空");
            }
            else if (IsDuplicateID(bgm.BgmId, selectedIndex))
            {
                warnings.Add("ID已存在");
            }
            if (bgm.BgmClip == null)
            {
                warnings.Add("需要指定音频文件");
            }
            if (warnings.Count > 0)
            {
                EditorGUILayout.HelpBox(string.Join("\n", warnings), MessageType.Error);
            }
        }

        /// <summary>
        /// 是重复的id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentIndex"></param>
        /// <returns></returns>
        private bool IsDuplicateID(string id, int currentIndex)
        {
            for (int i = 0; i < bgmTable.bgmList.Count; i++)
            {
                if (i != currentIndex && bgmTable.bgmList[i].BgmId == id)
                {
                    return true;
                }
            }
            return false;
        }

        private void AddNewBGM()
        {
            Undo.RecordObject(bgmTable, "添加BGM");
            var newBgm = new Bgm($"BGM_{bgmTable.bgmList.Count + 1}");
            bgmTable.bgmList.Add(newBgm);
            selectedIndex = bgmTable.bgmList.Count - 1;
            EditorUtility.SetDirty(bgmTable);
        }

        private void RemoveBGMAtIndex(int index)
        {
            if (EditorUtility.DisplayDialog("确认删除", $"确定要删除 '{bgmTable.bgmList[index].BgmId}' 吗？", "删除", "取消"))
            {
                Undo.RecordObject(bgmTable, "删除BGM");
                bgmTable.bgmList.RemoveAt(index);
                selectedIndex = Mathf.Clamp(selectedIndex, 0, bgmTable.bgmList.Count - 1);
                EditorUtility.SetDirty(bgmTable);
            }
        }

        private void OnGUI()
        {
            DrawTopbar();
            EditorGUILayout.Space();
            DrawMainContent();

            if (serializedTable != null && serializedTable.hasModifiedProperties)
            {
                serializedTable.ApplyModifiedProperties();
            }
        }
    }
}