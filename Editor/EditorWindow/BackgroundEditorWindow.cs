﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Background;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace Pluliter.Editor
{
    /// <summary>
    /// 背景编辑器
    /// </summary>
    public class BackgroundEditorWindow : EditorWindow
    {
        private BackgroundTable backgroundTable;

        private SerializedObject serializedTable;

        private Vector2 backgroundListScrollPos;

        private int selectedIndex = -1;

        /// <summary>
        /// 开启背景图片预览
        /// </summary>
        private bool backgroundSpritePreview = false;


        [MenuItem(PluliterEditorGlobal.RootMenuName + "背景编辑器", false, 50)]
        private static void ShowWindow()
        {
            var window = GetWindow<BackgroundEditorWindow>("背景编辑器");
            window.minSize = new Vector2(900, 620);
            window.Show();
        }

        /// <summary>
        /// 打开背景表
        /// </summary>
        /// <param name="asset">背景表Asset资源</param>
        public static void Open(BackgroundTable asset)
        {
            var window = GetWindow<BackgroundEditorWindow>("背景编辑器");
            window.minSize = new Vector2(900, 620);
            window.Show();
            window.backgroundTable = asset;
        }

        private void OnEnable()
        {
            Undo.undoRedoPerformed += OnUndoRedo;
        }

        private void OnDisable()
        {
            Undo.undoRedoPerformed -= OnUndoRedo;
        }

        private void OnUndoRedo()
        {
            Repaint();
            if (backgroundTable != null)
            {
                serializedTable?.Update();
                selectedIndex = Mathf.Clamp(selectedIndex, 0, backgroundTable.backgrounds.Count - 1);
            }
        }

        private void InitializeSerializedObject()
        {
            if (backgroundTable != null)
            {
                serializedTable = new SerializedObject(backgroundTable);
                selectedIndex = Mathf.Clamp(selectedIndex, 0, backgroundTable.backgrounds.Count - 1);
            }
            else
            {
                serializedTable = null;
                selectedIndex = -1;
            }
        }

        /// <summary>
        /// 绘制顶部工具栏
        /// </summary>
        private void DrawTopbar()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                GUILayout.Label("多文背景表编辑器", EditorStyles.whiteLabel, GUILayout.Width(150));

                GUILayout.Label("背景表:", EditorStyles.whiteLabel, GUILayout.Width(50));
                var newTable = EditorGUILayout.ObjectField(backgroundTable, typeof(BackgroundTable), false, GUILayout.Width(350)) as BackgroundTable;
                if (newTable != backgroundTable)
                {
                    backgroundTable = newTable;
                    InitializeSerializedObject();
                }

                if (GUILayout.Button("创建新背景表", EditorStyles.toolbarButton))
                {
                    CreateNewBackgroundTable();
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 创建新背景表
        /// </summary>
        private void CreateNewBackgroundTable()
        {
            var path = EditorUtility.SaveFilePanelInProject("创建新背景表", "NewBackgroundTable.asset", "asset", "选择保存位置");

            if (!string.IsNullOrEmpty(path))
            {
                var newTable = CreateInstance<BackgroundTable>();
                AssetDatabase.CreateAsset(newTable, path);
                backgroundTable = newTable;
                InitializeSerializedObject();
            }
        }

        /// <summary>
        /// 绘制
        /// </summary>
        private void DrawMainContent()
        {
            if (backgroundTable == null)
            {
                EditorGUILayout.HelpBox("请选择或创建一个背景表", MessageType.Info);
                return;
            }

            EditorGUILayout.BeginHorizontal();
            {
                DrawBackgroundList();
                GUILayout.Space(5);
                DrawEditArea();
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 绘制背景列表
        /// </summary>
        private void DrawBackgroundList()
        {
            EditorGUILayout.BeginVertical(GUILayout.Width(300));
            {
                EditorGUILayout.LabelField("背景列表", EditorStyles.boldLabel);
                backgroundListScrollPos = EditorGUILayout.BeginScrollView(backgroundListScrollPos, GUIStyle.none, GUI.skin.verticalScrollbar);

                if (backgroundTable.backgrounds == null)
                {
                    backgroundTable.backgrounds = new();
                }

                for (int i = 0; i < backgroundTable.backgrounds.Count; i++)
                {
                    var bg = backgroundTable.backgrounds[i];
                    var bgName = string.IsNullOrEmpty(bg.BgId) ? "未命名背景" : bg.BgId;

                    EditorGUILayout.BeginHorizontal();
                    {
                        GUILayout.Label(bgName, GUILayout.ExpandWidth(true));

                        if (GUILayout.Button("编辑", GUILayout.Width(50)))
                        {
                            selectedIndex = i;
                            // 失去焦点，防止刷新不及时误操作
                            GUI.FocusControl(null);
                        }

                        if (GUILayout.Button("删除", GUILayout.Width(50)))
                        {
                            RemoveBGMAtIndex(i);
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndScrollView();

                if (GUILayout.Button("添加背景"))
                {
                    AddNewBackground();
                }
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// 绘制编辑区域
        /// </summary>
        private void DrawEditArea()
        {
            EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
            {
                if (selectedIndex >= 0 && selectedIndex < backgroundTable.backgrounds.Count)
                {
                    var bg = backgroundTable.backgrounds[selectedIndex];
                    EditorGUILayout.LabelField("编辑背景", EditorStyles.boldLabel);

                    EditorGUI.BeginChangeCheck();
                    
                    var newId = EditorGUILayout.DelayedTextField("背景 ID", bg.BgId);
                    var newSprite = EditorGUILayout.ObjectField("背景图片", bg.BgSprite, typeof(Sprite), false) as Sprite;

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("预览", GUILayout.Width(50));
                    backgroundSpritePreview = EditorGUILayout.Toggle(backgroundSpritePreview);
                    EditorGUILayout.EndHorizontal();

                    if (backgroundSpritePreview)
                    {
                        var showWidth = EditorGUIUtility.currentViewWidth * 0.4f;

                        // 获取图片比例
                        var ratio = backgroundTable.backgrounds[selectedIndex].BgSprite.texture.width / (float)backgroundTable.backgrounds[selectedIndex].BgSprite.texture.height;

                        // 根据图片比例调整显示高度
                        var showHeight = (int)(showWidth / ratio);

                        GUILayout.Label(backgroundTable.backgrounds[selectedIndex].BgSprite.texture, GUILayout.Width(showWidth), GUILayout.Height(showHeight));
                    }

                    if (EditorGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(backgroundTable, "修改BGM属性");
                        bg.BgId = newId;
                        bg.BgSprite = newSprite;
                        EditorUtility.SetDirty(backgroundTable);
                    }

                    DrawValidationWarnings(bg);
                }
                else
                {
                    EditorGUILayout.HelpBox("请从列表中选择一个BGM进行编辑", MessageType.Info);
                }
            }
            EditorGUILayout.EndVertical();
        }

        private void DrawValidationWarnings(Background.Background background)
        {
            var warnings = new List<string>();

            if (string.IsNullOrEmpty(background.BgId))
            {
                warnings.Add("ID不能为空");
            }
            else if (IsDuplicateID(background.BgId, selectedIndex))
            {
                warnings.Add("ID已存在");
            }
            if (background.BgSprite == null)
            {
                warnings.Add("需要指定背景图片文件");
            }
            if (warnings.Count > 0)
            {
                EditorGUILayout.HelpBox(string.Join("\n", warnings), MessageType.Error);
            }
        }

        /// <summary>
        /// 是重复的id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentIndex"></param>
        /// <returns></returns>
        private bool IsDuplicateID(string id, int currentIndex)
        {
            for (int i = 0; i < backgroundTable.backgrounds.Count; i++)
            {
                if (i != currentIndex && backgroundTable.backgrounds[i].BgId == id)
                {
                    return true;
                }
            }
            return false;
        }

        private void AddNewBackground()
        {
            Undo.RecordObject(backgroundTable, "添加背景");
            var newBg = new Background.Background($"BG_{backgroundTable.backgrounds.Count + 1}");
            backgroundTable.backgrounds.Add(newBg);
            selectedIndex = backgroundTable.backgrounds.Count - 1;
            EditorUtility.SetDirty(backgroundTable);
        }

        private void RemoveBGMAtIndex(int index)
        {
            if (EditorUtility.DisplayDialog("确认删除", $"确定要删除 '{backgroundTable.backgrounds[index].BgId}' 吗？", "删除", "取消"))
            {
                Undo.RecordObject(backgroundTable, "删除BGM");
                backgroundTable.backgrounds.RemoveAt(index);
                selectedIndex = Mathf.Clamp(selectedIndex, 0, backgroundTable.backgrounds.Count - 1);
                EditorUtility.SetDirty(backgroundTable);
            }
        }

        private void OnGUI()
        {
            DrawTopbar();
            EditorGUILayout.Space();
            DrawMainContent();

            if (serializedTable != null && serializedTable.hasModifiedProperties)
            {
                serializedTable.ApplyModifiedProperties();
            }
        }
    }
}
