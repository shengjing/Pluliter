﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Audio;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;


namespace Pluliter.Editor
{
    /// <summary>
    /// 音效编辑器窗口
    /// </summary>
    public class SoundEffectEditorWindow : EditorWindow
    {
        private const string WINDOW_TITLE = "音效编辑器";
        private SoundEffectTable soundEffectTable;
        private Vector2 backgroundListScrollPos;
        private int selectedIndex = -1;
        private SerializedObject serializedTable;

        [MenuItem(PluliterEditorGlobal.RootMenuName + "音效编辑器", false, 50)]
        private static void ShowWindow()
        {
            var window = GetWindow<SoundEffectEditorWindow>(WINDOW_TITLE);
            window.minSize = new Vector2(900, 620);
            window.Show();
        }

        /// <summary>
        /// 打开音效编辑器窗口
        /// </summary>
        /// <param name="asset"></param>
        public static void Open(SoundEffectTable asset)
        {
            var window = GetWindow<SoundEffectEditorWindow>(WINDOW_TITLE);
            window.minSize = new Vector2(900, 620);
            window.Show();
            window.soundEffectTable = asset;
        }

        private void OnEnable()
        {
            Undo.undoRedoPerformed += OnUndoRedo;
        }

        private void OnDisable()
        {
            Undo.undoRedoPerformed -= OnUndoRedo;
        }

        private void OnUndoRedo()
        {
            Repaint();
            if (soundEffectTable != null)
            {
                serializedTable?.Update();
                selectedIndex = Mathf.Clamp(selectedIndex, 0, soundEffectTable.SoundEffectList.Count - 1);
            }
        }

        private void InitializeSerializedObject()
        {
            if (soundEffectTable != null)
            {
                serializedTable = new SerializedObject(soundEffectTable);
                selectedIndex = Mathf.Clamp(selectedIndex, 0, soundEffectTable.SoundEffectList.Count - 1);
            }
            else
            {
                serializedTable = null;
                selectedIndex = -1;
            }
        }

        /// <summary>
        /// 绘制顶部工具栏
        /// </summary>
        private void DrawTopbar()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                GUILayout.Label("多文音效表编辑器", EditorStyles.whiteLabel, GUILayout.Width(150));

                GUILayout.Label("音效表:", EditorStyles.whiteLabel, GUILayout.Width(50));
                var newTable = EditorGUILayout.ObjectField(soundEffectTable, typeof(SoundEffectTable), false, GUILayout.Width(350)) as SoundEffectTable;
                if (newTable != soundEffectTable)
                {
                    soundEffectTable = newTable;
                    InitializeSerializedObject();
                }

                if (GUILayout.Button("创建新音效表", EditorStyles.toolbarButton))
                {
                    CreateNewBGMTable();
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 创建新BGM表
        /// </summary>
        private void CreateNewBGMTable()
        {
            var path = EditorUtility.SaveFilePanelInProject("创建新音效表","新音效表.asset","asset", "选择保存位置");

            if (!string.IsNullOrEmpty(path))
            {
                var newTable = CreateInstance<SoundEffectTable>();
                AssetDatabase.CreateAsset(newTable, path);
                soundEffectTable = newTable;
                InitializeSerializedObject();
            }
        }

        /// <summary>
        /// 绘制
        /// </summary>
        private void DrawMainContent()
        {
            if (soundEffectTable == null)
            {
                EditorGUILayout.HelpBox("请选择或创建一个音效表", MessageType.Info);
                return;
            }

            EditorGUILayout.BeginHorizontal();
            {
                DrawBGMList();
                GUILayout.Space(5);
                DrawEditArea();
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 绘制音效列表
        /// </summary>
        private void DrawBGMList()
        {
            EditorGUILayout.BeginVertical(GUILayout.Width(300));
            {
                EditorGUILayout.LabelField("音效列表", EditorStyles.boldLabel);
                backgroundListScrollPos = EditorGUILayout.BeginScrollView(backgroundListScrollPos, GUIStyle.none, GUI.skin.verticalScrollbar);

                if (soundEffectTable.SoundEffectList == null)
                {
                    soundEffectTable.SoundEffectList = new();
                }

                for (int i = 0; i < soundEffectTable.SoundEffectList.Count; i++)
                {
                    var se = soundEffectTable.SoundEffectList[i];
                    var seName = string.IsNullOrEmpty(se.SoundEffectId) ? "未命名音效" : se.SoundEffectId;

                    EditorGUILayout.BeginHorizontal();
                    {
                        GUILayout.Label(seName, GUILayout.ExpandWidth(true));

                        if (GUILayout.Button("编辑", GUILayout.Width(50)))
                        {
                            selectedIndex = i;
                            // 失去焦点，防止刷新不及时误操作
                            GUI.FocusControl(null);
                        }

                        if (GUILayout.Button("删除", GUILayout.Width(50)))
                        {
                            RemoveSoundEffectAtIndex(i);
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndScrollView();

                if (GUILayout.Button("添加音效"))
                {
                    AddNewSoundEffect();
                }
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// 绘制编辑区域
        /// </summary>
        private void DrawEditArea()
        {
            EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
            {
                if (selectedIndex >= 0 && selectedIndex < soundEffectTable.SoundEffectList.Count)
                {
                    var bgm = soundEffectTable.SoundEffectList[selectedIndex];
                    EditorGUILayout.LabelField("编辑音效", EditorStyles.boldLabel);

                    EditorGUI.BeginChangeCheck();
                    var newId = EditorGUILayout.DelayedTextField("音效 ID", bgm.SoundEffectId);
                    var newClip = EditorGUILayout.ObjectField("音效文件", bgm.SoundEffectClip, typeof(AudioClip), false) as AudioClip;

                    if (EditorGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(soundEffectTable, "修改音效属性");
                        bgm.SoundEffectId = newId;
                        bgm.SoundEffectClip = newClip;
                        EditorUtility.SetDirty(soundEffectTable);
                    }

                    DrawValidationWarnings(bgm);
                }
                else
                {
                    EditorGUILayout.HelpBox("请从列表中选择一个音效进行编辑", MessageType.Info);
                }
            }
            EditorGUILayout.EndVertical();
        }

        private void DrawValidationWarnings(SoundEffect soundEffect)
        {
            var warnings = new List<string>();

            if (string.IsNullOrEmpty(soundEffect.SoundEffectId))
            {
                warnings.Add("ID不能为空");
            }
            else if (IsDuplicateID(soundEffect.SoundEffectId, selectedIndex))
            {
                warnings.Add("ID已存在");
            }
            if (soundEffect.SoundEffectClip == null)
            {
                warnings.Add("需要指定音频文件");
            }
            if (warnings.Count > 0)
            {
                EditorGUILayout.HelpBox(string.Join("\n", warnings), MessageType.Error);
            }
        }

        /// <summary>
        /// 是重复的id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentIndex"></param>
        /// <returns></returns>
        private bool IsDuplicateID(string id, int currentIndex)
        {
            for (int i = 0; i < soundEffectTable.SoundEffectList.Count; i++)
            {
                if (i != currentIndex && soundEffectTable.SoundEffectList[i].SoundEffectId == id)
                {
                    return true;
                }
            }
            return false;
        }

        private void AddNewSoundEffect()
        {
            Undo.RecordObject(soundEffectTable, "添加BGM");
            var newSe = new SoundEffect($"SE_{soundEffectTable.SoundEffectList.Count + 1}");
            soundEffectTable.SoundEffectList.Add(newSe);
            selectedIndex = soundEffectTable.SoundEffectList.Count - 1;
            EditorUtility.SetDirty(soundEffectTable);
        }

        private void RemoveSoundEffectAtIndex(int index)
        {
            if (EditorUtility.DisplayDialog("确认删除", $"确定要删除 '{soundEffectTable.SoundEffectList[index].SoundEffectId}' 吗？", "删除", "取消"))
            {
                Undo.RecordObject(soundEffectTable, "删除音效");
                soundEffectTable.SoundEffectList.RemoveAt(index);
                selectedIndex = Mathf.Clamp(selectedIndex, 0, soundEffectTable.SoundEffectList.Count - 1);
                EditorUtility.SetDirty(soundEffectTable);
            }
        }

        private void OnGUI()
        {
            DrawTopbar();
            EditorGUILayout.Space();
            DrawMainContent();

            if (serializedTable != null && serializedTable.hasModifiedProperties)
            {
                serializedTable.ApplyModifiedProperties();
            }
        }
    }
}