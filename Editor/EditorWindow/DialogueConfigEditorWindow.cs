﻿using UnityEditor;
using UnityEngine;
using Pluliter.Dialogue;

namespace Pluliter.Editor
{
    /// <summary>
    /// 对话配置编辑器窗口
    /// </summary>
    public class DialogueConfigEditorWindow : EditorWindow
    {
        private DialogueConfig config;
        private SerializedObject serializedConfig;
        private Vector2 scrollPosition;
        private int selectedTab = 0;
        private readonly string[] tabNames = { "基础信息", "播放设置", "章节列表" };


        [MenuItem(PluliterEditorGlobal.RootMenuName + "对话配置编辑器", false, 50)]
        public static void ShowWindow()
        {
            GetWindow<DialogueConfigEditorWindow>("对话配置编辑器");
        }

        public static void Open(DialogueConfig config)
        {
            var window = GetWindow<DialogueConfigEditorWindow>("对话配置编辑器");
            window.config = config;
            window.Show();
        }

        private void OnGUI()
        {
            // 绘制配置文件选择区域
            DrawConfigSelector();

            // 顶部选项卡工具栏
            selectedTab = GUILayout.Toolbar(selectedTab, tabNames);

            if (config == null)
            {
                EditorGUILayout.HelpBox("请先创建或选择对话配置资产", MessageType.Info);
                return;
            }

            // 初始化序列化对象
            if (serializedConfig == null || serializedConfig.targetObject != config)
            {
                serializedConfig = new SerializedObject(config);
            }

            // 开始滚动视图
            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            // 根据选中的选项卡绘制不同内容
            switch (selectedTab)
            {
                case 0:
                    DrawBasicInfoTab();
                    break;
                case 1:
                    DrawPlaySettingsTab();
                    break;
                case 2:
                    DrawChapterListTab();
                    break;
            }

            // 应用属性修改并结束滚动视图
            serializedConfig.ApplyModifiedProperties();
            EditorGUILayout.EndScrollView();
        }

        private void DrawConfigSelector()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("对话配置资产", GUILayout.Width(120));
            config = (DialogueConfig)EditorGUILayout.ObjectField(
                config,
                typeof(DialogueConfig),
                false
            );
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
        }

        #region 基础信息选项卡
        private void DrawBasicInfoTab()
        {
            // 基础信息
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("基础设置", EditorStyles.boldLabel);
            DrawPropertyField("dialogueListName", "对话名称");
            DrawPropertyField("dialogueListDescription", "对话描述");
            DrawPropertyField("DialogueReviewContent", "对话回顾内容");
            DrawFontField();

            // 存档系统设置
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("存档设置", EditorStyles.boldLabel);
            DrawBoolDropdown("EnableArchiveSystem", "启用存档系统");
            DrawBoolDropdown("EnableAutoSave", "自动存档功能");
            DrawBoolDropdown("EnabledDialogueHistory", "对话历史记录");
        }
        #endregion

        #region 播放设置选项卡
        private void DrawPlaySettingsTab()
        {
            // 自动播放设置
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("播放控制", EditorStyles.boldLabel);
            DrawBoolDropdown("enableAutoPlay", "自动播放功能");
            DrawBoolDropdown("enableAutoPlayOnEnter", "进入时自动播放");
            DrawFloatRangeFields();

            // 高级设置
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("高级设置", EditorStyles.boldLabel);
            DrawPropertyField("isAutoLoad", "自动加载对话");
            if (config.isAutoLoad)
            {
                DrawPropertyField("autoLoadChapterIndex", "起始章节索引");
            }
        }
        #endregion

        #region 章节列表选项卡
        private void DrawChapterListTab()
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("章节管理", EditorStyles.boldLabel);

            // 绘制章节列表
            var listProp = serializedConfig.FindProperty("dialogueDataList");
            EditorGUILayout.PropertyField(listProp, new GUIContent("章节列表"), true);

            // 清理空章节按钮
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("清理空章节", GUILayout.Width(120)))
            {
                config.dialogueDataList.RemoveAll(chapter => chapter == null);
                EditorUtility.SetDirty(config);
            }
            EditorGUILayout.EndHorizontal();
        }
        #endregion

        #region 通用绘制方法
        private void DrawPropertyField(string propName, string displayName)
        {
            var prop = serializedConfig.FindProperty(propName);
            EditorGUILayout.PropertyField(prop, new GUIContent(displayName), true);
        }

        private void DrawBoolDropdown(string propName, string label)
        {
            var prop = serializedConfig.FindProperty(propName);
            EditorGUI.BeginChangeCheck();

            int selection = prop.boolValue ? 0 : 1;
            selection = EditorGUILayout.Popup(label, selection, new[] { "开启", "关闭" });

            if (EditorGUI.EndChangeCheck())
            {
                prop.boolValue = selection == 0;
            }
        }

        private void DrawFloatRangeFields()
        {
            EditorGUILayout.BeginHorizontal();
            var minProp = serializedConfig.FindProperty("autoPlayMinReadTime");
            var maxProp = serializedConfig.FindProperty("autoPlayMaxReadTime");

            float minValue = EditorGUILayout.FloatField("阅读时间范围", minProp.floatValue);
            float maxValue = EditorGUILayout.FloatField(maxProp.floatValue);

            minProp.floatValue = Mathf.Min(minValue, maxValue);
            maxProp.floatValue = Mathf.Max(minValue, maxValue);
            EditorGUILayout.EndHorizontal();
        }

        private void DrawFontField()
        {
            var fontProp = serializedConfig.FindProperty("dialogueFont");
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("对话字体", GUILayout.Width(EditorGUIUtility.labelWidth));
            fontProp.objectReferenceValue = EditorGUILayout.ObjectField(
                fontProp.objectReferenceValue,
                typeof(TMPro.TMP_FontAsset),
                false
            );
            EditorGUILayout.EndHorizontal();
        }
        #endregion
    }
}