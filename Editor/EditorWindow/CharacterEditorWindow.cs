﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Character;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace Pluliter.Editor
{
    /// <summary>
    /// 角色编辑器
    /// </summary>
    public class CharacterEditorWindow : EditorWindow
    {
        private CharacterTable characterTable;

        private SerializedObject serializedTable;

        private SerializedObject serializedCharacter;

        private Character.Character currentCharacter;

        private Vector2 charaListScrollPos;

        private Vector2 charaStateScrollPos;

        private int selectedIndex = 0;

        private int pageIndex = 0;

        private bool SpritePreview = false;

        private GUIStyle selectedItemStyle;
        private GUIStyle normalItemStyle;


        [MenuItem(PluliterEditorGlobal.RootMenuName + "角色编辑器", false, 50)]
        private static void ShowWindow()
        {
            var window = GetWindow<CharacterEditorWindow>("角色编辑器");
            window.minSize = new Vector2(900, 620);
            window.Show();
        }

        public static void Open(CharacterTable asset)
        {
            var window = GetWindow<CharacterEditorWindow>("角色编辑器");
            window.minSize = new Vector2(900, 620);
            window.characterTable = asset;
            window.Show();
        }

        private void InitializeStyles()
        {
            selectedItemStyle = new GUIStyle(EditorStyles.miniButton)
            {
                normal = { background = MakeTex(2, 2, new Color(0.3f, 0.35f, 1f, 0.35f)) },
                alignment = TextAnchor.MiddleLeft
            };

            normalItemStyle = new GUIStyle(EditorStyles.miniButton)
            {
                alignment = TextAnchor.MiddleLeft
            };
        }

        private Texture2D MakeTex(int width, int height, Color col)
        {
            var pix = new Color[width * height];
            for (int i = 0; i < pix.Length; i++)
            {
                pix[i] = col;
            }
            var result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();
            return result;
        }

        private void OnEnable()
        {
            Undo.undoRedoPerformed += OnUndoRedo;
        }

        private void OnDisable()
        {
            Undo.undoRedoPerformed -= OnUndoRedo;
        }

        private void OnUndoRedo()
        {
            Repaint();
            if (characterTable != null)
            {
                serializedTable?.Update();
                selectedIndex = Mathf.Clamp(selectedIndex, 0, characterTable.characters.Count - 1);
            }
        }

        private void InitializeSerializedObject()
        {
            if (characterTable != null)
            {
                serializedTable = new SerializedObject(characterTable);
                selectedIndex = Mathf.Clamp(selectedIndex, 0, characterTable.characters.Count - 1);
            }
            else
            {
                serializedTable = null;
                selectedIndex = -1;
            }
        }

        private void DrawTopbar()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                GUILayout.Label("多文角色编辑器", EditorStyles.whiteLabel, GUILayout.Width(150));

                GUILayout.Label("角色表:", EditorStyles.whiteLabel, GUILayout.Width(50));
                var newTable = EditorGUILayout.ObjectField(characterTable, typeof(CharacterTable), false, GUILayout.Width(350)) as CharacterTable;
                if (newTable != characterTable)
                {
                    currentCharacter = null;
                    selectedIndex = -1;
                    characterTable = newTable;
                    InitializeSerializedObject();
                }

                if (GUILayout.Button("创建新角色表", EditorStyles.toolbarButton))
                {
                    CreateNewCharacterTable();
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 创建新角色表
        /// </summary>
        private void CreateNewCharacterTable()
        {
            var path = EditorUtility.SaveFilePanelInProject("创建新角色表", "NewCharacterTable.asset", "asset", "选择保存位置");

            if (!string.IsNullOrEmpty(path))
            {
                var newTable = CreateInstance<CharacterTable>();
                AssetDatabase.CreateAsset(newTable, path);
                characterTable = newTable;
                InitializeSerializedObject();
            }
        }

        /// <summary>
        /// 绘制角色列表
        /// </summary>
        private void DrawCharacterList()
        {
            EditorGUILayout.BeginVertical(GUILayout.Width(300));
            {
                EditorGUILayout.LabelField("角色列表", EditorStyles.boldLabel);
                charaListScrollPos = EditorGUILayout.BeginScrollView(charaListScrollPos, GUIStyle.none, GUI.skin.verticalScrollbar);
                {
                    if (characterTable.characters == null)
                    {
                        characterTable.characters = new();
                    }
                    if (characterTable != null && characterTable.characters != null)
                    {
                        if (characterTable.characters.Count == 0)
                        {
                            EditorGUILayout.HelpBox("请新建或添加一个角色", MessageType.Info);
                        }
                        for (int i = 0; i < characterTable.characters.Count; i++)
                        {
                            var chara = characterTable.characters[i];
                            var charaName = string.IsNullOrEmpty(chara.CharacterName) ? "未命名角色" : chara.CharacterName;
                            var style = i == selectedIndex ? selectedItemStyle : normalItemStyle;

                            EditorGUILayout.BeginHorizontal();
                            {
                                if (GUILayout.Button(charaName, style, GUILayout.ExpandWidth(true)))
                                {
                                    selectedIndex = i;
                                    currentCharacter = characterTable.characters[selectedIndex];
                                    serializedCharacter = new SerializedObject(currentCharacter);
                                    // 失去焦点，防止刷新不及时误操作
                                    GUI.FocusControl(null);
                                }
                                if (GUILayout.Button("移除", GUILayout.Width(45)))
                                {
                                    RemoveCharacterAtIndex(i);
                                }
                                if (GUILayout.Button("删除角色", GUILayout.Width(60)))
                                {
                                    DeleteCharacterAtIndex(i);
                                }
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                    }
                }
                EditorGUILayout.EndScrollView();

                if (GUILayout.Button("添加角色"))
                {
                    // 弹出选择路径
                    string path = EditorUtility.OpenFilePanel("选择角色", "Assets", "asset");
                    {
                        if (!string.IsNullOrEmpty(path))
                        {
                            path = FileUtil.GetProjectRelativePath(path);
                            Character.Character newCharacter = AssetDatabase.LoadAssetAtPath<Character.Character>(path);
                            if (newCharacter != null)
                            {
                                if (!characterTable.characters.Contains(newCharacter))
                                {
                                    // 添加到角色表
                                    characterTable.characters.Add(newCharacter);
                                    EditorUtility.SetDirty(characterTable);
                                }
                                else
                                {
                                    EditorUtility.DisplayDialog("多文 - 添加角色", "角色已存在于角色表", "确定");
                                }
                            }
                            else
                            {
                                EditorUtility.DisplayDialog("多文 - 添加角色", "选择的文件不是有效角色", "确定");
                            }
                        }
                    }
                }
                GUILayout.Space(5);
                if (GUILayout.Button("新建角色"))
                {
                    // 创建新的角色SO
                    Character.Character newCharacter = ScriptableObject.CreateInstance<Character.Character>();

                    // 弹出选择路径
                    string path = EditorUtility.SaveFilePanelInProject("新建角色", "新角色", "asset", "新建角色");
                    if (!string.IsNullOrEmpty(path))
                    {
                        AssetDatabase.CreateAsset(newCharacter, path);
                        AssetDatabase.SaveAssets();
                        AssetDatabase.Refresh();
                        characterTable.characters.Add(newCharacter);
                        EditorUtility.SetDirty(characterTable);
                    }

                }
            }
            EditorGUILayout.EndVertical();
        }

        private void DeleteCharacterAtIndex(int index)
        {
            if (EditorUtility.DisplayDialog("多文 - 删除角色", "确定要删除角色吗，此操作会删除角色的资源文件？", "确定", "取消"))
            {
                currentCharacter = null;
                AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(characterTable.characters[index]));
                characterTable.characters.RemoveAt(index);
                EditorUtility.SetDirty(characterTable);
            }
        }

        private void RemoveCharacterAtIndex(int index)
        {
            if (EditorUtility.DisplayDialog("多文 - 移除角色", "确定要从角色表移除角色吗？", "确定", "取消"))
            {
                currentCharacter = null;
                characterTable.characters.RemoveAt(index);
                EditorUtility.SetDirty(characterTable);
            }
        }

        /// <summary>
        /// 绘制编辑区域
        /// </summary>
        private void DrawEditArea()
        {
            GUILayout.BeginVertical(GUILayout.ExpandWidth(true));
            {
                if (characterTable != null)
                {
                    if (currentCharacter != null)
                    {
                        GUILayout.Label("角色 - " + characterTable.characters[selectedIndex].name, EditorStyles.boldLabel);
                        GUILayout.Space(10);
                        GUILayout.BeginHorizontal();
                        bool infoTabSelected = pageIndex == 0;
                        if (GUILayout.Toggle(infoTabSelected, "信息", EditorStyles.miniButtonLeft))
                        {
                            pageIndex = 0;
                        }

                        // 立绘选项卡
                        bool illustrationTabSelected = pageIndex == 1;
                        if (GUILayout.Toggle(illustrationTabSelected, "立绘", EditorStyles.miniButtonMid))
                        {
                            pageIndex = 1;
                        }

                        // 头像选项卡
                        bool avatarTabSelected = pageIndex == 2;
                        if (GUILayout.Toggle(avatarTabSelected, "头像", EditorStyles.miniButtonRight))
                        {
                            pageIndex = 2;
                        }
                        GUILayout.EndHorizontal();
                        GUILayout.Space(10);

                        // 信息页
                        if (pageIndex == 0)
                        {
                            EditorGUILayout.LabelField("信息", EditorStyles.boldLabel);

                            EditorGUI.BeginChangeCheck();
                            var newCharacterName = EditorGUILayout.TextField("角色名", currentCharacter.CharacterName);
                            EditorGUILayout.LabelField("角色描述");
                            var newCharacterDescription = EditorGUILayout.TextArea(currentCharacter.CharacterDescription, GUILayout.Height(100));
                            if (EditorGUI.EndChangeCheck())
                            {
                                Undo.RecordObject(currentCharacter, "修改角色信息");
                                currentCharacter.CharacterName = newCharacterName;
                                currentCharacter.CharacterDescription = newCharacterDescription;
                                EditorUtility.SetDirty(currentCharacter);
                            }
                        }
                        // 立绘页
                        else if (pageIndex == 1)
                        {
                            charaStateScrollPos = EditorGUILayout.BeginScrollView(charaStateScrollPos, GUILayout.ExpandHeight(true));
                            if (currentCharacter.CharacterStates == null)
                            {
                                currentCharacter.CharacterStates = new List<CharacterState>();
                            }
                            if (currentCharacter.CharacterStates != null)
                            {
                                // 显示图片
                                EditorGUILayout.LabelField("立绘", EditorStyles.boldLabel);
                                EditorGUILayout.Space(10);

                                EditorGUILayout.BeginHorizontal();

                                if (GUILayout.Button("添加立绘", GUILayout.Width(70)))
                                {
                                    currentCharacter.CharacterStates.Add(new CharacterState());
                                    EditorUtility.SetDirty(currentCharacter);
                                    AssetDatabase.SaveAssets();
                                    AssetDatabase.Refresh();
                                }

                                GUILayout.Space(10);

                                SpritePreview = GUILayout.Toggle(SpritePreview, "立绘预览", GUILayout.Width(70));

                                EditorGUILayout.EndHorizontal();

                                if (currentCharacter.CharacterStates.Count == 0)
                                {
                                    EditorGUILayout.HelpBox("当前角色没有立绘", MessageType.Info);
                                }

                                for (int i = 0; i < currentCharacter.CharacterStates.Count; i++)
                                {
                                    GUILayout.Space(20);
                                    if (currentCharacter.CharacterStates[i] != null)
                                    {
                                        EditorGUILayout.LabelField("立绘 " + (i + 1), EditorStyles.boldLabel);
                                    }

                                    EditorGUI.BeginChangeCheck();
                                    var newStateName = EditorGUILayout.TextField("状态名称", currentCharacter.CharacterStates[i].StateName);
                                    var newStateDescription = EditorGUILayout.TextField("状态描述", currentCharacter.CharacterStates[i].StateDescription);
                                    var newStateSprite = EditorGUILayout.ObjectField("立绘图片", currentCharacter.CharacterStates[i].StateSprite, typeof(Sprite), false) as Sprite;

                                    if (EditorGUI.EndChangeCheck())
                                    {
                                        Undo.RecordObject(currentCharacter, "修改角色状态");
                                        currentCharacter.CharacterStates[i].StateName = newStateName;
                                        currentCharacter.CharacterStates[i].StateDescription = newStateDescription;
                                        currentCharacter.CharacterStates[i].StateSprite = newStateSprite;
                                        EditorUtility.SetDirty(currentCharacter);
                                    }

                                    DrawValidationWarnings(currentCharacter.CharacterStates[i]);

                                    if (GUILayout.Button("删除立绘", GUILayout.Width(100)))
                                    {
                                        currentCharacter.CharacterStates.RemoveAt(i);
                                        EditorUtility.SetDirty(currentCharacter);
                                        AssetDatabase.SaveAssets();
                                        AssetDatabase.Refresh();
                                    }

                                    if (SpritePreview)
                                    {
                                        // 绘制图片
                                        if (currentCharacter.CharacterStates[i].StateSprite != null)
                                        {
                                            GUILayout.BeginHorizontal();
                                            GUILayout.FlexibleSpace();
                                            GUILayout.Label(currentCharacter.CharacterStates[i].StateSprite.texture, GUILayout.Height(390));
                                            GUILayout.FlexibleSpace();
                                            GUILayout.EndHorizontal();
                                        }
                                    }
                                }
                            }
                            else if (currentCharacter == null)
                            {
                                GUILayout.Label("请选择一个角色", EditorStyles.boldLabel);
                            }
                            GUILayout.EndScrollView();
                        }
                        else if (pageIndex == 2)
                        {
                            GUILayout.Label("头像（该功能暂未实现）", EditorStyles.boldLabel);
                            // TODO: 头像编辑器
                        }
                    }
                    else
                    {
                        EditorGUILayout.HelpBox("请选择编辑一个角色", MessageType.Info);
                    }
                }

                GUILayout.Space(20);
            }
            GUILayout.EndVertical();
        }

        private void DrawValidationWarnings(CharacterState bgm)
        {
            var warnings = new List<string>();

            if (string.IsNullOrEmpty(bgm.StateName))
            {
                warnings.Add("ID不能为空");
            }
            if (bgm.StateSprite == null)
            {
                warnings.Add("需要指定音频文件");
            }
            if (warnings.Count > 0)
            {
                EditorGUILayout.HelpBox(string.Join("\n", warnings), MessageType.Error);
            }
        }



        /// <summary>
        /// 绘制
        /// </summary>
        private void DrawMainContent()
        {
            if (characterTable == null)
            {
                EditorGUILayout.HelpBox("请选择或创建一个角色表", MessageType.Info);
                return;
            }

            EditorGUILayout.BeginHorizontal();
            {
                DrawCharacterList();
                GUILayout.Space(5);
                DrawEditArea();
            }
            EditorGUILayout.EndHorizontal();
        }


        private void OnGUI()
        {
            InitializeStyles();
            DrawTopbar();
            EditorGUILayout.Space();
            DrawMainContent();

            if (serializedTable != null && serializedTable.hasModifiedProperties)
            {
                serializedTable.ApplyModifiedProperties();
            }

            if (serializedCharacter != null && serializedCharacter.hasModifiedProperties)
            {
                serializedCharacter.ApplyModifiedProperties();
            }
        }
    }
}
