﻿using UnityEngine;
using UnityEditor;
using System;

namespace Pluliter.Editor
{
    /// <summary>
    /// 欢迎窗口
    /// </summary>
    public class WelcomeWindow : EditorWindow
    {
        private Texture2D bannerTexture;
        private const string BannerPath = "Packages/com.zhanlehall.pluliter/Documentation/assets/Banner.png";
        private static readonly Color HoverColor = new Color(0.1f, 0.8f, 1f, 0.3f);
        private GUIStyle buttonStyle;
        private GUIStyle titleStyle;
        private GUIStyle linkStyle;

        [MenuItem(PluliterEditorGlobal.RootMenuName + "欢迎")]
        public static void ShowWindow()
        {
            var window = GetWindow<WelcomeWindow>(true);
            window.titleContent = new GUIContent("Pluliter 多文 · 欢迎界面");
            window.minSize = new Vector2(960, 580); // 修改最小尺寸
            window.maxSize = new Vector2(960, 580); // 修改最大尺寸
            EditorPrefs.SetBool("PluliterWelcomeShown", true);
        }

        [InitializeOnLoadMethod]
        private static void OnInitialize()
        {
            EditorApplication.delayCall += () =>
            {
                if (!EditorPrefs.HasKey("PluliterWelcomeShown") ||
                    EditorPrefs.GetBool("PluliterWelcomeShown", true))
                {
                    ShowWindow();
                    EditorPrefs.SetBool("PluliterWelcomeShown", false);
                }
            };
        }

        private void OnEnable()
        {
            bannerTexture = AssetDatabase.LoadAssetAtPath<Texture2D>(BannerPath);
        }

        private void OnGUI()
        {
            buttonStyle = new GUIStyle(GUI.skin.button)
            {
                fontSize = 14,
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleCenter,
                fixedHeight = 45,
                padding = new RectOffset(15, 15, 7, 7),
                normal = { background = MakeTex(2, 2, new Color(0.2f, 0.6f, 1f, 0.9f)) },
                hover = { background = MakeTex(2, 2, new Color(0.3f, 0.7f, 1f, 1f)) },
                active = { background = MakeTex(2, 2, new Color(0.1f, 0.5f, 0.9f, 1f)) },
                border = new RectOffset(10, 10, 6, 6),
                richText = true
            };

            titleStyle = new GUIStyle(EditorStyles.label)
            {
                fontSize = 20,
                fontStyle = FontStyle.Normal,
                alignment = TextAnchor.MiddleCenter,
                normal = { textColor = Color.black }
            };

            linkStyle = new GUIStyle(EditorStyles.linkLabel)
            {
                fontSize = 18,
                alignment = TextAnchor.MiddleCenter,
                normal = { textColor = new Color(0.8f, 0.9f, 1f) },
                hover = { textColor = Color.white }
            };

            GUI.DrawTexture(new Rect(0, 0, position.width, position.height),
                MakeTex(1, 1, new Color(0.95f, 0.95f, 0.96f)));

            DrawBanner();
            DrawActionButtons();
            DrawFooter();
        }

        private void DrawBanner()
        {
            if (bannerTexture != null)
            {
                GUI.DrawTexture(new Rect(30, 20, position.width - 60, 260),
                    bannerTexture,
                    ScaleMode.ScaleAndCrop);
            }
            else
            {
                EditorGUI.DrawRect(new Rect(30, 20, position.width - 60, 200),
                    new Color(0.9f, 0.9f, 0.9f));
            }
        }

        private void DrawActionButtons()
        {
            GUILayout.BeginArea(new Rect(30, 300, position.width - 60, 200));

            GUILayout.BeginHorizontal();
            DrawIconButton("文档", "\U0001F4D6", "https://gitcode.com/shengjing/Pluliter/wiki");
            GUILayout.Space(15);
            DrawIconButton("源代码", "\uE12F", "https://gitcode.com/shengjing/Pluliter");
            GUILayout.Space(15);
            DrawIconButton("反馈", "\U0001F4AC", "https://gitcode.com/shengjing/Pluliter/issues/create/choose");
            GUILayout.EndHorizontal();

            GUILayout.Space(15);

            GUILayout.BeginHorizontal();
            DrawIconButton("许可证", "\uE1D8", "https://gitcode.com/shengjing/Pluliter/blob/master/LICENSE.md");
            GUILayout.Space(15);
            DrawIconButton("关于我们", "\uE13B", "https://gitcode.com/zhanlehall");
            GUILayout.Space(15);
            DrawIconButton("支持多文", "\U0001F4B8", "https://afdian.com/a/zhanlehall");
            GUILayout.EndHorizontal();

            // 赞助语
            GUILayout.Space(15);
            EditorGUI.LabelField(new Rect(0, 150, position.width - 60, 50),
                "您的支持是我们前进的最大动力！如果您欣赏我们的工作，并希望助力我们实现更多可能，欢迎通过爱发电平台给予支持。\n每一份贡献对我们来说都弥足珍贵，让我们携手共进，点亮未来！💡🤝", 
                new GUIStyle(titleStyle) { fontSize = 12, alignment = TextAnchor.UpperLeft });


            GUILayout.EndArea();
        }

        private void DrawIconButton(string text, string icon, string url)
        {
            var content = new GUIContent($"<size=16>{icon}</size>\n{text}");
            if (GUILayout.Button(content, buttonStyle, GUILayout.Width(220)))
            {
                Application.OpenURL(url);
                GUIUtility.ExitGUI();
            }
        }

        private void DrawFooter()
        {
            GUILayout.BeginArea(new Rect(0, position.height - 30, position.width, 30));
            EditorGUI.LabelField(new Rect(0, 0, position.width, 30),
                "Pluliter Copyright © 2025 Zhanle Hall, licensed under the Apache License, Version 2.0.",
                new GUIStyle(titleStyle) { fontSize = 14, alignment = TextAnchor.MiddleCenter });
            GUILayout.EndArea();
        }

        private Texture2D MakeTex(int width, int height, Color col)
        {
            Color[] pix = new Color[width * height];
            for (int i = 0; i < pix.Length; i++)
                pix[i] = col;
            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();
            return result;
        }
    }
}