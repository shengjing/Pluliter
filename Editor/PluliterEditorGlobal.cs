namespace Pluliter.Editor
{
    /// <summary>
    /// 多文编辑器全局变量
    /// </summary>
    public static class PluliterEditorGlobal
    {
        public const string RootMenuName = "Pluliter/";

    }
}
