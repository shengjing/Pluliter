﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Dialogue;
using UnityEditor;
using UnityEngine;
using System.Text;

namespace Pluliter.Editor
{
    /// <summary>
    /// 对话管理器编辑器
    /// </summary>
    //[CustomEditor(typeof(Pluliter.Dialogue.DialogueManager))]
    public class DialogueManagerEditor : UnityEditor.Editor
    {

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.LabelField("对话管理器", EditorStyles.boldLabel);

            GameObject gameObject = ((MonoBehaviour)target).gameObject;

            if (gameObject != null)
            {
                if (!gameObject.activeInHierarchy)
                {
                    EditorGUILayout.HelpBox("该脚本所在的游戏对象处于非激活状态，无法播放对话！", MessageType.Warning);
                }
            }

            var dialogueManagerObjs = FindObjectsByType<Pluliter.Dialogue.DialogueManager>(FindObjectsSortMode.None);
            if (dialogueManagerObjs != null)
            {
                if (dialogueManagerObjs.Length > 1)
                {
                    EditorGUILayout.HelpBox("场景中存在多个对话管理器，可能会出现问题，请检查！", MessageType.Warning);
                }
            }
            

            

            #region 对话配置
            var dialogueConfig = serializedObject.FindProperty("dialogueConfig");
            EditorGUILayout.PropertyField(dialogueConfig, new GUIContent("对话配置"));
            if (dialogueConfig.objectReferenceValue == null)
            {
                EditorGUILayout.HelpBox("请先设置对话配置！", MessageType.Error);

                // 在Assets中查找对话配置
                var dialogueConfigObjs = AssetDatabase.FindAssets("t:DialogueConfig");
                if (dialogueConfigObjs != null)
                {
                    if (dialogueConfigObjs.Length > 0)
                    {
                        if (dialogueConfigObjs.Length == 1)
                        {
                            if (GUILayout.Button("自动添加"))
                            {
                                Debug.Log("自动添加");
                                dialogueConfig.objectReferenceValue = AssetDatabase.LoadAssetAtPath<DialogueConfig>(AssetDatabase.GUIDToAssetPath(dialogueConfigObjs[0]));
                            }
                        }
                        else
                        {
                            string[] names = new string[dialogueConfigObjs.Length];
                            for (int i = 0; i < dialogueConfigObjs.Length; i++)
                            {
                                names[i] = AssetDatabase.GUIDToAssetPath(dialogueConfigObjs[i]);

                            }
                            var dialogueConfigNames = new StringBuilder();
                            if (names.Length > 1)
                            {
                                dialogueConfigNames.Append("请选择对话配置：\n");
                                for (int i = 0; i < names.Length; i++)
                                {
                                    dialogueConfigNames.Append(names[i] + "\n");
                                }
                                // 生成一个下拉菜单
                                var index = EditorGUILayout.Popup("对话配置", -1, names);
                                if (index >= 0)
                                {
                                    dialogueConfig.objectReferenceValue = AssetDatabase.LoadAssetAtPath<DialogueConfig>(names[index]);
                                }

                            }
                        }
                    }
                }
            }
            #endregion


            #region 对话框管理器
            var dialogueBoxManager = serializedObject.FindProperty("dialogueBoxManager");
            EditorGUILayout.PropertyField(dialogueBoxManager, new GUIContent("对话框管理器"));

            if (dialogueBoxManager.objectReferenceValue == null)
            {
                EditorGUILayout.HelpBox("请先设置对话框管理器！", MessageType.Error);

                var dialogueBoxManagerObjs = FindObjectsByType<Pluliter.Dialogue.DialogueBoxManager>(FindObjectsSortMode.None);
                if (dialogueBoxManagerObjs.Length > 0)
                {
                    if (dialogueBoxManagerObjs.Length == 1)
                    {
                        if (GUILayout.Button("自动添加"))
                        {
                            Debug.Log("自动添加");
                            dialogueBoxManager.objectReferenceValue = dialogueBoxManagerObjs[0];
                        }
                    }
                    if (dialogueBoxManagerObjs.Length >= 2)
                    {
                        string[] names = new string[dialogueBoxManagerObjs.Length - 1];
                        for (int i = 1; i < dialogueBoxManagerObjs.Length; i++)
                        {
                            names[i - 1] = dialogueBoxManagerObjs[i].name;
                        }
                        var managersName = new StringBuilder();
                        if (names.Length > 1)
                        {
                            for (int i = 0; i < names.Length; i++)
                            {
                                managersName.Append(names[i]);
                                managersName.Append(",");
                            }
                        }
                        else
                        {
                            managersName.Append(names[0]);
                        }
                        EditorGUILayout.HelpBox($"场景中存在{managersName}等多个对话框管理器，无法自动添加，请手动选择！", MessageType.Warning);
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("场景中未找到对话框管理器，请手动添加！", MessageType.Error);
                }
            }
            #endregion

            #region 背景管理器
            var backgroundManager = serializedObject.FindProperty("backgroundManager");
            EditorGUILayout.PropertyField(backgroundManager, new GUIContent("背景管理器"));

            if (backgroundManager.objectReferenceValue == null)
            {
                EditorGUILayout.HelpBox("请先设置背景管理器", MessageType.Error);
                var backgroundManagerObjs = FindObjectsByType<Pluliter.Background.BackgroundManager>(FindObjectsSortMode.None);
                
                if (backgroundManagerObjs.Length > 0)
                {
                    if (backgroundManagerObjs.Length == 1)
                    {
                        if (GUILayout.Button("自动添加"))
                        {
                            Debug.Log("自动添加");
                            backgroundManager.objectReferenceValue = backgroundManagerObjs[0];
                        }
                    }
                    if (backgroundManagerObjs.Length >= 2)
                    {
                        string[] names = new string[backgroundManagerObjs.Length - 1];
                        for (int i = 1; i < backgroundManagerObjs.Length; i++)
                        {
                            names[i - 1] = backgroundManagerObjs[i].name;

                        }
                        var managersName = new StringBuilder();
                        if (names.Length > 1)
                        {
                            for (int i = 0; i < names.Length; i++)
                            {
                                managersName.Append(names[i]);
                                managersName.Append(",");
                            }
                        }
                        else
                        {
                            managersName.Append(names[0]);
                        }
                        EditorGUILayout.HelpBox($"场景中存在{managersName}等多个背景管理器，无法自动添加，请手动选择！", MessageType.Warning);
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("场景中未找到背景管理器，请手动添加！", MessageType.Error);
                }
            }
            #endregion


            #region 演员管理器
            var actorManager = serializedObject.FindProperty("actorManager");
            EditorGUILayout.PropertyField(actorManager, new GUIContent("角色管理器"));
            if (actorManager.objectReferenceValue == null)
            {
                EditorGUILayout.HelpBox("请先设置角色管理器", MessageType.Error);
                var actorManagerObjs = FindObjectsByType<Pluliter.Actor.ActorManager>(FindObjectsSortMode.None);
                if (actorManagerObjs.Length > 0)
                {
                    if (actorManagerObjs.Length == 1)
                    {
                        if (GUILayout.Button("自动添加"))
                        {
                            Debug.Log("自动添加");
                            actorManager.objectReferenceValue = actorManagerObjs[0];
                        }
                    }
                    if (actorManagerObjs.Length >= 2)
                    {
                        string[] names = new string[actorManagerObjs.Length - 1];
                        for (int i = 1; i < actorManagerObjs.Length; i++)
                        {
                            names[i - 1] = actorManagerObjs[i].name;

                        }
                        var managersName = new StringBuilder();
                        if (names.Length > 1)
                        {
                            for (int i = 0; i < names.Length; i++)
                            {
                                managersName.Append(names[i]);
                                managersName.Append(",");
                            }
                        }
                        else
                        {
                            managersName.Append(names[0]);
                        }
                        EditorGUILayout.HelpBox($"场景中存在{managersName}等多个角色管理器，无法自动添加，请手动选择！", MessageType.Warning);
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("场景中未找到角色管理器，请手动添加！", MessageType.Error);
                }
            }

            #endregion

            #region 对话历史管理器

            var enableDialogueHistoryManager = serializedObject.FindProperty("EnabledDialogueHistory");

            EditorGUILayout.PropertyField(enableDialogueHistoryManager, new GUIContent("启用对话历史"));
            if (enableDialogueHistoryManager.boolValue)
            {
                var dialogueHistoryManager = serializedObject.FindProperty("dialogueHistoryManager");
                EditorGUILayout.PropertyField(dialogueHistoryManager, new GUIContent("对话历史管理器"));
                if (dialogueHistoryManager != null)
                {
                    if (dialogueHistoryManager.objectReferenceValue == null)
                    {
                        EditorGUILayout.HelpBox("请先设置对话历史管理器", MessageType.Error);
                        var dialogueHistoryManagerObjs = FindObjectsByType<Pluliter.Dialogue.DialogueHistoryManager>(FindObjectsSortMode.None);
                        if (dialogueHistoryManagerObjs.Length > 0)
                        {
                            if (dialogueHistoryManagerObjs.Length == 1)
                            {
                                if (GUILayout.Button("自动添加"))
                                {
                                    Debug.Log("自动添加");
                                    dialogueHistoryManager.objectReferenceValue = dialogueHistoryManagerObjs[0];
                                }
                            }
                            if (dialogueHistoryManagerObjs.Length >= 2)
                            {
                                string[] names = new string[dialogueHistoryManagerObjs.Length - 1];
                                for (int i = 1; i < dialogueHistoryManagerObjs.Length; i++)
                                {
                                    names[i - 1] = dialogueHistoryManagerObjs[i].name;

                                }
                                var managersName = new StringBuilder();
                                if (names.Length > 1)
                                {
                                    for (int i = 0; i < names.Length; i++)
                                    {
                                        managersName.Append(names[i]);
                                        managersName.Append(",");
                                    }
                                }
                                else
                                {
                                    managersName.Append(names[0]);
                                }
                                EditorGUILayout.HelpBox($"场景中存在{managersName}等多个对话历史管理器，无法自动添加，请手动选择！", MessageType.Warning);
                            }
                        }
                        else
                        {
                            EditorGUILayout.HelpBox("场景中未找到对话历史管理器，请手动添加！", MessageType.Error);
                        }
                    }
                }
            }
            else
            {
                EditorGUILayout.HelpBox("对话历史管理器未启用", MessageType.Info);
            }
            #endregion

            #region 对话wiki
            var enableDialogueWiki = serializedObject.FindProperty("EnabledDialogueWiki");
            #endregion
            serializedObject.ApplyModifiedProperties();
        }

    }
}
