﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;
using UnityEditor;

namespace Pluliter.Editor
{
    // TODO: 完善对话框管理器编辑器
    [CustomEditor(typeof(Pluliter.Dialogue.DialogueBoxManager))]
    public class DialogueBoxManagerEditor : UnityEditor.Editor
    {
        private SerializedProperty canvasProperty;

        enum CustomizeMode
        {
            [InspectorName("默认")]
            Normal,
            [InspectorName("自定义")]
            Customize
        }

        private CustomizeMode mode;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            // 获取组件所在的GameObject
            GameObject gameObject = ((MonoBehaviour)target).gameObject;

            #region Canvas组件检查
            // 尝试获取 Canvas 组件
            Canvas targetCanvas = gameObject.GetComponent<Canvas>();

            if (targetCanvas != null)
            {
                if (targetCanvas.enabled == false)
                {
                    EditorGUILayout.HelpBox("Canvas组件未启用，无法显示对话框", MessageType.Warning);
                    if (GUILayout.Button("启用Canvas组件"))
                    {
                        targetCanvas.enabled = true;
                    }
                }
                var rmod = targetCanvas.renderMode;
                if (rmod == RenderMode.ScreenSpaceOverlay || rmod == RenderMode.WorldSpace)
                {
                    EditorGUILayout.HelpBox($"Canvas渲染模式为{rmod.ToString()}，无法使用文本链接功能", MessageType.Warning);
                    // 操作按钮
                    if (GUILayout.Button("修改Canvas渲染模式为ScreenSpaceCamera"))
                    {
                        targetCanvas.renderMode = RenderMode.ScreenSpaceCamera;
                    }
                }
                else if (rmod == RenderMode.ScreenSpaceCamera)
                {
                    canvasProperty = serializedObject.FindProperty("UICamera");
                    EditorGUILayout.PropertyField(canvasProperty, new GUIContent("UI 摄像机"));
                    if (canvasProperty.objectReferenceValue == null)
                    {
                        EditorGUILayout.HelpBox("UI摄像机为空，无法使用文本链接功能", MessageType.Warning);
                        if (GUILayout.Button("应用UI相机"))
                        {
                            Camera uiCamera = targetCanvas.worldCamera;
                            if (uiCamera != null)
                            {
                                // 设置属性
                                canvasProperty.objectReferenceValue = uiCamera;
                            }
                        }
                    }
                }
            }
            else
            {
                EditorGUILayout.HelpBox("Canvas not found on the GameObject!", MessageType.Error);
            }

            #endregion

            #region 对话框组件自定义
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("对话框文本组件");
            mode = (CustomizeMode)EditorGUILayout.EnumPopup(mode);
            EditorGUILayout.EndHorizontal();
            if (mode == CustomizeMode.Customize)
            {
                var nameText = serializedObject.FindProperty("NameText");
                EditorGUILayout.PropertyField(nameText, new GUIContent("姓名文本框"));
                if (nameText.objectReferenceValue == null)
                {
                    EditorGUILayout.HelpBox("姓名文本框为空，无法显示角色姓名", MessageType.Warning);
                }
                var contentText = serializedObject.FindProperty("ContentText");
                EditorGUILayout.PropertyField(contentText, new GUIContent("对话文本框"));
                if (contentText.objectReferenceValue == null)
                {
                    EditorGUILayout.HelpBox("姓名文本框为空，无法显示对话内容", MessageType.Warning);
                }
                EditorGUILayout.HelpBox("请确保文本组件已正确设置", MessageType.Info);
                EditorGUILayout.Space();
                var dialogueBg = serializedObject.FindProperty("DialogueBoxImage");
                EditorGUILayout.PropertyField(dialogueBg, new GUIContent("对话框背景"));
                if (dialogueBg.objectReferenceValue == null)
                {
                    EditorGUILayout.HelpBox("对话框背景为空，无法正确显示对话框", MessageType.Warning);
                }

                var dialogueOptionPrefab = serializedObject.FindProperty("DialogueOptionPrefab");
                EditorGUILayout.PropertyField(dialogueOptionPrefab, new GUIContent("选项预制体"));
                if (dialogueOptionPrefab.objectReferenceValue == null)
                {
                    EditorGUILayout.HelpBox("选项预制体为空，无法显示选项", MessageType.Warning);
                }


            }
            #endregion

            //#region 对话Wiki
            //var wiki = serializedObject.FindProperty("dialogueWikiBox");
            //EditorGUILayout.PropertyField(wiki, new GUIContent("对话Wiki"));
            //#endregion

        serializedObject.ApplyModifiedProperties();
        }

    }
}
