# Pluliter 多文 | Unity 对话系统解决方案 🎮✨

![star](https://gitcode.com/shengjing/Pluliter/star/badge.svg)
![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)
![Unity Version](https://img.shields.io/badge/Unity-2022%2B-57b9d3.svg)
![Status](https://img.shields.io/badge/Status-Active-brightgreen.svg)
[![openupm](https://img.shields.io/npm/v/com.zhanlehall.pluliter?label=openupm&registry_uri=https://package.openupm.com)](https://openupm.com/packages/com.zhanlehall.pluliter/)

[简体中文](./README.md) | [English](./README-en.md)

✨ 盏乐堂工作室专为Unity开发者打造的对话系统框架，🚧 本项目正在积极开发中，欢迎 star ⭐️ 关注更新！

<p align="center">
  <img src="./Documentation/assets/Pluliter.png" alt="看板娘多文" width=396px>
</p>

<p align="center">
<span style="font-size:12px;">看板娘多文</span>
<span style="font-size:12px;">画师: Marukles</span>
</p>

## 🌟 项目介绍

Pluliter 多文 是一款专为Unity设计的强大对话系统解决方案，由盏乐堂工作室开发维护。它采用节点式的对话编辑器，让编排复杂的对话逻辑变得轻松简单；角色、背景、BGM等配置表完全图形化编辑，无需编写任何代码，降低了学习成本，让开发者能够快速创建自己的对话游戏。

### 📝 特性

- **灵活通用**：模块化设计允许根据需求启用相应的功能模块，适用于视觉小说、AVG类型的游戏，同样也适用于RPG、SLG、PZL等多种游戏类型的对话系统。
- **易于使用**：自带视觉小说创建模板，帮助用户迅速上手，减少开发周期。
- **全原生支持**：节点编辑器及各类配置表编辑器均基于Unity原生框架（UIElements和GraphView）构建，不依赖第三方插件，确保最佳兼容性和性能。
- **开源免费**：项目遵循Apache License 2.0开源许可证，可自由进行魔改自定义，以满足个性化需求。
- **高度可扩展**：代码注释详尽，广泛运用命令模式和工厂模式，具有很强的可扩展性，便于添加更多自定义对话逻辑。

- **可视化编辑器** - 节点式对话设计，轻松构建对话逻辑
- **扩展架构** - 支持扩展对话逻辑


### 📸 项目展示

<table>
  <!-- 第一组图片 -->
  <tr>
    <td><img src="./Documentation/assets/节点编辑器展示.png" alt="节点编辑器" style="width:100%"></td>
    <td><img src="./Documentation/assets/背景编辑器展示.png" alt="背景编辑器" style="width:100%"></td>
    <td><img src="./Documentation/assets/角色编辑器展示.png" alt="角色编辑器" style="width:100%"></td>
  </tr>
  <tr>
    <td align="center">可视化节点编辑器</td>
    <td align="center">动态背景配置界面</td>
    <td align="center">角色属性编辑器</td>
  </tr>

  <tr><td colspan="3" height="20"></td></tr>

  <!-- 第二组图片 -->
  <tr>
    <td><img src="./Documentation/assets/复杂的对话逻辑.png" alt="复杂的对话逻辑" style="width:100%"></td>
    <td><img src="./Documentation/assets/多种动画效果.png" alt="多种动画效果" style="width:100%"></td>
    <td><img src="./Documentation/assets/条件分支对话.png" alt="条件分支对话" style="width:100%"></td>
  </tr>
  <tr>
    <td align="center">复杂的对话逻辑</td>
    <td align="center">多种动画效果</td>
    <td align="center">条件分支对话</td>
  </tr>
</table>

<div align="center">🖱️ 点击图片查看大图</div>

## 🚀 快速入门

无论您是经验丰富的开发者还是刚入门的新手，Pluliter都提供了必要的工具和支持，让您能专注于创意实现。期待您利用Pluliter打造独一无二的游戏作品！

### 环境依赖要求

- Unity 2022.3 LTS 或更高版本
- Universal RP 2D 渲染管线
- 启用 TextMeshPro 并准备中文字体
- 从Unity资源商店安装 [Dotween](https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676) 并创建程序集

<img src="./Documentation/assets/创建Dotween程序集.png" width = "300" height = "auto" alt="图片名称" align=center />

- 可选安装编辑器插件 [Lingqiu-Inspector](https://gitcode.com/shengjing/Lingqiu-Inspector#%F0%9F%9A%80-%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8)，请自行添加程序集引用
- 在包管理器中安装 Newtonsoft.Json

⚠️ **重要提示**  一定要先安装 DOTween


### 安装指南

#### 方法一：通过 Git URL 安装（推荐）

> 注意，可能需要安装 [Git](https://git-scm.com/downloads)

1. 打开 Unity Editor ➔ 菜单栏选择 <kbd>Window</kbd> > <kbd>Package Manager</kbd>
2. 点击 ➕ 按钮 ➔ 选择 <kbd>Add package from git URL</kbd>
3. <kbd>Ctrl+V</kbd> 粘贴地址 ➔ 按回车确认安装


#### 方法二：通过 OpenUPM 安装

> 注意，OpenUPM的版本更新可能有延迟

链接：https://openupm.com/packages/com.zhanlehall.pluliter/

通过包管理器安装

请按照以下步骤操作：

1. 打开 Edit/Project Settings/Package Manager
2. 添加一个新的作用域注册表（或者编辑现有的 OpenUPM 条目）
   - 名称：`package.openupm.com`
   - URL：`https://package.openupm.com`
   - Scope(s)：`com.zhanlehall.pluliter`
3. 点击保存或应用
4. 打开 `窗口/包管理器`
5. 点击 `+` 号
6. 选择“通过名称添加包...”或“从Git URL添加包...”
7. 在名称中粘贴 `com.zhanlehall.pluliter`
8. 在版本中粘贴 `1.2.0-preview`
9. 点击添加

或者，您可以将以下代码片段合并到 `Packages/manifest.json` 文件中：

```json
{
    "scopedRegistries": [
        {
            "name": "package.openupm.com",
            "url": "https://package.openupm.com",
            "scopes": [
                "com.zhanlehall.pluliter"
            ]
        }
    ],
    "dependencies": {
        "com.zhanlehall.pluliter": "1.2.0-preview"
    }
}
```

这样就可以通过包管理器将指定的包添加到您的项目中了。

#### 方法二：本地安装

1. [下载最新发行版](https://gitcode.com/shengjing/Pluliter/releases)，或者直接在 `Packages` 目录Git Clone整个项目
2. 解压到项目 `Packages` 目录
3. 重启 Unity 编辑器，等待包管理器导入完成


## 🛠️ 开发指南

### 创建第一个对话

1. 右键项目创建一个对话章节资源文件并双击
2. 双击新建的对话资源进行可视化编辑
3. 拖拽节点构建对话流程

## 🤝 参与贡献

我们欢迎各种形式的贡献！可以参考以下流程：

1. Fork 项目仓库
2. 创建特性分支 (`git checkout -b feature/Feature`)
3. 提交修改 (`git commit -m 'Add some Feature'`)
4. 推送分支 (`git push origin feature/Feature`)
5. 发起 Pull Request

## 📬 反馈和建议

遇到问题或有建议？欢迎通过以下渠道反馈：

- 📧 盏乐堂工作室邮箱：[zhanlehall@163.com](mailto:zhanlehall@163.com)
- 🐞 问题追踪：[提交 Issue](https://gitcode.com/shengjing/Pluliter/issues)


## 📜 开源协议

本项目采用 **Apache License 2.0** 开源协议，允许：
- 商业使用 💰
- 修改发行 📝
- 专利授权 📜
- 个人使用 🙌

开源合规使用指南请查看 [开源合格使用指南](./Documentation/开源合规使用指南.md) 文件。

详细条款请查看 [LICENSE](./LICENSE) 文件。

## 💡 支持我们


您的支持是我们前进的最大动力！如果您欣赏我们的工作，并希望助力我们实现更多可能，欢迎通过[爱发电](https://afdian.com/a/zhanlehall)平台给予支持。每一份贡献对我们来说都弥足珍贵，让我们携手共进，点亮未来！💡🤝

