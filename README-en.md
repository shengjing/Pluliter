# Pluliter | Dialogue System Solution for Unity 🎮✨

![star](https://gitcode.com/shengjing/Pluliter/star/badge.svg)
![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)
![Unity Version](https://img.shields.io/badge/Unity-2022%2B-57b9d3.svg)
![Status](https://img.shields.io/badge/Status-Active-brightgreen.svg)
[![openupm](https://img.shields.io/npm/v/com.zhanlehall.pluliter?label=openupm&registry_uri=https://package.openupm.com)](https://openupm.com/packages/com.zhanlehall.pluliter/)

[简体中文](./README.md) | [English](./README-en.md)

✨ A professional dialogue system framework for Unity developers by Zhanlehall Studio. 🚧 Actively under development - star ⭐️ to stay updated!

<p align="center">
  <img src="./Documentation/assets/Pluliter.png" alt="Mascot Duowen" width=396px>
</p>

<p align="center">
<span style="font-size:12px;">Mascot Duowen</span><br>
<span style="font-size:12px;">Illustrator: Marukles</span>
</p>

## 🌟 Project Overview

Pluliter Duowen is a powerful dialogue system solution designed specifically for Unity, developed and maintained by Zhanlehall Studio. Featuring a node-based dialogue editor, it simplifies complex dialogue logic design. With fully graphical editing for characters, backgrounds, BGM configurations and more - no coding required! Empower developers to rapidly create dialogue-driven games.

### 📝 Key Features

- **Flexible & Versatile** - Modular design allows enabling specific features as needed. Suitable for visual novels/AVGs while equally effective for RPG, SLG, PZL and other game genres.
- **Easy Adoption** - Comes with visual novel templates for quick onboarding and reduced development cycles.
- **Fully Native Support** - Node editor and configuration tools built with Unity's native frameworks (UIElements/GraphView), ensuring optimal compatibility and performance without third-party dependencies.
- **Open & Free** - Licensed under Apache 2.0. Customize freely to meet your unique needs.
- **Highly Extensible** - Well-commented codebase utilizing Command Pattern and Factory Pattern for easy expansion of custom dialogue logic.

- **Visual Editor** - Node-based workflow for intuitive dialogue design
- **Extensible Architecture** - Supports custom dialogue logic extensions

### 📸 Project Showcase

<table>
  <!-- First Image Group -->
  <tr>
    <td><img src="./Documentation/assets/节点编辑器展示.png" alt="Node Editor" style="width:100%"></td>
    <td><img src="./Documentation/assets/背景编辑器展示.png" alt="Background Editor" style="width:100%"></td>
    <td><img src="./Documentation/assets/角色编辑器展示.png" alt="Character Editor" style="width:100%"></td>
  </tr>
  <tr>
    <td align="center">Visual Node Editor</td>
    <td align="center">Dynamic Background Configuration</td>
    <td align="center">Character Attribute Editor</td>
  </tr>

  <tr><td colspan="3" height="20"></td></tr>

  <!-- Second Image Group -->
  <tr>
    <td><img src="./Documentation/assets/复杂的对话逻辑.png" alt="Complex Dialogue Logic" style="width:100%"></td>
    <td><img src="./Documentation/assets/多种动画效果.png" alt="Multiple Animations" style="width:100%"></td>
    <td><img src="./Documentation/assets/条件分支对话.png" alt="Conditional Branches" style="width:100%"></td>
  </tr>
  <tr>
    <td align="center">Complex Dialogue Logic</td>
    <td align="center">Rich Animation Effects</td>
    <td align="center">Conditional Branching</td>
  </tr>
</table>

<div align="center">🖱️ Click images to view larger versions</div>

## 🚀 Quick Start

Whether you're a seasoned developer or just starting out, Pluliter provides the tools and support to bring your creative vision to life. Let's build something amazing!

### Requirements

- Unity 2022.3 LTS or newer
- Universal RP 2D Render Pipeline
- TextMeshPro enabled with Chinese font support
- [Dotween](https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676) installed from Asset Store
- Recommended editor plugin: [Lingqiu-Inspector](https://gitcode.com/shengjing/Lingqiu-Inspector#%F0%9F%9A%80-quick-start)
- Newtonsoft.Json via Package Manager

⚠️ **Important** - Must install DOTween and Lingqiu-Inspector first

### Installation

#### Method 1: Via Git URL (Recommended)

> Requires [Git](https://git-scm.com/downloads)

1. Open Unity Editor ➔ Navigate to <kbd>Window</kbd> > <kbd>Package Manager</kbd>
2. Click ➕ ➔ Select <kbd>Add package from git URL</kbd>
3. Paste URL ➔ Press Enter

#### Method 2: Via OpenUPM

> Note: OpenUPM updates may have delays

Package URL: https://openupm.com/packages/com.zhanlehall.pluliter/

Steps:
1. Open Edit/Project Settings/Package Manager
2. Add new scoped registry (or edit existing OpenUPM entry):
   - Name: `package.openupm.com`
   - URL: `https://package.openupm.com`
   - Scope(s): `com.zhanlehall.pluliter`
3. Save changes
4. Open Package Manager window
5. Click ➕ ➔ "Add package by name..."
6. Enter `com.zhanlehall.pluliter`
7. Version: `1.1.0-preview`
8. Click Add

Or merge this into `Packages/manifest.json`:
```json
{
    "scopedRegistries": [
        {
            "name": "package.openupm.com",
            "url": "https://package.openupm.com",
            "scopes": [
                "com.zhanlehall.pluliter"
            ]
        }
    ],
    "dependencies": {
        "com.zhanlehall.pluliter": "1.1.0-preview"
    }
}
```

#### Method 3: Local Installation

1. [Download latest release](https://gitcode.com/shengjing/Pluliter/releases) or clone into Packages directory
2. Extract to project's `Packages` folder
3. Restart Unity Editor

## 🛠️ Development Guide

### Create Your First Dialogue

1. Right-click in Project window ➔ Create Dialogue Chapter Asset
2. Double-click the new asset to open editor
3. Drag-and-drop nodes to design dialogue flow

## 🤝 Contributing

We welcome contributions! Here's how:

1. Fork the repository
2. Create feature branch: `git checkout -b feature/Feature`
3. Commit changes: `git commit -m 'Add some Feature'`
4. Push branch: `git push origin feature/Feature`
5. Create Pull Request

## 📬 Feedback & Support

Encounter issues or have suggestions? Reach us via:

- 📧 Email: [zhanlehall@163.com](mailto:zhanlehall@163.com)
- 🐞 Issue Tracker: [Submit Issue](https://gitcode.com/shengjing/Pluliter/issues)

## 📜 License

Licensed under **Apache License 2.0**. Permits:
- Commercial use 💰
- Modification & distribution 📝
- Patent use 📜
- Private use 🙌

For compliance details, see [Open Source Compliance Guide](./Documentation/开源合规使用指南.md).

Full license terms at [LICENSE](./LICENSE).