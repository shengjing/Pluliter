# 第三方依赖声明

本项目使用了以下第三方库，其许可条款及版权信息如下：

## DOTween
- **官方链接**：http://dotween.demigiant.com/  
- **许可证**：[DOTween License](https://dotween.demigiant.com/license.php)  
- **版权声明**：© 2014 Daniele Giardini  


## Newtonsoft.Json
- **官方链接**：https://github.com/JamesNK/Newtonsoft.Json  
- **许可证**：[MIT License](https://github.com/JamesNK/Newtonsoft.Json/blob/master/LICENSE.md)  
- **版权声明**：© 2007 James Newton-King  
