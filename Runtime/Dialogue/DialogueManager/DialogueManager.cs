﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Pluliter.Character;
using Pluliter.Actor;
using Pluliter.Background;
using Pluliter.Audio;
using Pluliter.Blackboard;
using Pluliter.Input;
#if LINGQIU
using LingqiuInspector.Tools;
#endif

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 对话管理器
    /// </summary>
    public class DialogueManager : MonoBehaviour, IDialogueManager, IPauseable
    {
        private static DialogueManager instance;
        public static DialogueManager Instance
        {
            get
            {
                if (instance == null)
                {
#if UNITY_2023_1_OR_NEWER
                    instance = FindFirstObjectByType<DialogueManager>();
#else
                    instance = FindObjectOfType<DialogueManager>();
#endif
                    if (instance == null)
                    {
                        GameObject go = new("DialogueManager");
                        instance = go.AddComponent<DialogueManager>();
                        Debug.LogWarning($"没有找到对话管理器，创建新对话管理器{go.GetInstanceID()}");
                    }
                }
                return instance;
            }
        }

        /// <summary>
        /// 对话配置
        /// </summary>
#if LINGQIU
        [InspectorLabelText("对话配置", "dialogueConfig")]
#endif
        public DialogueConfig dialogueConfig;


        /// <summary>
        /// 对话数据
        /// </summary>
        [SerializeField]
#if LINGQIU
        [ReadOnlyLabel]
#endif
        private DialogueChapter dialogueChapter;

        /// <summary>
        /// 当前对话节点
        /// </summary>
        [SerializeField]
#if LINGQIU
        [ReadOnlyLabel]
#endif
        private DialogueNode currentDialogueNode;

        private bool justEnter = false;

        /// <summary>
        /// 输入管理器
        /// </summary>
        [SerializeField]
        private PluliterInputManager inputManager;

        /// <summary>
        /// 音频管理器
        /// </summary>
        private DialogueAudioManager AudioManager => DialogueAudioManager.Instance;

        /// <summary>
        /// 对话黑板
        /// </summary>
        [SerializeField]
        private DialogueBlackboard blackboard;


        /// <summary>
        /// 对话状态
        /// </summary>
#if LINGQIU
        [InspectorLabelText("对话状态", "dialogueState")]
#endif
        public DialogueState dialogueState = DialogueState.OFF;

        public DialogueBoxManager dialogueBoxManager;

        public BackgroundManager backgroundManager;

        public ActorManager actorManager;

        public DialogueHistoryManager dialogueHistoryManager;

        /// <summary>
        /// 当对话开始时
        /// </summary>
        public UnityEvent OnDialogueStart;

        /// <summary>
        /// 当对话结束时
        /// </summary>
        public UnityEvent OnDialogueEnd;



        [SerializeField]
#if LINGQIU
        [ReadOnlyLabel]
#endif
        private bool autoPlay = false;

        // 0是禁用状态，1是自动播放
        public Sprite[] autoPlayButtonSprites;

        public Button autoPlayButton;

        private IDialogueCommandFactory CommandFactory => new DialogueCommandFactory();


        private void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(this);
                return;
            }
            instance = this;
        }

        /// <summary>
        /// 初始化对话管理器
        /// </summary>
        public void InitDialogueManager()
        {
            justEnter = false;

            if (dialogueBoxManager == null)
            {
                Debug.LogError("对话框管理器为空！");
            }
            
            // 设置字体
            dialogueBoxManager.SetFont(dialogueConfig.dialogueFont);

            if (dialogueConfig.EnabledDialogueHistory)
            {
                dialogueHistoryManager.gameObject.SetActive(false);
            }
            if (dialogueConfig == null)
            {
                Debug.LogError("对话配置为空！");
                return;
            }
            if (dialogueConfig.dialogueDataList.Count == 0)
            {
                Debug.LogError("对话章节列表为空！");
#if UNITY_EDITOR
                // 暂停播放
                Debug.Break();
#endif
                return;
            }

            // 初始化对话章节
            dialogueChapter = dialogueConfig.dialogueDataList[0];

            if (dialogueChapter == null)
            {
                Debug.LogError("对话章节为空！");
#if UNITY_EDITOR
                // 暂停播放
                Debug.Break();
#endif
                return;
            }

            if (dialogueChapter.BgmTable == null)
            {
                Debug.LogWarning("对话章节的背景音乐表为空！");
            }

            

            // 订阅对话暂停事件
            dialogueBoxManager.OnDialogueContentPause += OnDialogueContentPause;
            if (dialogueConfig.EnabledDialogueHistory)
            {
                dialogueHistoryManager.OnDialogueHistoryPause += OnDialogueContentPause;
            }

            if (autoPlayButton != null)
            {
                autoPlayButton.onClick.AddListener(SetAutoPlay);
            }

            inputManager.OnClickEvent += (pos) =>
            {
                DialogueBoxClicked();
            };


        }

        /// <summary>
        /// 开始对话
        /// </summary>
        public void StartDialogue()
        {
            Debug.Log("StartDialogue");

            // 初始化对话
            if (dialogueChapter.DialogueNodes.Count <= 0)
            {
                Debug.LogWarning("对话章节的对话节点列表为空！");
                StopDialogue();
                return;
            }
            currentDialogueNode = dialogueChapter.StartNode;

            dialogueBoxManager.ShowDialogueBox(0.5f, () =>
            {
                SwitchState(DialogueState.PLAYING);

                OnDialogueStart?.Invoke();
            });
        }

        /// <summary>
        /// 停止对话
        /// </summary>
        public void StopDialogue()
        {
            Debug.Log("StopDialogue");
            dialogueBoxManager.DestroyAllOptions();
            dialogueBoxManager.HideDialogueBox(0.5f, () =>
            {
                backgroundManager.ClearBackground();
                actorManager.RemoveAllActor();
                OnDialogueEnd?.Invoke();
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        private void OnDialogueContentPause(bool arg)
        {
            if (arg)
            {
                Pause();
            }
            else
            {
                Resume();
            }
        }

        /// <summary>
        /// 设置自动播放
        /// </summary>
        public void SetAutoPlay()
        {
            autoPlay = !autoPlay;
            if (autoPlay)
            {
                autoPlayButton.GetComponent<Image>().sprite = autoPlayButtonSprites[1];
                if (dialogueState == DialogueState.NEXTONE)
                {
                    PlayNextNode();
                }
            }
            else
            {
                autoPlayButton.GetComponent<Image>().sprite = autoPlayButtonSprites[0];
            }
        }

        private void Update()
        {
            if (justEnter)
            {
                justEnter = false;
                switch (dialogueState)
                {
                    case DialogueState.OFF:
                        StopDialogue();
                        break;
                    case DialogueState.PLAYING:
                        ProcessDialogue(currentDialogueNode);
                        break;
                    case DialogueState.PAUSE:
                        break;
                    case DialogueState.RESUME:
                        break;
                    case DialogueState.NEXTONE:
                        break;
                }
            }
        }

        /// <summary>
        /// 对话框点击
        /// </summary>
        public void DialogueBoxClicked()
        {
            switch (dialogueState)
            {
                case DialogueState.OFF:
                    Debug.LogWarning("对话已结束");
                    break;
                case DialogueState.PLAYING:
                    Debug.LogWarning("对话播放中");
                    // TODO: 暂停打字
                    // dialogueBoxManager.PauseTyping();
                    // SwitchState(DialogueState.NEXTONE);
                    break;
                case DialogueState.PAUSE:
                    Debug.LogWarning("对话已暂停");
                    break;
                case DialogueState.RESUME:
                    SwitchState(DialogueState.PLAYING);
                    break;
                case DialogueState.NEXTONE:
                    PlayNextNode();
                    break;
            }
        }

        /// <summary>
        /// 下一个对话节点
        /// </summary>
        private void PlayNextNode()
        {
            if (!currentDialogueNode)
            {
                Debug.LogWarning("当前对话节点为空");
                SwitchState(DialogueState.OFF);
            }
            else
            {
                if (currentDialogueNode.NextDialogueNode == null)
                {
                    Debug.LogWarning("对话已结束");
                    SwitchState(DialogueState.OFF);
                }
                else
                {
                    currentDialogueNode = currentDialogueNode.NextDialogueNode;
                    //Debug.Log("下一个对话节点：" + currentDialogueNode);
                    SwitchState(DialogueState.PLAYING);
                }
            }

        }

        /// <summary>
        /// 处理对话
        /// </summary>
        /// <param name="dialogueNode"></param>
        public void ProcessDialogue(DialogueNode dialogueNode)
        {
            // TODO: 为了处理选项，需要后续优化逻辑
            currentDialogueNode = dialogueNode;
            // Debug.Log("处理对话"+dialogueNode);
            IDialogueCommand command = CommandFactory.CreateDialogueCommand(dialogueNode);
            if (command != null)
            {
                command.Execute(this, OnDialogueProcessed);
            }
            else
            {
                Debug.LogError("未找到对应的对话命令");
            }
        }

        /// <summary>
        /// 对话处理完成
        /// </summary>
        /// <param name="autoNext">是否自动播放下一个节点</param>
        public void OnDialogueProcessed(bool autoNext)
        {
            if (autoPlay)
            {
                float autoPlayReadTime = currentDialogueNode.DialogueContent.Length * 0.1f;
                autoPlayReadTime = Mathf.Clamp(autoPlayReadTime, dialogueConfig.autoPlayMinReadTime, dialogueConfig.autoPlayMaxReadTime);
                // Debug.Log("自动播放时间：" + autoPlayReadTime);
                Invoke(nameof(PlayNextNode), autoPlayReadTime);
            }
            else
            {
                if (autoNext)
                {
                    PlayNextNode();
                }
                else
                {
                    SwitchState(DialogueState.NEXTONE);
                }
            }
        }

        public DialogueBoxManager GetDialogueBoxManager()
        {
            return dialogueBoxManager;
        }

        public BackgroundManager GetBackgroundManager()
        {
            return backgroundManager;
        }

        public ActorManager GetActorManager()
        {
            return actorManager;
        }

        public CharacterTable GetCharacterList()
        {
            return dialogueChapter.characterTable;
        }

        public BackgroundTable GetBackgroundList()
        {
            return dialogueChapter.backgroundTable;

        }

        public BgmTable GetBGMList()
        {
            return dialogueChapter.BgmTable;
        }
        public DialogueAudioManager GetAudioManager()
        {
            return AudioManager;
        }

        public DialogueHistoryManager GetDialogueHistoryManager()
        {
            if (dialogueConfig.EnabledDialogueHistory)
            {
                return dialogueHistoryManager;
            }
            return null;
        }


        /// <summary>
        /// 切换状态
        /// </summary>
        /// <param name="state">状态</param>
        public void SwitchState(DialogueState state)
        {
            dialogueState = state;
            justEnter = true;
        }

        public void Pause()
        {
            SwitchState(DialogueState.PAUSE);
            dialogueBoxManager.Pause();
            actorManager.Pause();
        }

        public void Resume()
        {
            dialogueBoxManager.Resume();
            actorManager.Resume();
            SwitchState(DialogueState.RESUME);
        }

        public int GetChapterIndex()
        {
            return dialogueConfig.dialogueDataList.IndexOf(dialogueChapter);
        }

        public int GetNodeIndex()
        {
            return dialogueChapter.DialogueNodes.IndexOf(currentDialogueNode);
        }

        /// <summary>
        /// 跳转到指定章节
        /// </summary>
        /// <param name="chapterIndex"></param>
        public void JumpToChapter(int chapterIndex)
        {
            if (chapterIndex < 0 || chapterIndex >= dialogueConfig.dialogueDataList.Count)
            {
                Debug.LogError($"对话章节索引 {chapterIndex} 超出范围");
                return;
            }
            OnDialogueEnd?.Invoke();
            dialogueBoxManager.DestroyAllOptions();
            backgroundManager.ClearBackground();
            actorManager.RemoveAllActor();
            dialogueChapter = dialogueConfig.dialogueDataList[chapterIndex];
            StartDialogue();
        }

        public void SetChapterIndex(int chapterIndex)
        {
            if (chapterIndex < 0 || chapterIndex >= dialogueConfig.dialogueDataList.Count)
            {
                Debug.LogError($"对话章节索引 {chapterIndex} 超出范围");
            }
            var chapter = dialogueConfig.dialogueDataList[chapterIndex];
            if (chapter == dialogueChapter)
            {
                return;
            }
            dialogueChapter = chapter;

            StopDialogue();
            StartDialogue();

        }

        /// <summary>
        /// 跳转到指定节点
        /// </summary>
        /// <param name="nodeIndex"></param>
        public void SetNodeIndex(int nodeIndex)
        {
            if (nodeIndex < 0 || nodeIndex >= dialogueChapter.DialogueNodes.Count)
            {
                Debug.LogError($"对话节点索引 {nodeIndex} 超出范围");
            }
            currentDialogueNode = dialogueChapter.DialogueNodes[nodeIndex];
            ProcessDialogue(currentDialogueNode);
        }

        /// <summary>
        /// 获取章节名称
        /// </summary>
        /// <returns></returns>
        public string GetChapterName()
        {
            return dialogueChapter.ChapterName;
        }

        public DialogueConfig GetDialogueConfig()
        {
            return dialogueConfig;
        }

        public DialogueBlackboard GetBlackboard()
        {
            return blackboard;
        }

        public SoundEffectTable GetSoundEffectList()
        {
            return dialogueChapter.soundEffectTable;
        }

    }
}