﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Events;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 对话历史记录
    /// </summary>
    public class DialogueHistoryManager : MonoBehaviour
    {
        // 回到顶部
        public Button BackToTopButton;
        
        /// <summary>
        /// 对话历史记录框
        /// </summary>
        public ScrollRect HistoryScrollRect;
        /// <summary>
        /// 关闭按钮
        /// </summary>
        public Button CloseButton;

        /// <summary>
        /// 对话历史记录面板
        /// </summary>
        public GameObject ContentPanel;

        /// <summary>
        /// 对话历史记录框预制体
        /// </summary>
        public GameObject DialogueBoxPrefab;

        /// <summary>
        /// 对话历史记录
        /// </summary>
        public Queue<(string name, string content)> historyDialogues = new Queue<(string name, string content)>();

        /// <summary>
        /// 对话历史记录框
        /// </summary>
        public List<GameObject> historyBoxes = new List<GameObject>();

        /// <summary>
        /// 角色名预设颜色
        /// </summary>
        public List<Color> NameColors = new List<Color> 
        {
            Color.yellow, 
            Color.cyan, 
            Color.grey,
            Color.green,
            Color.blue,
            Color.magenta
        };

        public Color DialogueTextColor = Color.gray;

        public Color DialogueTextHighlightColor = Color.white;

        public UnityAction<bool> OnDialogueHistoryPause;

        /// <summary>
        /// 角色名颜色字典
        /// </summary>
        public Dictionary<string, Color> NameColorDic = new Dictionary<string, Color>();

        private void Start()
        {
            CloseButton.onClick.AddListener(HidePanel);
            if (BackToTopButton != null)
            {
                BackToTopButton.onClick.AddListener(() =>
                {
                    HistoryScrollRect.verticalNormalizedPosition = 1f;
                });
            }
        }

        public void ShowPanel()
        {
            OnDialogueHistoryPause?.Invoke(true);
            gameObject.SetActive(true);
            HistoryScrollRect.verticalNormalizedPosition = 0f;
            if (historyBoxes.Count > 0)
            {
                Debug.Log(historyBoxes.Count);
                historyBoxes[historyBoxes.Count - 1].GetComponent<DialogueHistoryBox>().SetDialogueTextColor(DialogueTextHighlightColor);
            }
            
        }

        public void HidePanel()
        {
            OnDialogueHistoryPause?.Invoke(false);
            gameObject.SetActive(false);
            if (historyBoxes.Count > 0)
            {
                historyBoxes[historyBoxes.Count - 1].GetComponent<DialogueHistoryBox>().SetDialogueTextColor(DialogueTextColor);
            }
            
        }


        /// <summary>
        /// 添加对话
        /// </summary>
        /// <param name="node">对话节点</param>
        public void AddDialogue(string name, string content)
        {
            historyDialogues.Enqueue((name, content));
            GameObject dialogueBox = Instantiate(DialogueBoxPrefab, ContentPanel.transform);
            
            if (NameColorDic.TryGetValue(name, out Color color))
            {
                dialogueBox.GetComponent<DialogueHistoryBox>().SetColor(color);
            }
            else
            {
                Color newcolor = NameColors[UnityEngine.Random.Range(0, NameColors.Count)];
                while (NameColorDic.ContainsValue(newcolor))
                {
                    newcolor = NameColors[UnityEngine.Random.Range(0, NameColors.Count)];
                }
                NameColorDic.Add(name, newcolor);
                dialogueBox.GetComponent<DialogueHistoryBox>().SetColor(newcolor);
            }
            dialogueBox.GetComponent<DialogueHistoryBox>().SetDialogue($"【{name}】", content);
            dialogueBox.GetComponent<DialogueHistoryBox>().SetDialogueTextColor(DialogueTextColor);
            historyBoxes.Add(dialogueBox);
            if (historyBoxes.Count > 0)
            {
                historyBoxes[historyBoxes.Count - 1].GetComponent<DialogueHistoryBox>().SetDialogueTextColor(DialogueTextColor);
                historyBoxes[historyBoxes.Count - 1].GetComponent<DialogueHistoryBox>().SetDialogueTextColor(DialogueTextHighlightColor);
            }
        }

        public void AddDialogueToUI()
        {
            foreach (var dialogue in historyDialogues)
            {
                GameObject dialogueBox = Instantiate(DialogueBoxPrefab, ContentPanel.transform);
                if (NameColorDic.TryGetValue(dialogue.name, out Color color))
                {
                    dialogueBox.GetComponent<DialogueHistoryBox>().SetColor(color);

                }
                else
                {
                    Color newcolor = NameColors[UnityEngine.Random.Range(0, NameColors.Count)];
                    while (NameColorDic.ContainsValue(newcolor))
                    {
                        newcolor = NameColors[UnityEngine.Random.Range(0, NameColors.Count)];
                    }
                    NameColorDic.Add(dialogue.name, newcolor);
                }
                dialogueBox.GetComponent<DialogueHistoryBox>().SetDialogue(dialogue.name, dialogue.content);
            }
        }


        /// <summary>
        /// 清空对话历史记录
        /// </summary>
        public void ClearHistory()
        {
            historyDialogues.Clear();
            NameColorDic.Clear();
        }


    }
}
