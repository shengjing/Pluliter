﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using Pluliter.Animation;
using Pluliter.Editor.Config;
using System.Collections;
using System.Text;
using Pluliter.LWTween;
#if DOTWEEN
using DG.Tweening;
#endif


namespace Pluliter.Dialogue
{
    /// <summary>
    /// 打字机效果
    /// </summary>
    public enum TypewriterEffect
    {
        [InspectorName("普通")]
        Normal,
        [InspectorName("字符淡入")]
        FadeCharacter,
        [InspectorName("无")]
        None
    }
    /// <summary>
    /// 对话框管理器，负责显示对话内容以及选项
    /// </summary>
    public class DialogueBoxManager : MonoBehaviour, IPauseable
    {
        /// <summary>
        /// 多文配置
        /// </summary>
        private PluliterConfig pluliterConfig;

        /// <summary>
        /// 动画引擎
        /// </summary>
        private AnimationEngine animationEngine;

        /// <summary>
        /// 打字机效果
        /// </summary>
        private TypewriterEffect typewriterEffect;

        /// <summary>
        /// 打字机隐藏字数
        /// </summary>
        private float typewriterFadeRange;

#if DOTWEEN
        private Tween contentTween;
#endif
        private bool isPause = false;

        private Action<bool> contentExecuteCallback = null;

        private PlayerInput playerInput;

        public TMP_Text NameText;
        
        public TMP_Text ContentText;

        public Image DialogueBoxImage;

        private bool isDialogueBoxShow = false;

        public GameObject ButtonGroup;

        public GameObject DialogueOptionPrefab;

        public Camera UICamera;

        public UnityAction<bool> OnDialogueContentPause;

        private Coroutine fadeTypeCoroutine;

        private Coroutine typeCoroutine;

        private Action onPauseTyping = null;

        public UnityEvent<bool> OnNextOne;

        private void Awake()
        {
            pluliterConfig = Resources.Load<PluliterConfig>("PluliterConfig");

            if (pluliterConfig != null)
            {
                animationEngine = pluliterConfig.animationEngine;
                typewriterEffect = pluliterConfig.typewriterEffect;
                typewriterFadeRange = pluliterConfig.TypewriterFadeRange;
            }
            else
            {
                Debug.LogError("未找到PluliterConfig.asset");
            }
        }

        private void Start()
        {
#if DOTWEEN
            contentTween = null;
#endif
            NameText.text = string.Empty;
            ContentText.text = string.Empty;
            //// 如果运行平台是安卓
            //if (Application.platform == RuntimePlatform.Android)
            //{
            //    inputActions.MobileDialogueAction.Wiki.performed += OnPhoneWikiClickLink;
            //}
            //// 如果运行平台是PC
            //else if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
            //{
            //    inputActions.PCDialogueAction.Wiki.performed += OnMouseClickLink;
            //}
            //if (dialogueWikiBox == null)
            //{
            //    dialogueWikiBox = GetComponentInChildren<DialogueWikiBox>();
            //}
            //dialogueWikiBox.gameObject.SetActive(false);
            //dialogueWikiBox.boxClicked += () =>
            //{
            //    OnDialogueContentPause?.Invoke(false);
            //};
        }

        public void OnPhoneWikiClickLink(InputAction.CallbackContext callbackContext)
        {
            if (callbackContext.performed)
            {
                int linkIndex = TMP_TextUtilities.FindIntersectingLink(ContentText, callbackContext.ReadValue<Vector2>(), UICamera);
                if (linkIndex != -1)
                {
                    TMP_LinkInfo linkInfo = ContentText.textInfo.linkInfo[linkIndex];
                    //dialogueWikiBox.gameObject.SetActive(true);
                    OnDialogueContentPause?.Invoke(true);
                }
            }
        }

        public void OnMouseClickLink(InputAction.CallbackContext callbackContext)
        {
            if (callbackContext.performed)
            {
                int linkIndex = TMP_TextUtilities.FindIntersectingLink(ContentText, Mouse.current.position.ReadValue(), UICamera);
                if (linkIndex != -1)
                {
                    TMP_LinkInfo linkInfo = ContentText.textInfo.linkInfo[linkIndex];
                    //Debug.LogError(linkInfo.GetLinkID() + " " + linkInfo.GetLinkText());
                    //dialogueWikiBox.gameObject.SetActive(true);
                    OnDialogueContentPause?.Invoke(true);
                }
            }
        }

        /// <summary>
        /// 设置对话
        /// </summary>
        /// <param name="name"></param>
        /// <param name="content"></param>
        /// <param name="rate"></param>
        public void SetDialogue(string name, string content, float rate, Action<bool> executeCallback, bool narration)
        {
            OnNextOne?.Invoke(false);

            if (!narration)
            {
                NameText.text = name;
                //SetDialogueImage();
            }
            else
            {
                NameText.text = string.Empty;
                //SetDialogueImage();
            }

            if (typewriterEffect == TypewriterEffect.None)
            {
                ContentText.text = content;
                OnNextOne?.Invoke(true);
                contentExecuteCallback = executeCallback;
                contentExecuteCallback?.Invoke(false);
                contentExecuteCallback = null;
            }
            else if (typewriterEffect == TypewriterEffect.Normal)
            {
                if (animationEngine == AnimationEngine.DOTween)
                {
#if DOTWEEN
                    contentExecuteCallback = executeCallback;
                    if (isDialogueBoxShow)
                    {
                        contentTween = DOTween.To(() => string.Empty, value => ContentText.text = value, content, rate * content.Length).SetEase(Ease.Linear).SetOptions(true);
                    }
                    else if (!isDialogueBoxShow)
                    {
                        contentTween = DOTween.To(() => string.Empty, value => ContentText.text = value, content, rate * content.Length).SetEase(Ease.Linear).SetOptions(true).SetDelay(0.52f);
                    }
                    contentTween.OnKill(() =>
                    {
                        ContentText.text = content;
                        contentExecuteCallback?.Invoke(false);
                        contentExecuteCallback = null;
                        OnNextOne?.Invoke(true);
                        contentTween = null;
                    });
                    onPauseTyping = () =>
                    {
                        if (contentTween != null && contentTween.IsPlaying())
                        {
                            contentTween.Kill();
                        }
                        OnNextOne?.Invoke(true);
                    };
#endif
#if !DOTWEEN
                Debug.LogError("没有安装DOTween，请安装或者在项目设置中选择其他动画引擎");
#endif
                }
                else if (animationEngine == AnimationEngine.LWTween)
                {
                    // 富文本打字机协程
                    IEnumerator TypeDialogue()
                    {
                        ContentText.text = string.Empty;
                        yield return new WaitUntil(() => isDialogueBoxShow);
                        contentExecuteCallback = executeCallback;

                        var contentTextBuilder = new StringBuilder();
                        var beginTagBuilder = new StringBuilder();
                        var endTagBuilder = new StringBuilder();

                        for (int i = 0; i < content.Length; i++)
                        {
                            if (content[i] == '<')
                            {
                                if (i + 1 < content.Length && content[i + 1] == '/')
                                {
                                    endTagBuilder.Clear();
                                    while (content[i] != '>')
                                    {
                                        endTagBuilder.Append(content[i]);
                                        i++;
                                    }

                                    endTagBuilder.Append('>');
                                    contentTextBuilder.Append(endTagBuilder);
                                }
                                else
                                {
                                    beginTagBuilder.Clear();
                                    while (content[i] != '>')
                                    {
                                        beginTagBuilder.Append(content[i]);
                                        i++;
                                    }
                                    beginTagBuilder.Append(">");
                                    contentTextBuilder.Append(beginTagBuilder);
                                }
                            }
                            else
                            {
                                contentTextBuilder.Append(content[i]);
                                ContentText.text = contentTextBuilder.ToString();
                                yield return new WaitForSeconds(rate * 2f);
                            }
                            yield return new WaitUntil(() => !isPause);
                            yield return new WaitUntil(() => isDialogueBoxShow);
                        }
                        
                        contentExecuteCallback?.Invoke(false);
                        contentExecuteCallback = null;
                        OnNextOne?.Invoke(true);
                    }
                    typeCoroutine = StartCoroutine(TypeDialogue());
                    onPauseTyping = () =>
                    {
                        ContentText.text = content;
                        contentExecuteCallback?.Invoke(false);
                        contentExecuteCallback = null;
                        OnNextOne?.Invoke(true);
                    };
                }
            }
            else if (typewriterEffect == TypewriterEffect.FadeCharacter)
            {
                // 淡入打字机协程
                IEnumerator FadeTypewriter()
                {
                    ContentText.text = string.Empty;
                    yield return new WaitUntil(() => isDialogueBoxShow);
                    ContentText.SetText(content);
                    contentExecuteCallback = executeCallback;
                    bool typing = true;

                    // 确保字符处于可见状态
                    var textInfo = ContentText.textInfo;
                    ContentText.maxVisibleCharacters = textInfo.characterCount;
                    ContentText.ForceMeshUpdate();

                    // 先将所有字符设置到透明状态
                    for (int i = 0; i < textInfo.characterCount; i++)
                    {
                        SetCharacterAlpha(i, 0);
                    }
                    // 按时间逐渐显示字符
                    byte outputSpeed = (byte)(1 / rate);
                    var timer = 0f;
                    var interval = 1.0f / outputSpeed;
                    var headCharacterIndex = 0;
                    while (typing)
                    {
                        timer += Time.deltaTime;
                        // 计算字符顶点颜色透明度
                        var isFadeCompleted = true;
                        var tailIndex = headCharacterIndex - typewriterFadeRange + 1;
                        for (int i = headCharacterIndex; i > -1 && i >= tailIndex; i--)
                        {
                            // 不处理不可见字符，否则可能导致某些位置的字符闪烁
                            if (!textInfo.characterInfo[i].isVisible)
                            {
                                continue;
                            }

                            var step = headCharacterIndex - i;
                            var alpha = (byte)Mathf.Clamp((timer / interval + step) / typewriterFadeRange * 255, 0, 255);

                            isFadeCompleted &= alpha == 255;
                            SetCharacterAlpha(i, alpha);
                        }

                        ContentText.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

                        // 检查是否完成字符输出
                        if (timer >= interval)
                        {
                            if (headCharacterIndex < textInfo.characterCount - 1)
                            {
                                timer = 0;
                                headCharacterIndex++;
                            }
                            else if (isFadeCompleted)
                            {
                                typing = false;

                                fadeTypeCoroutine = null;
                                // 触发输出完成回调

                                contentExecuteCallback?.Invoke(false);
                                contentExecuteCallback = null;
                                OnNextOne?.Invoke(true);
                                yield break;
                            }
                        }
                        yield return new WaitUntil(() => !isPause);

                        yield return null;
                    }

                    void SetCharacterAlpha(int index, byte alpha)
                    {
                        var materialIndex = ContentText.textInfo.characterInfo[index].materialReferenceIndex;
                        var vertexColors = ContentText.textInfo.meshInfo[materialIndex].colors32;
                        var vertexIndex = ContentText.textInfo.characterInfo[index].vertexIndex;

                        vertexColors[vertexIndex + 0].a = alpha;
                        vertexColors[vertexIndex + 1].a = alpha;
                        vertexColors[vertexIndex + 2].a = alpha;
                        vertexColors[vertexIndex + 3].a = alpha;
                    }
                }
                // TODO: 修复第一句不打印的Bug，目前方案是调用两次
                fadeTypeCoroutine = StartCoroutine(FadeTypewriter());
                fadeTypeCoroutine = StartCoroutine(FadeTypewriter());
                onPauseTyping = () =>
                {
                    Debug.LogWarning($"当前{typewriterEffect}未实现暂停结束功能");
                };

            }
        }

        /// <summary>
        /// 设置对话框背景图片
        /// </summary>
        /// <param name="sprite"></param>
        public void SetDialogueImage(Sprite sprite)
        {
            DialogueBoxImage.sprite = sprite;
        }


        /// <summary>
        /// 显示对话框
        /// </summary>
        /// <param name="duration"></param>
        public void ShowDialogueBox(float duration, Action callback)
        {
            if (animationEngine == AnimationEngine.LWTween)
            {
                DialogueBoxImage.gameObject.SetActive(true);
                if (DialogueBoxImage.color.a != 1)
                {
                    DialogueBoxImage.FadeColor(1f, duration).SetOnKill(() =>
                    {
                        isDialogueBoxShow = true;
                        callback?.Invoke();
                    });
                }
                else
                {
                    isDialogueBoxShow = true;
                    callback?.Invoke();
                }

            }
            else if (animationEngine == AnimationEngine.DOTween)
            {
#if DOTWEEN
                DialogueBoxImage.gameObject.SetActive(true);
                NameText.gameObject.SetActive(true);
                ContentText.gameObject.SetActive(true);

                if (DialogueBoxImage.color.a == 1)
                {
                    isDialogueBoxShow = true;
                    callback?.Invoke();
                    return;
                }
                Sequence sequence = DOTween.Sequence();
                sequence.OnKill(() =>
                {
                    isDialogueBoxShow = true;
                    callback?.Invoke();
                });
                sequence.Append(ContentText.DOFade(1, duration));
                sequence.Join(NameText.DOFade(1, duration));
                sequence.Join(DialogueBoxImage.DOFade(1f, duration));
                sequence.Play();
#endif

            }
            else if (animationEngine == AnimationEngine.None)
            {
                DialogueBoxImage.gameObject.SetActive(true);
                isDialogueBoxShow = true;
                callback?.Invoke();
            }
        }



        /// <summary>
        /// 隐藏对话框
        /// </summary>
        /// <param name="duration"></param>
        public void HideDialogueBox(float duration, Action callback)
        {
            if (animationEngine == AnimationEngine.LWTween)
            {
                isDialogueBoxShow = false;
                DialogueBoxImage.DOFade(0, duration).OnKill(() =>
                {
                    DialogueBoxImage.gameObject.SetActive(false);
                });

            }
            else if (animationEngine == AnimationEngine.DOTween)
            {
#if DOTWEEN
                Sequence sequence = DOTween.Sequence();
                sequence.OnKill(() =>
                {
                    ContentText.text = string.Empty;
                    NameText.text = string.Empty;
                    ContentText.gameObject.SetActive(false);
                    NameText.gameObject.SetActive(false);
                    DialogueBoxImage.gameObject.SetActive(false);
                    callback?.Invoke();
                });
                sequence.Append(ContentText.DOFade(0, duration));
                sequence.Join(NameText.DOFade(0, duration));
                sequence.Join(DialogueBoxImage.DOFade(0, duration));
                Debug.Log("对话框隐藏完成");
#else
                Debug.LogError("DOTween没有安装");
                DialogueBoxImage.gameObject.SetActive(false);
#endif
            }
            else if (animationEngine == AnimationEngine.None)
            {
                DialogueBoxImage.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// 添加单个对话选项
        /// </summary>
        /// <param name="optionText">选项文本</param>
        /// <param name="selectCallback">选项被选择后的回调函数</param>
        /// <returns></returns>
        private DialogueOptionButton AddDialogueOption(string optionText, Action selectCallback)
        {
            var optionObj = Instantiate(DialogueOptionPrefab, ButtonGroup.transform);
            optionObj.GetComponentInChildren<Text>().text = optionText;
            var opt = optionObj.AddComponent<DialogueOptionButton>();
            opt.SetOption(optionText, () =>
            {
                if (selectCallback != null)
                {
                    selectCallback();
                }
            });
            return opt;
        }

        /// <summary>
        /// 添加对话选项
        /// </summary>
        /// <param name="dialogueOptions">对话选项</param>
        /// <param name="dialogueManager">对话管理器</param>
        /// <param name="executeCallback">执行回调函数</param>
        /// <returns></returns>
        public List<DialogueOptionButton> AddOptions(List<DialogueOption> dialogueOptions, IDialogueManager dialogueManager, Action<DialogueNode> executeCallback)
        {
            DestroyAllOptions();

            List<DialogueOptionButton> optionButtons = new List<DialogueOptionButton>();
            foreach(DialogueOption option in dialogueOptions)
            {
                var dialogueOptionButton = AddDialogueOption(option.optionText, () =>
                {
                    DestroyAllOptions();
                    if (executeCallback != null)
                    {
                        executeCallback(option.OptionNextDialogueNode);
                    }
                });
                optionButtons.Add(dialogueOptionButton);
            }
            
            return optionButtons;
        }

        /// <summary>
        /// 销毁所有对话选项
        /// </summary>
        public void DestroyAllOptions()
        {
            foreach (Transform child in ButtonGroup.transform)
            {
                Destroy(child.gameObject);
            }
        }


        /// <summary>
        /// 暂停打字机
        /// </summary>
        public void PauseTyping()
        {
#if DOTWEEN
            if (contentTween != null && contentTween.IsPlaying())
            {
                contentTween.Kill();
            }
#endif
            if (fadeTypeCoroutine != null)
            {
                StopCoroutine(fadeTypeCoroutine);
            }

            if (typeCoroutine != null)
            {
                StopCoroutine(typeCoroutine);
            }
            onPauseTyping?.Invoke();
        }

        /// <summary>
        /// 暂停对话
        /// </summary>
        public void Pause()
        {
            isPause = true;
#if DOTWEEN
            if (animationEngine == AnimationEngine.DOTween)
            {
                contentTween.Pause();
            }
#endif
        }

        /// <summary>
        /// 继续对话
        /// </summary>
        public void Resume()
        {
            isPause = false;
#if DOTWEEN
            if (animationEngine == AnimationEngine.DOTween)
            {
                contentTween.Play();
            }
#endif
        }

        /// <summary>
        /// 设置对话字体
        /// </summary>
        /// <param name="dialogueFont"></param>
        public void SetFont(TMP_FontAsset dialogueFont)
        {
            NameText.font = dialogueFont;
            ContentText.font = dialogueFont;
        }
    }
}
