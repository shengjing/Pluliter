﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


namespace Pluliter
{
    /// <summary>
    /// 可暂停接口，用于暂停和继续对话
    /// </summary>
    public interface IPauseable
    {
        /// <summary>
        /// 暂停       
        /// </summary>
        void Pause();

        /// <summary>
        /// 继续
        /// </summary>
        void Resume();
    }
}
