﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;
using UnityEngine;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 对话指令工厂
    /// </summary>
    public class DialogueCommandFactory : IDialogueCommandFactory
    {
        public IDialogueCommand CreateDialogueCommand(DialogueNode dialogueNode)
        {
            if (dialogueNode == null)
            {
                throw new ArgumentNullException(nameof(dialogueNode));
            }

            var dialogueType = dialogueNode.dialogueType;

            switch (dialogueType)
            {
                // 开始对话节点
                case DialogueNodeType.StartSingle:
                    return new StartSingleDialogueCommand();
                // 结束对话节点
                case DialogueNodeType.StopSingle:
                    return new StopSingleDialogueCommand();
                // 普通对话
                case DialogueNodeType.NormalDialogue:
                    var characterName = dialogueNode.DialogueCharacterName;
                    var dialogueContent = dialogueNode.DialogueContent;
                    var rate = dialogueNode.AnimationRate;
                    return new NormalDialogueCommand(characterName, dialogueContent, rate);
                // 全屏对话
                case DialogueNodeType.FullScreenDialogue:
                    var fullScreenDialogueContent = dialogueNode.DialogueContent;
                    return new FullScreenDialogueCommand(fullScreenDialogueContent);
                // 旁白对话
                case DialogueNodeType.Narration:
                    var narrationContent = dialogueNode.DialogueContent;
                    var narrate = dialogueNode.AnimationRate;
                    return new NarrationCommand(narrationContent, narrate);
                // 选项对话
                case DialogueNodeType.Choice:
                    var choiceOptions = dialogueNode.DialogueOptions;
                    return new ChoiceDialogueCommand(choiceOptions);
                // 显示角色
                case DialogueNodeType.MoveInActor:
                    var charaID = dialogueNode.CharacterId;
                    var stateID = dialogueNode.CharacterStateId;
                    var actorScale = dialogueNode.ActorScale;
                    var actorPosition = dialogueNode.ActorPosition;
                    var mianim = dialogueNode.ActorMoveInAnimationType;
                    var moutanim = dialogueNode.ActorMoveOutAnimationType;
                    var duration = dialogueNode.AnimationDuration;
                    // TODO:
                    // 需要分成两个类型进行优化，太乱了
                    return new ShowActorCommand(charaID, stateID, actorScale, actorPosition, mianim, moutanim, duration);
                // 晃动角色
                case DialogueNodeType.ShakeActor:
                    var shakeActorID = dialogueNode.CharacterId;
                    var shakeDuration = dialogueNode.AnimationDuration;
                    return new ShakeActorCommand(shakeActorID, shakeDuration);
                // 移动角色
                case DialogueNodeType.ModifyActor:
                    var moveActorID = dialogueNode.CharacterId;
                    var moveDuration = dialogueNode.AnimationDuration;
                    var moveTargetPositon = dialogueNode.ActorMoveTargetPositon;
                    var moStateId = dialogueNode.CharacterStateId;
                    var modifyPosition = dialogueNode.ModifyActorPosition;
                    var modifyState = dialogueNode.ModifyActorState;
                    return new ModifyActorCommand(moveActorID, moStateId, moveTargetPositon, moveDuration, modifyPosition, modifyState);
                // 退场角色
                case DialogueNodeType.MoveOutActor:
                    var exitActorID = dialogueNode.CharacterId;
                    var exitanim = dialogueNode.ActorMoveOutAnimationType;
                    var exitDuration = dialogueNode.AnimationDuration;
                    return new ExitActorCommand(exitActorID, exitanim, exitDuration);
                // 背景切换
                case DialogueNodeType.Transition:
                    var bgid = dialogueNode.BackgroundId;
                    var transitionType = dialogueNode.BackgroundTransitionType;
                    var bgDuration = dialogueNode.AnimationDuration;
                    return new TransitionCommand(bgid, transitionType, bgDuration);
                // 播放BGM
                case DialogueNodeType.PlayBgm:
                    var bgmId = dialogueNode.BgmId;
                    var bgmLoop = dialogueNode.LoopBgm;
                    return new PlayBgmCommand(bgmId, bgmLoop);
                // 停止BGM
                case DialogueNodeType.StopBgm:
                    return new StopBgmCommand();
                // 播放音效
                case DialogueNodeType.PlaySoundEffect:
                    var soundEffectId = dialogueNode.SoundEffectId;
                    return new PlaySoundEffectCommand(soundEffectId);
                // 停止音效
                case DialogueNodeType.StopSoundEffect:
                    return new StopSoundEffectCommand();
                // 跳转章节
                case DialogueNodeType.JumpToChapter:
                    var chapterIndex = dialogueNode.JumpChapterId;
                    return new JumpToChapterCommand(chapterIndex);
                // 条件分支
                case DialogueNodeType.ConditionBranch:
                    var conditionBranch = dialogueNode.ConditionBranch;
                    return new ConditionBranchCommand(conditionBranch);
                // 设置变量
                case DialogueNodeType.SetVariable:
                    var data = dialogueNode.BlackboardDataVariables;
                    return new SetVariableCommand(data);
                default:
                    throw new ArgumentException($"未知对话命令类型，无法创建命令{ dialogueType }");
            }
        }
    }
}