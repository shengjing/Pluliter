﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 对话选项类
    /// </summary>
    [System.Serializable]
    public class DialogueOption
    {
        /// <summary>
        /// 对话选项ID
        /// </summary>
        public string optionID;

        /// <summary>
        /// 对话选项的文本
        /// </summary>
        public string optionText;

        /// <summary>
        /// 对话选项跳转的节点
        /// </summary>
        public DialogueNode OptionNextDialogueNode;
    }
}
