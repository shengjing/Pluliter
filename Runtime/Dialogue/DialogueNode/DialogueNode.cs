﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Actor;
using Pluliter.Blackboard;
using System.Collections.Generic;
using UnityEngine;


namespace Pluliter.Dialogue
{
    /// <summary>
    /// 对话节点类，用于存储对话数据
    /// </summary>
    [System.Serializable]
    public class DialogueNode : ScriptableObject
    {
        /// <summary>
        /// 对话节点类型
        /// </summary>
        [Tooltip("对话节点类型")]
        public DialogueNodeType dialogueType = DialogueNodeType.NormalDialogue;

        /// <summary>
        /// 对话角色姓名
        /// </summary>
        [Tooltip("角色姓名")]
        public string DialogueCharacterName = "对话角色姓名";

        /// <summary>
        /// 对话内容
        /// </summary>
        [UnityEngine.Multiline]
        public string DialogueContent = "对话内容";

        /// <summary>
        /// 对话选项
        /// </summary>
        public List<DialogueOption> DialogueOptions;

        /// <summary>
        /// 角色ID
        /// </summary>
        public string CharacterId;

        /// <summary>
        /// 是否改变角色状态
        /// </summary>
        public bool ModifyActorState;

        /// <summary>
        /// 角色状态
        /// </summary>
        public string CharacterStateId;

        /// <summary>
        /// 演员立绘缩放
        /// </summary>
        public float ActorScale = 0.5f;

        /// <summary>
        /// 角色立绘位置
        /// </summary>
        public Vector2 ActorPosition = new();

        /// <summary>
        /// 演员入场动画类型
        /// </summary>
        public MoveInAnimation ActorMoveInAnimationType = MoveInAnimation.None;

        /// <summary>
        /// 演员退场动画类型
        /// </summary>
        public MoveOutAnimation ActorMoveOutAnimationType = MoveOutAnimation.None;

        /// <summary>
        /// 过度动画时间，用于控制演员入场、演员退场、背景切换等动画的持续时间
        /// </summary>
        public float AnimationDuration = 0.5f;

        /// <summary>
        /// 动画速率
        /// </summary>
        public float AnimationRate = 0.02f;

        /// <summary>
        /// 是否移动演员到目标位置
        /// </summary>
        public bool ModifyActorPosition;

        /// <summary>
        /// 目标移动位置
        /// </summary>
        public Vector2 ActorMoveTargetPositon = new();

        /// <summary>
        /// 背景ID
        /// </summary>
        public string BackgroundId;

        /// <summary>
        /// 背景过渡类型
        /// </summary>
        public Background.TransitionAnimType BackgroundTransitionType;

        /// <summary>
        /// 背景音乐ID
        /// </summary>
        public string BgmId;

        /// <summary>
        /// 背景音乐循环
        /// </summary>
        public bool LoopBgm;

        /// <summary>
        /// 音效ID
        /// </summary>
        public string SoundEffectId;

        /// <summary>
        /// 音效循环
        /// </summary>
        public bool LoopSoundEffect;

        /// <summary>
        /// 跳转章节ID
        /// </summary>
        public string JumpChapterId;

        /// <summary>
        /// 复合条件分支
        /// </summary>
        public CompositeCondition ConditionBranch;

        /// <summary>
        /// 黑板数据变量
        /// </summary>
        public BlackboardDataVariables BlackboardDataVariables;

        /// <summary>
        /// 下一节点
        /// </summary>
        public DialogueNode NextDialogueNode;

        /// <summary>
        /// 节点在节点树视图中的位置
        /// </summary>
        public Vector2 NodeViewPosition = new();

        public DialogueNode()
        {

        }
    }
}