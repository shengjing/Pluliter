﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using System.Collections.Generic;
using UnityEngine;
using TMPro;
#if LINGQIU
using LingqiuInspector.Tools;
#endif

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 对话章节列表类，用于存储游戏中所有的对话数据
    /// </summary>
    [CreateAssetMenu(fileName = "对话配置", menuName = "多文Pluliter/对话配置")]
    public class DialogueConfig : ScriptableObject
    {
        /// <summary>
        /// 对话配置名称
        /// </summary>
#if LINGQIU
        [InspectorLabelText("对话名称")]
#endif
        public string dialogueListName;

        /// <summary>
        /// 对话列表描述
        /// </summary>
#if LINGQIU
        [InspectorLabelText("对话列表描述")]
#endif
        public string dialogueListDescription;

        /// <summary>
        /// 对话预览内容
        /// </summary>
#if LINGQIU
        [InspectorLabelText("对话回顾内容")]
#endif
        public string DialogueReviewContent;

        /// <summary>
        /// 对话字体
        /// </summary>
#if LINGQIU
        [InspectorLabelText("对话字体")]
#endif
        public TMP_FontAsset dialogueFont;

        /// <summary>
        /// 是否自动加载对话，默认为true
        /// </summary>
#if LINGQIU
        [InspectorLabelText("是否自动加载对话")]
#endif
        public bool isAutoLoad = true;

        /// <summary>
        /// 自动加载对话列表的起始章节索引，默认为0
        /// </summary>
#if LINGQIU
        [InspectorLabelText("起始章节索引")]
#endif
        public int autoLoadChapterIndex = 0;

        /// <summary>
        /// 是否启用自动播放，默认为true
        /// </summary>
#if LINGQIU
        [InspectorLabelText("自动播放功能")]
        [BoolDropdown("开启", "关闭")]
#endif
        public bool enableAutoPlay = true;

        /// <summary>
        /// 是否在进入对话时自动播放，默认为false
        /// </summary>
#if LINGQIU
        [InspectorLabelText("进入对话时自动播放")]
        [BoolDropdown("是", "否")]
#endif
        public bool enableAutoPlayOnEnter = false;

#if LINGQIU
        [InspectorLabelText("最长自动播放速度", "autoPlayMaxReadTime")]
#endif
        public float autoPlayMaxReadTime = 2.6f;

#if LINGQIU
        [InspectorLabelText("最短自动播放速度", "autoPlayMinReadTime")]
#endif
        public float autoPlayMinReadTime = 1.8f;

        /// <summary>
        /// 是否启用存档系统，默认为true
        /// </summary>
#if LINGQIU
        [InspectorLabelText("启用存档系统功能", "EnableArchiveSystem")]
        [BoolDropdown("开启", "关闭")]
#endif
        public bool EnableArchiveSystem = true;

        /// <summary>
        /// 是否启用自动存档，默认为true
        /// </summary>
#if LINGQIU
        [InspectorLabelText("启用存档系统功能", "EnableArchiveSystem")]
        [BoolDropdown("开启", "关闭")]
#endif
        public bool EnableAutoSave = true;

        /// <summary>
        /// 是否启用对话历史记录，默认为true
        /// </summary>
#if LINGQIU
        [InspectorLabelText("是否启用对话历史记录功能", "enableArchiveSystem")]
        [BoolDropdown("开启", "关闭")]
#endif
        public bool EnabledDialogueHistory = true;

#if UNITY_EDITOR
        [Header("对话章节列表")]
#endif
        public List<DialogueChapter> dialogueDataList;
    }
}
