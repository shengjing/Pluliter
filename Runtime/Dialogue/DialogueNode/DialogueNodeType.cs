﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 对话节点类型
    /// </summary>
    public enum DialogueNodeType
    {
        /// <summary>
        /// 对话起点，用于触发新对话流程（不可连接上游节点）
        /// </summary>
        [InspectorName("开始对话")]
        StartSingle,

        /// <summary>
        /// 对话终点，用于结束当前对话（不可连接下游节点）
        /// </summary>
        [InspectorName("结束对话")]
        StopSingle,

        /// <summary>
        /// 基础对话节点
        /// </summary>
        [InspectorName("普通对话")]
        NormalDialogue,

        /// <summary>
        /// 全屏文字对话（暂未实现）
        /// </summary>
        [InspectorName("全屏对话（暂未实现）")]
        FullScreenDialogue,

        /// <summary>
        /// 旁白（暂未实现）
        /// </summary>
        [InspectorName("旁白对话（暂未实现）")]
        Narration,

        /// <summary>
        /// 玩家选项分支节点（可连接多个下游选项路径）
        /// </summary>
        [InspectorName("对话选项")]
        Choice,

        /// <summary>
        /// 角色入场
        /// </summary>
        [InspectorName("演员入场")]
        MoveInActor,

        /// <summary>
        /// 角色晃动
        /// </summary>
        [InspectorName("演员晃动")]
        ShakeActor,

        /// <summary>
        /// 修改角色属性（立绘状态、位置等）
        /// </summary>
        [InspectorName("修改演员状态")]
        ModifyActor,

        /// <summary>
        /// 角色退场（需指定角色ID和退场动画）
        /// </summary>
        [InspectorName("演员退场")]
        MoveOutActor,

        /// <summary>
        /// 背景切换
        /// </summary>
        [InspectorName("背景切换")]
        Transition,

        /// <summary>
        /// 播放背景音乐
        /// </summary>
        [InspectorName("播放背景音乐")]
        PlayBgm,

        /// <summary>
        /// 停止背景音乐
        /// </summary>
        [InspectorName("停止背景音乐")]
        StopBgm,

        /// <summary>
        /// 播放音效
        /// </summary>
        [InspectorName("播放音效")]
        PlaySoundEffect,

        /// <summary>
        /// 停止音效
        /// </summary>
        [InspectorName("停止音效")]
        StopSoundEffect,

        /// <summary>
        /// 跨章节跳转节点
        /// </summary>
        [InspectorName("跳转到章节")]
        JumpToChapter,

        /// <summary>
        /// 条件分支节点
        /// </summary>
        [InspectorName("条件分支")]
        ConditionBranch,

        /// <summary>
        /// 变量操作节点
        /// </summary>
        [InspectorName("设置变量")]
        SetVariable
    }
}