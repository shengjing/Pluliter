﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using System.Collections.Generic;
using UnityEngine;
using Pluliter.Background;
using Pluliter.Character;
using Pluliter.Audio;
#if LINGQIU
using LingqiuInspector.Tools;
#endif

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 对话章节，包含多个对话节点以及对话角色表和背景表
    /// </summary>
    [System.Serializable]
    [CreateAssetMenu(fileName = "新章节", menuName = "多文Pluliter/对话章节")]
    public class DialogueChapter : ScriptableObject
    {
        /// <summary>
        /// 章节名称
        /// </summary>
#if LINGQIU
        [InspectorLabelText("章节名称")]
#endif
        public string ChapterName = "新章节";

        /// <summary>
        /// 对话配置所有者
        /// </summary>
        public DialogueConfig owner = null;

        /// <summary>
        /// 对话起点，每个对话章节应只有一个对话起点
        /// </summary>
#if LINGQIU
        [InspectorLabelText("对话开始节点")]
#endif
        public DialogueNode StartNode = null;

        /// <summary>
        /// 背景表
        /// </summary>
#if LINGQIU
        [InspectorLabelText("背景表")]
#endif
        public BackgroundTable backgroundTable = null;

        /// <summary>
        /// 角色表
        /// </summary>
#if LINGQIU
        [InspectorLabelText("角色表")]
#endif
        public CharacterTable characterTable = null;

        /// <summary>
        /// 背景音乐表
        /// </summary>
#if LINGQIU
        [InspectorLabelText("背景音乐表")]
#endif
        public BgmTable BgmTable = null;

        /// <summary>
        /// 音效表
        /// </summary>
#if LINGQIU
        [InspectorLabelText("音效表")]
#endif
        public SoundEffectTable soundEffectTable = null;


        /// <summary>
        /// 章节对话节点
        /// </summary>
        public List<DialogueNode> DialogueNodes = null;

    }
}
