﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 对话选项按钮，用于显示对话选项并处理选项点击事件
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class DialogueOptionButton : MonoBehaviour
    {
        /// <summary>
        /// 选项被选择时的回调函数
        /// </summary>
        public Action OnOptionSelect;

        private Button optionButton;
        private string optionText;

        void Start()
        {
            optionButton = GetComponent<Button>();
            optionButton.onClick.AddListener(OnClick);
        }

        public void SetOption(string text, Action selectCallback)
        {
            optionText = text;
            OnOptionSelect = selectCallback;
        }

        public void OnClick()
        {
            if (OnOptionSelect != null)
            {
                OnOptionSelect();
                OnOptionSelect = null;
            }
        }
    }
    
}
