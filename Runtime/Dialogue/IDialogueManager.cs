﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Actor;
using Pluliter.Audio;
using Pluliter.Background;
using Pluliter.Blackboard;
using Pluliter.Character;


namespace Pluliter.Dialogue
{
    /// <summary>
    /// 对话管理器接口
    /// </summary>
    public interface IDialogueManager
    {
        /// <summary>
        /// 初始化对话管理器
        /// </summary>
        void InitDialogueManager();

        /// <summary>
        /// 开始对话
        /// </summary>
        void StartDialogue();

        /// <summary>
        /// 结束对话
        /// </summary>
        void StopDialogue();

        /// <summary>
        /// 处理对话
        /// </summary>
        /// <param name="dialogueNode">对话节点</param>
        void ProcessDialogue(DialogueNode dialogueNode);

        /// <summary>
        /// 获取对话框管理器
        /// </summary>
        /// <returns></returns>
        DialogueBoxManager GetDialogueBoxManager();

        /// <summary>
        /// 获取角色管理器
        /// </summary>
        /// <returns></returns>
        ActorManager GetActorManager();

        /// <summary>
        /// 获取背景管理器
        /// </summary>
        /// <returns></returns>
        BackgroundManager GetBackgroundManager();

        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <returns></returns>
        CharacterTable GetCharacterList();

        /// <summary>
        /// 获取背景列表
        /// </summary>
        /// <returns></returns>
        BackgroundTable GetBackgroundList();

        /// <summary>
        /// 获取BGM列表
        /// </summary>
        /// <returns></returns>
        BgmTable GetBGMList();

        /// <summary>
        /// 获取音效表
        /// </summary>
        /// <returns></returns>
        SoundEffectTable GetSoundEffectList();

        /// <summary>
        /// 获取对话历史管理器
        /// </summary>
        /// <returns></returns>
        DialogueHistoryManager GetDialogueHistoryManager();

        /// <summary>
        /// 获取音频管理器
        /// </summary>
        /// <returns></returns>
        DialogueAudioManager GetAudioManager();


        /// <summary>
        /// 跳转到指定章节
        /// </summary>
        /// <param name="chapterIndex"></param>
        void JumpToChapter(int chapterIndex);

        /// <summary>
        /// 获取对话配置
        /// </summary>
        /// <returns></returns>
        DialogueConfig GetDialogueConfig();

        /// <summary>
        /// 获取对话黑板
        /// </summary>
        /// <returns></returns>
        DialogueBlackboard GetBlackboard();
    }
}