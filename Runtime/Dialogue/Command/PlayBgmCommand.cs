﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 播放背景音乐命令
    /// </summary>
    public class PlayBgmCommand : IDialogueCommand
    {
        private readonly string bgmId;

        private readonly bool bgmLoop;

        public PlayBgmCommand(string bgmId, bool bgmLoop)
        {
            this.bgmId = bgmId;
            this.bgmLoop = bgmLoop;
        }

        public void Execute(IDialogueManager dialogueManager, System.Action<bool> executeCallback)
        {
            var bgmList = dialogueManager.GetBGMList();
            if (bgmList == null)
            {
                Debug.LogError("BGMList is null");
                return;
            }
            var audioClip = bgmList.GetBgmById(bgmId).BgmClip;
            if (audioClip == null)
            {
                Debug.LogError($"BGM {bgmId} 没有找到");
                return;
            }
            var audioManager = dialogueManager.GetAudioManager();
            audioManager.PlayBgm(audioClip, bgmLoop);
            executeCallback?.Invoke(true);
        }
    }
}
