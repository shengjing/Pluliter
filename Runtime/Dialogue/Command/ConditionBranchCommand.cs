//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Blackboard;
using System;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 条件分支指令
    /// </summary>
    public class ConditionBranchCommand : IDialogueCommand
    {
        private readonly CompositeCondition compositeCondition;

        public void Execute(IDialogueManager dialogueManager, Action<bool> executeCallback)
        {
            var blackboard = dialogueManager.GetBlackboard();
            var nextNode = compositeCondition.GetNextNode(blackboard);
            dialogueManager.ProcessDialogue(nextNode);
        }


        public ConditionBranchCommand(CompositeCondition compositeCondition)
        {
            this.compositeCondition = compositeCondition;
        }
    }
}