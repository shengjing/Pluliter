﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Actor;
using System;
using UnityEngine;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 显示演员命令
    /// </summary>
    public class ShowActorCommand : IDialogueCommand
    {
        private readonly string actorId;
        private readonly string stateId;
        private readonly float actorScale;
        private readonly Vector2 actorPosition;

        private readonly MoveInAnimation moveInAnimationType;
        private readonly MoveOutAnimation moveOutAnimationType;

        private readonly float duration;


        public void Execute(IDialogueManager dialogueManager, Action<bool> executeCallback)
        {
            var charaList = dialogueManager.GetCharacterList();
            if (charaList == null)
            {
                Debug.LogError("Character List is null");
                executeCallback.Invoke(false);
                return;
            }
            Character.Character character = dialogueManager.GetCharacterList().GetCharacterByName(actorId);
            ActorManager actorManager = dialogueManager.GetActorManager();
            Actor.DialogueActor actor = new()
            {
                ActorName = actorId.ToString()
            };
            var asp = dialogueManager.GetCharacterList().GetCharacterByName(actorId).
                GetCharacterState(stateId).StateSprite;
            if (asp == null)
            {
                Debug.Log("Actor Sprite is null");
                actor.ActorSprite = null;
            }
            else
            {
                actor.ActorSprite = asp;
            }
            actor.ActorPosition = actorPosition;
            actor.ActorScale = actorScale;
            actorManager.CreateActor(actor, moveInAnimationType, duration, () =>
            {
                executeCallback(true);
            });
            
            
        }

        public ShowActorCommand(string actorId, string stateID, float actorScale, Vector2 actorPosition, MoveInAnimation moveInAnimationType, MoveOutAnimation moveOutAnimationType, float MoveDuration)
        {
            this.actorId = actorId;
            this.stateId = stateID;
            this.actorScale = actorScale;
            this.actorPosition = actorPosition;
            this.moveInAnimationType = moveInAnimationType;
            this.moveOutAnimationType = moveOutAnimationType;
            this.duration = MoveDuration;
        }
    }
}