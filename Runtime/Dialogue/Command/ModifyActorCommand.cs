﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;
using UnityEngine;

namespace Pluliter.Dialogue
{
    public class ModifyActorCommand : IDialogueCommand
    {
        private readonly string actorId;
        private readonly string stateId;
        private readonly Vector2 moveTargetPositon;
        private readonly float moveDuration;

        private readonly bool modifyPositon;
        private readonly bool modifyState;

        public ModifyActorCommand(string actorId, string stateId, Vector2 moveTargetPositon, float moveDuration, bool modifyPositon, bool modifyState)
        {
            this.actorId = actorId;
            this.stateId = stateId;
            this.moveTargetPositon = moveTargetPositon;
            this.moveDuration = moveDuration;
            this.modifyPositon = modifyPositon;
            this.modifyState = modifyState;
        }

        public void Execute(IDialogueManager dialogueManager, Action<bool> executeCallback)
        {
            var actorManager = dialogueManager.GetActorManager();
            Character.Character character = dialogueManager.GetCharacterList().GetCharacterByName(actorId);
            var asp = dialogueManager.GetCharacterList().GetCharacterByName(actorId).GetCharacterState(stateId).StateSprite;
            if (asp == null)
            {
                Debug.Log("Actor Sprite is null");
            }
            if (modifyPositon)
            {
                actorManager.MoveActor(actorId, moveTargetPositon, moveDuration, () =>
                {
                    if (modifyState)
                    {
                        actorManager.ChangeActorState(actorId, asp);
                    }
                    executeCallback?.Invoke(true);
                });
            }
            else if (modifyState)
            {
                actorManager.ChangeActorState(actorId, asp);
                executeCallback?.Invoke(true);
            }
            else
            {
                executeCallback?.Invoke(true);
            }
        }
        
    }
}