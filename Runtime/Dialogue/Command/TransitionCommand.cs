﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Background;
using System;
using UnityEngine;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 背景切换命令
    /// </summary>
    public class TransitionCommand : IDialogueCommand
    {
        private readonly string bgid;
        private readonly TransitionAnimType transitionType;
        private readonly float bgDuration;

        public TransitionCommand(string bgid, TransitionAnimType transitionType, float bgDuration)
        {
            this.bgid = bgid;
            this.transitionType = transitionType;
            this.bgDuration = bgDuration;
        }

        public void Execute(IDialogueManager dialogueManager, Action<bool> executeCallback)
        {
            var backgroundManager = dialogueManager.GetBackgroundManager();
            var dialogueBox = dialogueManager.GetDialogueBoxManager();

            Action showCallback = () =>
            {
                executeCallback?.Invoke(false);
                //dialogueBox.ShowDialogueBox(bgDuration, () =>
                //{
                //    executeCallback?.Invoke(false);
                //});
            };

            dialogueBox.HideDialogueBox(bgDuration, () =>
            {
                var bgList = dialogueManager.GetBackgroundList();
                if (string.IsNullOrEmpty(bgid))
                {
                    Debug.LogError("背景ID为空");
                    return;
                }
                var bg = bgList.GetBackgroundByName(bgid).BgSprite;
                if (bg != null)
                {
                    backgroundManager.SetBackgroundSprite(bg, transitionType, bgDuration);
                }
                else
                {
                    Debug.LogError("背景没有找到或背景图片为空: " + bgid);
                }

                showCallback?.Invoke();
            });


            
        }
    }
}