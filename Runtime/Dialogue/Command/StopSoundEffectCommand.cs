﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


namespace Pluliter.Dialogue
{
    /// <summary>
    /// 停止播放音效命令
    /// </summary>
    public class StopSoundEffectCommand : IDialogueCommand
    {

        public StopSoundEffectCommand()
        {

        }

        public void Execute(IDialogueManager dialogueManager, System.Action<bool> executeCallback)
        {
            var audioManager = dialogueManager.GetAudioManager();
            if (audioManager == null)
            {
                throw new System.Exception("无法获取AudioManager");
            }
            audioManager.StopSfx();
            executeCallback?.Invoke(true);
        }
    }
}
