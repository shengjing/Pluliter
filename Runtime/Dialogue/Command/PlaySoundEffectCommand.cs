﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Audio;
using UnityEngine;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 播放音效命令
    /// </summary>
    public class PlaySoundEffectCommand : IDialogueCommand
    {
        private readonly string seId;


        public PlaySoundEffectCommand(string seId)
        {
            this.seId = seId;
        }

        public void Execute(IDialogueManager dialogueManager, System.Action<bool> executeCallback)
        {
            var audioClip = dialogueManager.GetSoundEffectList().GetSoundEffectById(seId).SoundEffectClip;
            if (audioClip == null)
            {
                Debug.LogError($"SoundEffectCommand 找不到 {seId} 没有找到");
            }
            var audioManager = dialogueManager.GetAudioManager();
            audioManager.PlaySfx(audioClip);
            executeCallback?.Invoke(true);
        }
    }
}
