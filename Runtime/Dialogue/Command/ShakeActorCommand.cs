﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Actor;
using System;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 晃动演员命令
    /// </summary>
    public class ShakeActorCommand : IDialogueCommand
    {
        private readonly string actorID;
        private readonly float duration;

        public void Execute(IDialogueManager dialogueManager, Action<bool> executeCallback)
        {
            ActorManager actorManager = dialogueManager.GetActorManager();
            actorManager.ShakeActor(actorID, duration, () =>
            {
                executeCallback(true);
            });
        }

        public ShakeActorCommand(string actorID, float duration)
        {
            this.actorID = actorID;
            this.duration = duration;
        }
    }
}