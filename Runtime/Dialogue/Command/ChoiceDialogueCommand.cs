﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pluliter.Dialogue
{
    /// <summary>
    /// 选择对话命令
    /// </summary>
    public class ChoiceDialogueCommand : IDialogueCommand
    {
        private readonly List<DialogueOption> dialogueOptions;

        public void Execute(IDialogueManager dialogueManager, Action<bool> executeCallback)
        {
            var dialogueBoxManager = dialogueManager.GetDialogueBoxManager();
            var options = dialogueBoxManager.AddOptions(dialogueOptions, dialogueManager, (nextNode) =>
            {
                executeCallback?.Invoke(false);
                dialogueManager.ProcessDialogue(nextNode);
            });
        }

        public ChoiceDialogueCommand(List<DialogueOption> dialogueOptions)
        {
            this.dialogueOptions = dialogueOptions;
        }
    }
}