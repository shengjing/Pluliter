﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;
using System.Collections.Generic;

namespace Pluliter.Blackboard
{
    /// <summary>
    /// 黑板数据
    /// </summary>
    [Serializable]
    public class BlackboardData
    {
        public Dictionary<string, bool> boolVariables = new Dictionary<string, bool>();
        public Dictionary<string, float> floatVariables = new Dictionary<string, float>();
        public Dictionary<string, int> intVariables = new Dictionary<string, int>();
    }
}
