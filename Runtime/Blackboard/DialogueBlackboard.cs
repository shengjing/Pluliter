﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pluliter.Blackboard
{
    /// <summary>
    /// 黑板
    /// </summary>
    public class DialogueBlackboard : MonoBehaviour
    {
        /// <summary>
        /// 黑板数据
        /// </summary>
        public BlackboardData data = new BlackboardData();

        /// <summary>
        /// 添加黑板数据
        /// </summary>
        /// <param name="data"></param>
        public void AddBlackboardData(BlackboardDataVariables data)
        {
            foreach (var item in data.boolVariables)
            {
                if (!this.data.boolVariables.ContainsKey(item.variableName))
                {
                    this.data.boolVariables.Add(item.variableName, item.value);
                }
                else
                {
                    // 更新
                    this.data.boolVariables[item.variableName] = item.value;
                }
            }

            foreach (var item in data.floatVariables)
            {
                if (!this.data.floatVariables.ContainsKey(item.variableName))
                {
                    this.data.floatVariables.Add(item.variableName, item.value);
                }
                else
                {
                    // 更新
                    this.data.floatVariables[item.variableName] = item.value;
                }
            }

            foreach (var item in data.intVariables)
            {
                if (!this.data.intVariables.ContainsKey(item.variableName))
                {
                    this.data.intVariables.Add(item.variableName, item.value);
                }
                else
                {
                    // 更新
                    this.data.intVariables[item.variableName] = item.value;
                }
            }

        }

        /// <summary>
        /// 设置布尔变量值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetBool(string key, bool value) => data.boolVariables[key] = value;

        /// <summary>
        /// 设置浮点数变量值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetFloat(string key, float value) => data.floatVariables[key] = value;

        /// <summary>
        /// 设置整数变量值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetInt(string key, int value) => data.intVariables[key] = value;

        /// <summary>
        /// 获取布尔变量值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public bool GetBool(string key, bool defaultValue = false)
        {
            return data.boolVariables.TryGetValue(key, out bool value) ? value : defaultValue;
        }
        /// <summary>
        /// 获取浮点数变量值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public float GetFloat(string key, float defaultValue = 0f)
        {
            return data.floatVariables.TryGetValue(key, out float value) ? value : defaultValue;
        }

        /// <summary>
        /// 获取整数变量值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public int GetInt(string key, int defaultValue = 0)
        {
            return data.intVariables.TryGetValue(key, out int value) ? value : defaultValue;
        }

        /// <summary>
        /// 获取快照
        /// </summary>
        /// <returns></returns>
        public BlackboardSnapshot GetBlackboardSnapshot()
        {
            var snapshot = new BlackboardSnapshot();
            snapshot.SnapshotData = data;
            return snapshot;
        }

        /// <summary>
        /// 恢复快照
        /// </summary>
        /// <param name="blackboardSnapshot"></param>
        public void RevertToBlackboardSnapshot(BlackboardSnapshot blackboardSnapshot)
        {
            data = blackboardSnapshot.SnapshotData;
        }
    }

    /// <summary>
    /// 比较类型
    /// </summary>
    public enum CompareType
    {
        [InspectorName("等于")]
        Equal,
        [InspectorName("不等于")]
        NotEqual,
        [InspectorName("大于")]
        Greater,
        [InspectorName("小于")]
        Less,
        [InspectorName("大于等于")]
        GreaterOrEqual,
        [InspectorName("小于等于")]
        LessOrEqual
    }

    /// <summary>
    /// 条件检查器接口
    /// </summary>
    public interface IConditionChecker
    {
        bool CheckCondition(DialogueBlackboard blackboard);
    }

    /// <summary>
    /// 布尔条件检查
    /// </summary>
    [Serializable]
    public class BoolCondition : IConditionChecker
    {
        public string variableName;
        public bool expectedValue;

        public bool CheckCondition(DialogueBlackboard blackboard)
        {
            return blackboard.GetBool(variableName) == expectedValue;
        }
    }

    /// <summary>
    /// 浮点数条件检查
    /// </summary>
    [Serializable]
    public class FloatCondition : IConditionChecker
    {
        public string variableName;
        public CompareType compareType;
        public float compareValue;

        public bool CheckCondition(DialogueBlackboard blackboard)
        {
            float value = blackboard.GetFloat(variableName);

            return compareType switch
            {
                CompareType.Equal => Mathf.Approximately(value, compareValue),
                CompareType.NotEqual => !Mathf.Approximately(value, compareValue),
                CompareType.Greater => value > compareValue,
                CompareType.Less => value < compareValue,
                CompareType.GreaterOrEqual => value >= compareValue,
                CompareType.LessOrEqual => value <= compareValue,
                _ => false
            };
        }
    }


    /// <summary>
    /// 整数条件检查
    /// </summary>
    [Serializable]
    public class IntegerCondition : IConditionChecker
    {
        public string variableName;
        public CompareType compareType;
        public int compareValue;

        public bool CheckCondition(DialogueBlackboard blackboard)
        {
            float value = blackboard.GetInt(variableName);

            return compareType switch
            {
                CompareType.Equal => Mathf.Approximately(value, compareValue),
                CompareType.NotEqual => !Mathf.Approximately(value, compareValue),
                CompareType.Greater => value > compareValue,
                CompareType.Less => value < compareValue,
                CompareType.GreaterOrEqual => value >= compareValue,
                CompareType.LessOrEqual => value <= compareValue,
                _ => false
            };
        }
    }
}