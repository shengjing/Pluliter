﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Pluliter.Dialogue;
using System.Collections.Generic;
using System;

namespace Pluliter.Blackboard
{
    /// <summary>
    /// 复合条件
    /// </summary>
    [Serializable]
    public class CompositeCondition
    {
        /// <summary>
        /// 布尔条件组
        /// </summary>
        public List<BoolCondition> boolConditions = new List<BoolCondition>();

        /// <summary>
        /// 浮点数条件组
        /// </summary>
        public List<FloatCondition> floatConditions = new List<FloatCondition>();

        /// <summary>
        /// 整数条件组
        /// </summary>
        public List<IntegerCondition> intConditions = new List<IntegerCondition>();

        /// <summary>
        /// 真条件节点，当所有当所有条件都满足时跳转
        /// </summary>
        public DialogueNode TrueNextNode = null;
        
        /// <summary>
        /// 假条件节点，当有条件没有满足时跳转
        /// </summary>
        public DialogueNode FalseNextNode = null;

        /// <summary>
        /// 检查所有条件
        /// </summary>
        /// <param name="blackboard"></param>
        /// <returns></returns>
        public bool CheckAllConditions(DialogueBlackboard blackboard)
        {
            foreach (var condition in boolConditions)
            {
                if (!condition.CheckCondition(blackboard)) return false;
            }

            foreach (var condition in floatConditions)
            {
                if (!condition.CheckCondition(blackboard)) return false;
            }

            foreach (var condition in intConditions)
            {
                if (!condition.CheckCondition(blackboard)) return false;
            }
            return true;
        }

        /// <summary>
        /// 获取下一节点
        /// </summary>
        /// <param name="blackboard"></param>
        /// <returns></returns>
        public DialogueNode GetNextNode(DialogueBlackboard blackboard)
        {
            return CheckAllConditions(blackboard) ? TrueNextNode : FalseNextNode;
        }
    }
}