﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;
using Pluliter.Animation;
using Pluliter.Dialogue;


namespace Pluliter.Editor.Config
{
    /// <summary>
    /// 多文Pluliter配置文件，用于存储多文项目的配置信息
    /// </summary>
    public class PluliterConfig : ScriptableObject
    {
        /// <summary>
        /// 启用错误报告
        /// </summary>
        public bool EnableErrorReport = true;

        #region 演出系统配置
        /// <summary>
        /// 演出动画引擎
        /// </summary>
        [Header("演出动画引擎")]
        [SerializeField]
        public AnimationEngine animationEngine = AnimationEngine.DOTween;

        /// <summary>
        /// 对话打字机效果
        /// </summary>
        [Header("对话打字机效果")]
        [SerializeField]
        public TypewriterEffect typewriterEffect = TypewriterEffect.Normal;

        /// <summary>
        /// 淡入字数，用于打字机淡入效果
        /// </summary>
        [Header("淡入字数")]
        [Range(0.5f, 10f)]
        [SerializeField]
        public byte TypewriterFadeRange = 3;
        #endregion

    }
}
