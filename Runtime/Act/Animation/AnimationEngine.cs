﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;

namespace Pluliter.Animation
{
    /// <summary>
    /// 动画引擎，用于实现动画效果
    /// </summary>
    public enum AnimationEngine
    {
        /// <summary>
        /// DOTween是由DEMIGIANT开发的一个快速、高效、完全类型安全的动画引擎
        /// </summary>
        [InspectorName("DOTween")]
        DOTween,
        /// <summary>
        /// LWTween是Puliter内置的轻量级动画引擎，用于实现简单的动画效果
        /// </summary>
        [InspectorName("LWTween（实验性）")]
        LWTween,
        /// <summary>
        /// 禁用的动画引擎，不执行任何动画
        /// </summary>
        [InspectorName("禁用")]
        None
    }
}