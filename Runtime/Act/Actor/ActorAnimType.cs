﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.



using UnityEngine;

namespace Pluliter.Actor
{
    /// <summary>
    /// 入场演员动画，可自行扩展
    /// </summary>
    public enum MoveInAnimation
    {
        /// <summary>
        /// 无
        /// </summary>
        [InspectorName("无动画")]
        None,
        /// <summary>
        /// 淡入
        /// </summary>
        [InspectorName("淡入")]
        FadeIn,
        /// <summary>
        /// 从下往上滑入
        /// </summary>
        [InspectorName("从下滑入")]
        UnderSlideIn,
        /// <summary>
        /// 从左往右滑入
        /// </summary>
        [InspectorName("从左滑入")]
        LeftSlideIn,
        /// <summary>
        /// 从右往左滑入
        /// </summary>
        [InspectorName("从右滑入")]
        RightSlideIn
    }

    /// <summary>
    /// 出场演员动画，可自行扩展
    /// </summary>
    public enum MoveOutAnimation
    {
        /// <summary>
        /// 无
        /// </summary>
        [InspectorName("无动画")]
        None,
        /// <summary>
        /// 淡出
        /// </summary>
        [InspectorName("淡出")]
        FadeOut,
        /// <summary>
        /// 从下往上滑出
        /// </summary>
        [InspectorName("从下滑出")]
        UnderSlideOut,
        /// <summary>
        /// 从左滑出
        /// </summary>
        [InspectorName("从左滑出")]
        LeftSlideOut,
        /// <summary>
        /// 从右滑出
        /// </summary>
        [InspectorName("从右滑出")]
        RightSlideOut
    }




    /// <summary>
    /// 移动方向
    /// </summary>
    public enum MoveDirection
    {
        /// <summary>
        /// 右
        /// </summary>
        Right,
        /// <summary>
        /// 左
        /// </summary>
        Left
    }
}
