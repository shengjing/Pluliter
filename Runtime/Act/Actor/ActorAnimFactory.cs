﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using System;
using UnityEngine;

namespace Pluliter.Actor
{
    /// <summary>
    /// 演员动画工厂
    /// </summary>
    public class ActorAnimFactory
    {
        /// <summary>
        /// 创建演员入场动画
        /// </summary>
        /// <param name="actor"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.NotImplementedException"></exception>
        public static BaseActorAnimation CreateMoveInAnim(GameObject actor, MoveInAnimation moveInAnimationType, float animationDuration)
        {
            if (actor == null)
            {
                throw new System.ArgumentNullException(nameof(actor));
            }
            return moveInAnimationType switch
            {
                MoveInAnimation.None => new NoneAnim(actor, animationDuration),
                MoveInAnimation.FadeIn => new FadeInAnim(actor, animationDuration),
                MoveInAnimation.UnderSlideIn => new UnderSlideInAnim(actor, animationDuration),
                MoveInAnimation.RightSlideIn => new RightSlideInAnim(actor, animationDuration),
                MoveInAnimation.LeftSlideIn => new LeftSlideInAnim(actor, animationDuration),
                _ => throw new System.NotImplementedException(),
            };
        }

        /// <summary>
        /// 创建演员出场动画
        /// </summary>
        /// <param name="actor"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.NotImplementedException"></exception>
        public static BaseActorAnimation CreateMoveOutAnim(GameObject actor, MoveOutAnimation moveOutAnimationType, float animationDuration)
        {
            if (actor == null)
            {
                throw new System.ArgumentNullException(nameof(actor));
            }
            return moveOutAnimationType switch
            {
                MoveOutAnimation.None => new NoneAnim(actor, animationDuration),
                MoveOutAnimation.FadeOut => new FadeOutAnim(actor, animationDuration),
                MoveOutAnimation.UnderSlideOut => new UnderSlideOutAnim(actor, animationDuration),
                MoveOutAnimation.RightSlideOut => new RightSlideOutAnim(actor, animationDuration),
                MoveOutAnimation.LeftSlideOut => new LeftSlideOutAnim(actor, animationDuration),
                _ => throw new System.NotImplementedException(),
            };
        }

        public static BaseActorAnimation CreateShakeAnim(GameObject actor, float duration)
        {
            if (actor == null)
            {
                throw new System.ArgumentNullException(nameof(actor));
            }
            return new ShakeAnim(actor, duration);

        }
    }
}
