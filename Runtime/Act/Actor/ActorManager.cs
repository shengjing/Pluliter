﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Pluliter.Animation;
using Pluliter.Editor.Config;
using DG.Tweening;
using Pluliter.Dialogue;


namespace Pluliter.Actor
{
    /// <summary>
    /// 演员管理器
    /// </summary>
    public class ActorManager : MonoBehaviour, IPauseable
    {
        private PluliterConfig pluliterConfig;
        private AnimationEngine animationEngine;
        private GameObject ActorLayer;

        /// <summary>
        /// 演员
        /// </summary>
        public Dictionary<string, GameObject> ActorDic = new();

        private void Awake()
        {
            pluliterConfig = Resources.Load<PluliterConfig>("PluliterConfig");
            
            if (pluliterConfig != null)
            {
                animationEngine = pluliterConfig.animationEngine;
            }
            else
            {
                Debug.LogError("未找到PluliterConfig.asset");
            }

            ActorLayer = gameObject;
        }


        /// <summary>
        /// 创建演员
        /// </summary>
        /// <param name="actor"></param>
        /// <param name="callback"></param>
        public void CreateActor(DialogueActor actor, MoveInAnimation moveInAnimationType, float animationDuration, Action callback)
        {
            if (!ActorDic.ContainsKey(actor.ActorName))
            {
                GameObject actorObj = new(actor.ActorName);
                actorObj.transform.SetParent(ActorLayer.transform);
                
                actorObj.transform.localPosition = new Vector3(actor.ActorPosition.x, actor.ActorPosition.y, 0f);
                actorObj.transform.localScale = new Vector3(actor.ActorScale, actor.ActorScale, 1);
                SpriteRenderer spriteRenderer = actorObj.AddComponent<SpriteRenderer>();
                spriteRenderer.sprite = actor.ActorSprite;

                // 先设置透明
                spriteRenderer.color = new Color(1, 1, 1, 0);

                ActorDic.Add(actor.ActorName, actorObj);

                // 播放入场动画
                var actorAnim = ActorAnimFactory.CreateMoveInAnim(actorObj, moveInAnimationType, animationDuration);
                actorAnim.Play(actorObj, callback, animationEngine);
                
            }
            else
            {
                Debug.LogError("已经存在演员:" + actor.ActorName);
            }
        }

        /// <summary>
        /// 创建演员
        /// </summary>
        /// <param name="actor"></param>
        public void CreateActor(DialogueActor actor)
        {
            if (ActorDic.ContainsKey(actor.ActorName))
            {
                Debug.LogError("已经存在演员:" + actor.ActorName);
                return;
            }

            GameObject actorObj = new(actor.ActorName);
            actorObj.transform.SetParent(ActorLayer.transform);

            actorObj.transform.localPosition = new Vector3(actor.ActorPosition.x, actor.ActorPosition.y, 0f);
            actorObj.transform.localScale = new Vector3(actor.ActorScale, actor.ActorScale, 1);
            SpriteRenderer spriteRenderer = actorObj.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = actor.ActorSprite;

            ActorDic.Add(actor.ActorName, actorObj);

        }


        /// <summary>
        /// 移除演员
        /// </summary>
        /// <param name="actorName"></param>
        /// <param name="moveOutAnimationType"></param>
        /// <param name="duration"></param>
        public void RemoveActor(string actorName, MoveOutAnimation moveOutAnimationType, float animationDuration)
        {
            if (ActorDic.ContainsKey(actorName))
            {
                GameObject actorObj = ActorDic[actorName];
                // 退场动画
                var actorAnim = ActorAnimFactory.CreateMoveOutAnim(actorObj, moveOutAnimationType, animationDuration);

                actorAnim.Play(actorObj, () =>
                {
                    Destroy(ActorDic[actorName]);
                    ActorDic.Remove(actorName);
                }, animationEngine);

            }
            else
            {
                Debug.LogError("没有找到演员:" + actorName);
            }
        }

        /// <summary>
        /// 移除演员
        /// </summary>
        /// <param name="actorName"></param>
        public void RemoveActor(string actorName)
        {
            if (!ActorDic.ContainsKey(actorName))
            {
                Debug.LogError("没有找到演员:" + actorName);
                return;
            }
            Destroy(ActorDic[actorName]);
            ActorDic.Remove(actorName);
        }

        /// <summary>
        /// 移除所有演员
        /// </summary>
        public void RemoveAllActor()
        {
            ActorDic.Keys.ToList().ForEach((string actorName) =>
            {
                RemoveActor(actorName);
            });
        }


        /// <summary>
        /// 晃动演员
        /// </summary>
        /// <param name="actorName"></param>
        /// <param name="duration"></param>
        /// <param name="rate"></param>
        public void ShakeActor(string actorName, float duration, Action callback)
        {
            if (ActorDic.ContainsKey(actorName))
            {
                var actorObj = ActorDic[actorName];
                var actorAnim = ActorAnimFactory.CreateShakeAnim(actorObj, duration);
                actorAnim.Play(actorObj, callback, animationEngine);
            }
            else
            {
                Debug.LogError("没有找到演员:" + actorName);
            }

        }


        /// <summary>
        /// 移动演员
        /// </summary>
        /// <param name="actorName"></param>
        /// <param name="moveDirection"></param>
        /// <param name="distance"></param>
        /// <param name="duration"></param>
        public void MoveActor(string actorName, Vector2 moveTargetPosition, float duration, Action callback)
        {
            if (ActorDic.ContainsKey(actorName))
            {
                GameObject actorObj = ActorDic[actorName];
                Vector3 originalPosition = actorObj.transform.localPosition;

                Vector3 targetPosition = new Vector3(moveTargetPosition.x, moveTargetPosition.y, originalPosition.z);

                actorObj.transform.DOLocalMove(targetPosition, duration).OnKill(() =>
                {
                    originalPosition = moveTargetPosition;
                    actorObj.transform.localPosition = originalPosition;
                    callback();
                });
            }
            else
            {
                Debug.LogError("没有找到演员:" + actorName);
            }
        }

        public void ChangeActorState(string actorName, Sprite actorState)
        {
            if (ActorDic.ContainsKey(actorName))
            {
                ActorDic[actorName].GetComponent<SpriteRenderer>().sprite = actorState;
            }
        }


        /// <summary>
        /// 恢复到演员快照
        /// </summary>
        /// <param name="actorLayerSnapshot"></param>
        public void RevertToActorLayerSnapshot(ActorLayerSnapshot actorLayerSnapshot, IDialogueManager dialogueManager)
        {
            if (actorLayerSnapshot == null)
            {
                Debug.LogError("actorLayerSnapshot为空");
                return;
            }

            RemoveAllActor();

            actorLayerSnapshot.actorSnapshots.ToList().ForEach((KeyValuePair<string, ActorSnapshot> actorSnapshot) =>
            {
                DialogueActor actor = new()
                {
                    ActorScale = actorSnapshot.Value.ActorScale,
                    ActorPosition = actorSnapshot.Value.ActorPosition,
                    ActorName = actorSnapshot.Value.ActorObjectName,
                };

                var asp = dialogueManager.GetCharacterList().GetCharacterByName(actorSnapshot.Value.ActorObjectName).CharacterStates.Find(x => x.StateSprite.name == actorSnapshot.Value.SpriteName);
                actor.ActorSprite = asp.StateSprite;

                CreateActor(actor);
            });
        }

        /// <summary>
        /// 获取演员层快照
        /// </summary>
        /// <returns></returns>
        public ActorLayerSnapshot GetActorLayerSnapshot()
        {
            var actorLayerSnapshot = new ActorLayerSnapshot();

            ActorDic.ToList().ForEach((KeyValuePair<string, GameObject> actor) =>
            {
                if (actor.Value == null)
                {
                    throw new ArgumentNullException(nameof(actor.Value));
                }
                var actorSnapshot = new ActorSnapshot();

                actorSnapshot.ActorObjectName = actor.Value.name;
                actorSnapshot.ActorPosition = actor.Value.transform.localPosition;
                actorSnapshot.ActorScale = actor.Value.transform.localScale.x;
                actorSnapshot.SpriteName = actor.Value.GetComponent<SpriteRenderer>().sprite.name;

                actorLayerSnapshot.actorSnapshots.Add(actor.Key, actorSnapshot);
            });
            return actorLayerSnapshot;
        }

        public void Pause()
        {
            
        }

        public void Resume()
        {
            
        }

        
    }

}
