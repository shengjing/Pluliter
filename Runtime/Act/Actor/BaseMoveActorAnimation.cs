//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.



using Pluliter.Animation;
using UnityEngine;


namespace Pluliter.Actor
{
    /// <summary>
    /// 角色动画基类，所有角色动画必须继承此类
    /// </summary>
    public class BaseMoveActorAnimation
    {
        /// <summary>
        /// 动画播放时间
        /// </summary>
        private protected float animationDuration = 0.0f;

        /// <summary>
        /// 动画完成回调函数
        /// </summary>
        private protected System.Action onAnimationFinish = null;

        /// <summary>
        /// 播放动画
        /// </summary>
        /// <param name="actorObj">演员对象的GameObject</param>
        public virtual void Play(GameObject actorObj, System.Action onAnimationFinish, AnimationEngine animationEngine)
        {
            this.onAnimationFinish = onAnimationFinish;
        }

        /// <summary>
        /// 暂停
        /// </summary>
        public virtual void Pause()
        {
            Debug.LogWarning("BaseActorAnimation.Pause是空方法，请重写");
        }

        public BaseMoveActorAnimation(GameObject actorObj, float animationDuration)
        {
            this.animationDuration = animationDuration;
        }
    }
}
