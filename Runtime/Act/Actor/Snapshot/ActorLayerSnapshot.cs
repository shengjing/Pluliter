﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Pluliter.Actor
{
    /// <summary>
    /// 演员层快照，用于保存演员层状态
    /// </summary>
    [Serializable]
    public class ActorLayerSnapshot
    {

        /// <summary>
        /// 演员快照
        /// </summary>

        public Dictionary<string, ActorSnapshot> actorSnapshots = new();
    }
}
