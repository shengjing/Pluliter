﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 演员快照
/// </summary>
[Serializable]
public class ActorSnapshot
{
    public string ActorObjectName = null;

    public Vector2 ActorPosition;

    public float ActorScale;

    public string SpriteName;
    

    //public ActorSnapshot(GameObject actorObj)
    //{
    //    if (actorObj == null)
    //    {
    //        Debug.Log("actorObj为空");
    //    }
    //    if (actorObj.GetComponent<SpriteRenderer>() == null)
    //    {
    //        Debug.Log("actorObj没有SpriteRenderer组件");
    //    }

    //    ActorObjectName = actorObj.name;

    //    var localPos = actorObj.transform.localPosition;
    //    ActorPosition = (Vector2)localPos;
    //    ActorScale = (int)actorObj.transform.localScale.x;

    //    SpriteName = actorObj.GetComponent<SpriteRenderer>().sprite.name;
    //}
}
