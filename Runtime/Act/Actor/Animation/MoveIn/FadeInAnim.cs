﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine;
using Pluliter.Animation;
using Pluliter.LWTween;
#if DOTWEEN
using DG.Tweening;
#endif


namespace Pluliter.Actor
{
    public class FadeInAnim : BaseActorAnimation
    {
        public FadeInAnim(GameObject actorObj, float animationDuration) : base(actorObj, animationDuration)
        {
            
        }

        public override void Play(GameObject actorObj, System.Action onAnimationFinish, AnimationEngine animationEngine)
        {

            base.onAnimationFinish = onAnimationFinish;
            if (actorObj == null)
            {
                Debug.LogError("没有找到演员对象");
            }
            else
            {
                var actorSprite = actorObj.GetComponent<SpriteRenderer>();
                actorSprite.color = new Color(1, 1, 1, 0);

                if (animationEngine == AnimationEngine.None)
                {
                    Color color = actorObj.GetComponent<SpriteRenderer>().color;
                    color.a = 1;
                    actorSprite.color = color;
                }
                if (animationEngine == AnimationEngine.DOTween)
                {
# if DOTWEEN
                    actorObj.GetComponent<SpriteRenderer>().DOFade(1, animationDuration).OnKill(() =>
                    {
                        Color color = actorObj.GetComponent<SpriteRenderer>().color;
                        color.a = 1;
                        actorSprite.color = color;
                        onAnimationFinish?.Invoke();
                    });
#endif
#if !DOTWEEN
                    Debug.LogError("没有安装DOTween，请安装或者在项目设置中选择其他动画引擎");
#endif
                }
                else if (animationEngine == AnimationEngine.LWTween)
                {
                    actorObj.GetComponent<SpriteRenderer>().FadeColor(1f, animationDuration).SetOnKill(() =>
                    {
                        Color color = actorObj.GetComponent<SpriteRenderer>().color;
                        color.a = 1;
                        actorSprite.color = color;
                        onAnimationFinish?.Invoke();
                    });
                }
            }
        } 
    }
}
