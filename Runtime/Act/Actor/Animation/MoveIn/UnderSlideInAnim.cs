﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using UnityEngine;
using Pluliter.Animation;
using Pluliter.LWTween;
#if DOTWEEN
using DG.Tweening;
#endif

namespace Pluliter.Actor
{
    /// <summary>
    /// 角色从下侧滑入动画
    /// </summary>
    public class UnderSlideInAnim : BaseActorAnimation
    {

        public UnderSlideInAnim(GameObject actorObj, float animationDuration) : base(actorObj, animationDuration)
        {
        }

        public override void Play(GameObject actorObj, System.Action onAnimationFinish, AnimationEngine animationEngine)
        {
            base.onAnimationFinish = onAnimationFinish;
            if (animationEngine == AnimationEngine.LWTween)
            {
                actorObj.transform.localPosition = new Vector3(actorObj.transform.localPosition.x, actorObj.transform.localPosition.y - 0.5f, actorObj.transform.localPosition.z);
                actorObj.GetComponent<SpriteRenderer>().FadeColor(1, animationDuration);
                var targetPos = new Vector3(actorObj.transform.position.x, actorObj.transform.position.y + 0.5f, actorObj.transform.position.z);
                actorObj.transform.LocalMoveTo(targetPos, animationDuration).SetOnKill(() =>
                {
                    actorObj.transform.localPosition = targetPos;
                    onAnimationFinish?.Invoke();
                });
            }
            else if (animationEngine == AnimationEngine.DOTween)
            {
#if DOTWEEN
                actorObj.transform.localPosition = new Vector3(actorObj.transform.localPosition.x, actorObj.transform.localPosition.y - 0.5f, actorObj.transform.localPosition.z);
                actorObj.GetComponent<SpriteRenderer>().DOFade(1, animationDuration);
                actorObj.transform.DOLocalMoveY(actorObj.transform.position.y + 0.5f, animationDuration).OnComplete(() => { onAnimationFinish?.Invoke(); });
#endif
            }
            else if (animationEngine == AnimationEngine.None)
            {
                onAnimationFinish?.Invoke();
            }
        }   
    }
}
