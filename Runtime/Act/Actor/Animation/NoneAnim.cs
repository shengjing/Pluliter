﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Animation;
using UnityEngine;

namespace Pluliter.Actor
{
    public class NoneAnim : BaseActorAnimation
    {
        public NoneAnim(GameObject actorObj, float animationDuration) : base(actorObj, animationDuration)
        {

        }

        public override void Play(GameObject actorObj, System.Action onAnimationFinish, AnimationEngine animationEngine)
        {
            base.onAnimationFinish = onAnimationFinish;

            actorObj.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);

            base.onAnimationFinish?.Invoke();
        }
    }
}
