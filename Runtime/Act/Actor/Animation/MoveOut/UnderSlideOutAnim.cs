﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Animation;
using System;
using UnityEngine;
using Pluliter.LWTween;
#if DOTWEEN
using DG.Tweening;
#endif


namespace Pluliter.Actor
{
    public class UnderSlideOutAnim : BaseActorAnimation
    {
        public UnderSlideOutAnim(GameObject actorObj, float animationDuration) : base(actorObj, animationDuration)
        {
        }

        public override void Pause()
        {
            base.Pause();
        }

        public override void Play(GameObject actorObj, Action onAnimationFinish, AnimationEngine animationEngine)
        {
            base.onAnimationFinish = onAnimationFinish;
            if (animationEngine == AnimationEngine.LWTween)
            {
                actorObj.GetComponent<SpriteRenderer>().FadeColor(0, animationDuration);
                var targetPos = new Vector3(actorObj.transform.position.x, actorObj.transform.position.y - 0.5f, actorObj.transform.position.z);
                actorObj.transform.LocalMoveTo(targetPos, animationDuration).SetOnKill(() =>
                {
                    actorObj.transform.localPosition = targetPos;
                    onAnimationFinish?.Invoke();
                });
            }
            else if (animationEngine == AnimationEngine.DOTween)
            {
#if DOTWEEN
                actorObj.GetComponent<SpriteRenderer>().DOFade(0, animationDuration);
                actorObj.transform.DOLocalMoveY(actorObj.transform.position.y - 0.5f, animationDuration).OnComplete(() => { onAnimationFinish?.Invoke(); });
#endif
            }
            else if (animationEngine == AnimationEngine.None)
            {
                onAnimationFinish?.Invoke();
            }
        }
    }
}
