﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.



using Pluliter.Animation;
using Pluliter.LWTween;
using System;
using UnityEngine;
#if DOTWEEN
using DG.Tweening;
#endif

namespace Pluliter.Actor
{
    /// <summary>
    /// 渐隐退场动画
    /// </summary>
    public class FadeOutAnim : BaseActorAnimation
    {
        public FadeOutAnim(GameObject actorObj, float animationDuration) : base(actorObj, animationDuration)
        {
            this.animationDuration = animationDuration;
        }

        public override void Pause()
        {
            
        }

        public override void Play(GameObject actorObj, Action onAnimationFinish, AnimationEngine animationEngine)
        {
            this.onAnimationFinish = onAnimationFinish;
            var actorSprite = actorObj.GetComponent<SpriteRenderer>();
            if (animationEngine == AnimationEngine.LWTween)
            {
                if (actorSprite != null)
                {
                    actorSprite.FadeColor(0, animationDuration).SetOnKill(() =>
                    {
                        Color color = actorSprite.color;
                        color.a = 1;
                        actorSprite.color = color;
                        onAnimationFinish?.Invoke();
                    });
                }
                else
                {
                    throw new ArgumentNullException(nameof(actorSprite));
                }
            }
            else if (animationEngine == AnimationEngine.DOTween)
            {
#if DOTWEEN
                if (actorSprite != null)
                {
                    actorSprite.DOFade(0, animationDuration).OnKill(() =>
                    {
                        Color color = actorSprite.color;
                        color.a = 1;
                        actorSprite.color = color;
                        onAnimationFinish?.Invoke();
                    });
                }
                else
                {
                    throw new ArgumentNullException(nameof(actorSprite));
                }
#endif
#if !DOTWEEN
                Debug.LogError("没有安装DOTween，请安装或者在项目设置中选择其他动画引擎");
#endif
            }
            else if (animationEngine == AnimationEngine.None)
            {
                Color color = actorSprite.color;
                color.a = 1;
                actorSprite.color = color;
                onAnimationFinish?.Invoke();
            }
        }
    }
}