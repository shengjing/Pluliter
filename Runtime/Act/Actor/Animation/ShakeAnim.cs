﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using Pluliter.Animation;
using Pluliter.LWTween;
using System;
using UnityEngine;
#if DOTWEEN
using DG.Tweening;
#endif

namespace Pluliter.Actor
{
    /// <summary>
    /// 上下晃动动画
    /// </summary>
    public class ShakeAnim : BaseActorAnimation
    {
        public ShakeAnim(GameObject actorObj, float animationDuration) : base(actorObj, animationDuration)
        {
            this.animationDuration = animationDuration;
        }

        public override void Pause()
        {

        }

        public override void Play(GameObject actorObj, Action onAnimationFinish, AnimationEngine animationEngine)
        {
            this.onAnimationFinish = onAnimationFinish;
            if (animationEngine == AnimationEngine.LWTween)
            {
                Debug.LogError("LWTween暂未实现，请安装或者在项目设置中选择其他动画引擎");
            }
            else if (animationEngine == AnimationEngine.DOTween)
            {
#if DOTWEEN
                if (actorObj != null)
                {
                    Vector2 startPosition = actorObj.transform.localPosition;
                    Vector2 targetUpPosition = startPosition;
                    targetUpPosition.y += 0.5f;
                    actorObj.transform.DOLocalMoveY(targetUpPosition.y, animationDuration / 2).SetEase(Ease.Linear).OnKill(
                        () =>
                        {
                            actorObj.transform.DOLocalMoveY(startPosition.y, animationDuration / 2).SetEase(Ease.Linear).OnKill(() =>
                            {
                                actorObj.transform.localPosition = startPosition;
                                Debug.Log("演员:" + actorObj.name + "演员晃动");
                                if (onAnimationFinish != null)
                                {
                                    onAnimationFinish();
                                }
                            });
                        });
                }
                else
                {
                    throw new ArgumentNullException(nameof(actorObj));
                }
#endif
#if !DOTWEEN
                Debug.LogError("没有安装DOTween，请安装或者在项目设置中选择其他动画引擎");
#endif
            }
            else if (animationEngine == AnimationEngine.None)
            {
                onAnimationFinish?.Invoke();
            }
        }
    }
}