﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if LINGQIU
using LingqiuInspector.Tools;
#endif

namespace Pluliter.Character
{
    /// <summary>
    /// 角色
    /// </summary>
    [CreateAssetMenu(fileName = "新角色", menuName = "多文Pluliter/角色", order = 1)]
    public class Character : ScriptableObject
    {
        /// <summary>
        /// 角色姓名
        /// </summary>
#if LINGQIU
        [InspectorLabelText("角色姓名", "CharacterName")]
#endif
        public string CharacterName;

        /// <summary>
        /// 角色描述
        /// </summary>
#if LINGQIU
        [InspectorLabelText("角色描述", "CharacterDescription")]
#endif
        public string CharacterDescription;

        /// <summary>
        /// 角色状态
        /// </summary>
        public List<CharacterState> CharacterStates;

        /// <summary>
        /// 角色头像
        /// </summary>
        public List<CharacterAvatar> CharacterAvatars;

        /// <summary>
        /// 获取角色状态
        /// </summary>
        /// <param name="stateName">状态名称</param>
        /// <returns></returns>
        public CharacterState GetCharacterState(string stateName)
        {
            var state = CharacterStates.Find(x => x.StateName == stateName);
            if (state == null)
            {
                Debug.LogError("没有找到角色状态 " + stateName);
            }
            return state;
        }

        /// <summary>
        /// 获取角色状态名称列表
        /// </summary>
        /// <returns></returns>
        public List<string> GetCharacterStateNames()
        {
            var names = new List<string>();
            foreach (var state in CharacterStates) 
            {
                names.Add(state.StateName);
            }
            return names;
        }
    }

}
