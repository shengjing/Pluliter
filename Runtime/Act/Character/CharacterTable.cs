﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System.Collections.Generic;
using UnityEngine;


namespace Pluliter.Character
{
    /// <summary>
    /// 角色表
    /// </summary>
    [CreateAssetMenu(fileName = "角色列表", menuName = "多文Pluliter/角色表", order = 1)]
    public class CharacterTable : ScriptableObject
    {
        /// <summary>
        /// 角色列表
        /// </summary>
        public List<Character> characters;

        /// <summary>
        /// 获取角色名称列表
        /// </summary>
        /// <returns></returns>
        public List<string> GetCharacterNames()
        {
            var names = new List<string>();
            foreach (var character in characters)
            {
                names.Add(character.CharacterName);
            }
            return names;
        }

        /// <summary>
        /// 根据名称获取指定角色
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Character GetCharacterByName(string name)
        {
            if (characters == null)
            {
                Debug.LogError("Character list is null");
                return null;
            }
            if (characters.Count == 0)
            {
                Debug.LogError("Character list is empty");
                return null;
            }
            var targetChara = characters.Find(x => x.CharacterName == name);
            if (targetChara == null)
            {
                Debug.LogError("Character not found: " + name);
#if UNITY_EDITOR
                Debug.Break();
#endif
            }
            return targetChara;
        }
    }
}
