﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using System;
using UnityEngine;
using Pluliter.Dialogue;

#if DOTWEEN
using DG.Tweening;
#endif

namespace Pluliter.Background
{
    /// <summary>
    /// 背景管理器
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public class BackgroundManager : MonoBehaviour
    {
        private SpriteRenderer bgRender;

        /// <summary>
        /// 当前背景名称
        /// </summary>
        private string currentBackgroundName;

        private void Awake()
        {
            bgRender = gameObject.GetComponent<SpriteRenderer>();
            if (bgRender == null)
            {
                Debug.LogError("背景管理器需要一个SpriteRenderer组件");
                return;
            }

            bgRender.enabled = true;
            bgRender.color = Color.white;
            bgRender.sprite = null;
        }

        public void SetBackgroundSprite(Sprite sprite, TransitionAnimType transitionType, float transitionTime)
        {
            switch (transitionType)
            {
                case TransitionAnimType.None:
                    bgRender.sprite = sprite;
                    break;
                case TransitionAnimType.Fade:
#if DOTWEEN
                    bgRender.DOFade(0, transitionTime / 2).OnKill(() =>
                    {
                        bgRender.sprite = sprite;
                        bgRender.DOFade(1, transitionTime / 2);
                        Debug.Log("背景过渡动画：渐变");
                    });
#endif
                    break;
                default:
                    Debug.LogWarning("未知的背景过渡动画类型");
                    bgRender.sprite = sprite;
                    break;
            }
        }

        public void ClearBackground()
        {
            Debug.Log("清除背景");
            SpriteRenderer render = gameObject.GetComponent<SpriteRenderer>();
            render.sprite = null;
        }

        /// <summary>
        /// 获取当前背景的名称
        /// </summary>
        /// <returns></returns>
        public string GetBackgroundName()
        {
            return bgRender.sprite != null ? bgRender.sprite.name : string.Empty;
        }

        /// <summary>
        /// 设置当前背景快照
        /// </summary>
        /// <returns></returns>
        public BackgroundLayerSnapshot GetBackgroundSnapshot()
        {
            var snapshot = new BackgroundLayerSnapshot();
            snapshot.BackgroundName = GetBackgroundName();
            return snapshot;
        }

        /// <summary>
        /// 恢复背景快照
        /// </summary>
        /// <param name="backgroundLayerSnapshot"></param>
        /// <param name="dialogueManager"></param>
        public void RevertToBackgroundSnapshot(BackgroundLayerSnapshot backgroundLayerSnapshot, IDialogueManager dialogueManager)
        {
            if (backgroundLayerSnapshot.BackgroundName != string.Empty)
            {
                var bg = dialogueManager.GetBackgroundList().backgrounds.Find(x => x.BgSprite.name == backgroundLayerSnapshot.BackgroundName);
                if (bg != null)
                {
                    SetBackgroundSprite(bg.BgSprite, TransitionAnimType.None, 0);
                }
            }
        }
    }
}