﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.




using UnityEngine;

namespace Pluliter.Background
{
    /// <summary>
    /// 背景过度动画
    /// </summary>
    public enum TransitionAnimType
    {
        [InspectorName("无动画")]
        None,
        [InspectorName("淡入")]
        Fade
    }
}
