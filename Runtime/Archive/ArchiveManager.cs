﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Pluliter.Dialogue;
using System;
using System.IO;
using UnityEngine.UI;
using System.Security.Cryptography;


namespace Pluliter.Archive
{
    /// <summary>
    /// 存档管理器
    /// </summary>
    public class ArchiveManager : MonoBehaviour
    {
        [Header("存档设置")]
        public string ArchiveFileName = "archive.dat";
        public int ArchiveCount = 10;
        public GameObject ArchiveBoxPrefab;
        public ArchiveData archiveData;
        public GameObject ArchivePanel;
        public DialogueManager dialogueManager;
        public Button saveButton;
        public Button LoadButton;

        [Header("加密设置")]
        public EncryptionType encryptionType = EncryptionType.AES;
        private IArchiveEncryptor encryptor;

        private static ArchiveManager instance;
        private Action<Archive> ArchiveAction;

        /// <summary>
        /// 存档操作
        /// </summary>
        public enum ArchiveOperation 
        {
            /// <summary>
            /// 无操作
            /// </summary>
            None,
            /// <summary>
            /// 保存
            /// </summary>
            Save,
            /// <summary>
            /// 加载
            /// </summary>
            Load 
        }
        public ArchiveOperation operation = ArchiveOperation.None;


        public static ArchiveManager Instance
        {
            get
            {
#if UNITY_2023_1_OR_NEWER
                instance = FindFirstObjectByType<ArchiveManager>();
#else
                instance = FindObjectOfType<ArchiveManager>();
#endif
                if (instance == null)
                {
                    instance = new GameObject("ArchiveManager").AddComponent<ArchiveManager>();
                }
                return instance;
            }
        }

        private void Awake()
        {
            if (ArchiveCount < 1)
            {
                Debug.LogError("存档数量不能小于1");
                ArchiveCount = 1;
            }
            InitializeEncryptor();
            LoadArchives();
            dialogueManager.OnDialogueStart.AddListener(AutoSave);
        }

        private void Start()
        {
            saveButton.onClick.AddListener(() => ShowArchivePanel(ArchiveOperation.Save));
            LoadButton.onClick.AddListener(() => ShowArchivePanel(ArchiveOperation.Load));
            GenerateArchiveBoxes();
        }

        /// <summary>
        /// 初始化加密器
        /// </summary>
        private void InitializeEncryptor()
        {
            encryptor = EncryptorFactory.CreateEncryptor(encryptionType);
        }


        public void SaveArchives()
        {
            try
            {
                string json = JsonConvert.SerializeObject(archiveData.Archives,
                    Formatting.Indented,
                    new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

                string encrypted = encryptor.Encrypt(json);
                SafeFileWrite(Path.Combine(Application.persistentDataPath, ArchiveFileName), encrypted);
            }
            catch (CryptographicException ex)
            {
                Debug.LogError($"加密失败: {ex.Message}");
                FallbackSave();
            }
        }

        private void SafeFileWrite(string path, string content)
        {
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    File.WriteAllText(path, content);
                    return;
                }
                catch (IOException) when (i < 2)
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
            Debug.LogError("文件写入失败");
        }

        private void FallbackSave()
        {
            try
            {
                string json = JsonConvert.SerializeObject(archiveData.Archives);
                File.WriteAllText(GetArchivePath(), json);
                Debug.LogWarning("使用未加密应急保存成功");
            }
            catch (Exception ex)
            {
                Debug.LogError($"应急保存失败: {ex.Message}");
            }
        }

        public void LoadArchives()
        {
            string path = GetArchivePath();
            if (!File.Exists(path))
            {
                InitializeArchiveFile(path);
                return;
            }

            string encrypted = File.ReadAllText(path);
            try
            {
                string json = encryptor.Decrypt(encrypted);
                if (IsValidJson(json))
                {
                    archiveData.Archives = JsonConvert.DeserializeObject<List<Archive>>(json) ?? new();
                }
                else
                {
                    HandleLegacySave(encrypted);
                }
            }
            catch (CryptographicException ex)
            {
                Debug.LogError($"解密失败: {ex.Message}");
                HandleLegacySave(encrypted);
            }

            while (archiveData.Archives.Count < ArchiveCount)
            {
                archiveData.Archives.Add(new Archive());
            }
        }

        private void HandleLegacySave(string content)
        {
            try
            {
                archiveData.Archives = JsonConvert.DeserializeObject<List<Archive>>(content) ?? new();
                Debug.LogWarning("检测到未加密存档，已自动转换");
                SaveArchives(); // 重新加密保存
            }
            catch
            {
                Debug.LogError("存档文件损坏，初始化新存档");
                archiveData.Archives = new List<Archive>();
                CreateNullArchives();
            }
        }

        private bool IsValidJson(string str)
        {
            if (string.IsNullOrWhiteSpace(str)) return false;
            str = str.Trim();
            return (str.StartsWith("{") && str.EndsWith("}")) ||
                   (str.StartsWith("[") && str.EndsWith("]"));
        }

        public void ShowArchivePanel(ArchiveOperation op)
        {
            if (ArchivePanel.activeSelf) return;

            operation = op;
            ArchiveAction = op switch
            {
                ArchiveOperation.Save => SaveArchive,
                ArchiveOperation.Load => LoadArchive,
                _ => null
            };

            GenerateArchiveBoxes();
            ArchivePanel.SetActive(true);
        }

        public void GenerateArchiveBoxes()
        {
            foreach (Transform child in ArchivePanel.transform) Destroy(child.gameObject);

            for (int i = 0; i < archiveData.Archives.Count; i++)
            {
                var box = Instantiate(ArchiveBoxPrefab, ArchivePanel.transform).GetComponent<ArchiveBox>();
                box.index = i;
                box.ArchiveButtonClicked = () => ProcessArchive(box.index);

                var archive = archiveData.Archives[i];
                box.archiveIndexText.text = $"存档位{(i + 1):D2}";
                box.chapterName.text = archive.IsEmpty ? "空存档位" : archive.chapterName;
                box.saveTime.text = archive.IsEmpty ? "" : archive.saveTime;
            }
        }


        private string GetArchivePath() => Path.Combine(Application.persistentDataPath, ArchiveFileName);

        private void InitializeArchiveFile(string path)
        {
            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
                CreateNullArchives();
                SaveArchives();
            }
        }

        private void CreateNullArchives()
        {
            archiveData.Archives.Clear();
            for (int i = 0; i < ArchiveCount; i++)
            {
                archiveData.Archives.Add(new Archive());
            }
        }

        private void AutoSave() => SaveArchive(archiveData.Archives[0]);

        public void SaveArchive(Archive archive)
        {
            if (dialogueManager == null) return;

            archive.actorLayerSnapshot = dialogueManager.GetActorManager().GetActorLayerSnapshot();
            archive.backgroundLayerSnapshot = dialogueManager.GetBackgroundManager().GetBackgroundSnapshot();
            archive.blackboardSnapshot = dialogueManager.GetBlackboard().GetBlackboardSnapshot();
            archive.chapterIndex = dialogueManager.GetChapterIndex();
            archive.nodeIndex = dialogueManager.GetNodeIndex();
            archive.chapterName = dialogueManager.GetChapterName();
            archive.saveTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            SaveArchives();
            HideArchivePanel();
        }

        public void LoadArchive(Archive archive)
        {
            if (archive.IsEmpty) return;

            dialogueManager.GetActorManager().RevertToActorLayerSnapshot(archive.actorLayerSnapshot, dialogueManager);
            dialogueManager.GetBackgroundManager().RevertToBackgroundSnapshot(archive.backgroundLayerSnapshot, dialogueManager);
            dialogueManager.GetBlackboard().RevertToBlackboardSnapshot(archive.blackboardSnapshot);
            dialogueManager.SetChapterIndex(archive.chapterIndex);
            dialogueManager.SetNodeIndex(archive.nodeIndex);

            HideArchivePanel();
        }

        public void ProcessArchive(int index) => ArchiveAction?.Invoke(archiveData.Archives[index]);

        public void HideArchivePanel()
        {
            ArchivePanel.SetActive(false);
            operation = ArchiveOperation.None;
        }
    }
}