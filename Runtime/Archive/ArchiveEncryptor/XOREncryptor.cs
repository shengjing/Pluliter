﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using System;
using System.Text;

namespace Pluliter.Archive
{
    /// <summary>
    /// XOR加密器
    /// </summary>
    public class XOREncryptor : IArchiveEncryptor
    {
        private readonly string key;

        public XOREncryptor(string key)
        {
            this.key = key;
        }

        public string Encrypt(string plainText)
        {
            var data = Encoding.UTF8.GetBytes(plainText);
            var keyData = Encoding.UTF8.GetBytes(key);
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = (byte)(data[i] ^ keyData[i % keyData.Length]);
            }
            return Convert.ToBase64String(data);
        }

        public string Decrypt(string cipherText)
        {
            var data = Convert.FromBase64String(cipherText);
            var keyData = Encoding.UTF8.GetBytes(key);
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = (byte)(data[i] ^ keyData[i % keyData.Length]);
            }
            return Encoding.UTF8.GetString(data);
        }
    }
}
