﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


namespace Pluliter.Archive
{
    /// <summary>
    /// 加密类型
    /// </summary>
    public enum EncryptionType
    {
        /// <summary>
        /// 无加密
        /// </summary>
        None,
        /// <summary>
        /// AES加密，需要32字节密钥，性能速度较慢，安全性较高
        /// </summary>
        AES,
        /// <summary>
        /// XOR加密，需要16字节密钥，性能速度较快，安全性一般
        /// </summary>
        XOR,
        /// <summary>
        /// Base64编码，无需密钥，性能速度较快，安全性最低
        /// </summary>
        Base64
    }
}