﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


namespace Pluliter.Archive
{
    /// <summary>
    /// 加密工厂
    /// </summary>
    public class EncryptorFactory
    {
        private static readonly string AES_KEY = "Your32ByteAESKey1234567890!@#$%^&";
        private static readonly string XOR_KEY = "XorKey123";

        public static IArchiveEncryptor CreateEncryptor(EncryptionType type)
        {
            switch (type)
            {
                case EncryptionType.AES:
                    return new AESEncryptor(AES_KEY);
                case EncryptionType.XOR:
                    return new XOREncryptor(XOR_KEY);
                case EncryptionType.Base64:
                    return new Base64Encryptor();
                default:
                    return new NullEncryptor();
            }
        }
    }
}
