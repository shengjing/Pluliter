﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pluliter.Archive
{
    /// <summary>
    /// 存档位
    /// </summary>
    public class ArchiveBox : MonoBehaviour
    {
        public int index;

        public Text chapterName;

        public Text archiveIndexText;

        public Text saveTime;

        public Button archiveButton;

        public Action ArchiveButtonClicked;

        private void Start()
        {
            archiveButton = GetComponent<Button>();
            archiveButton.onClick.AddListener(OnArchiveButtonClicked);
        }

        private void OnArchiveButtonClicked()
        {
            //archiveManager.ProsessArchive(index);
            if (ArchiveButtonClicked != null)
            {
                ArchiveButtonClicked();
            }
        }
    }
}
