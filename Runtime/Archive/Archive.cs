﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


using System;
using Pluliter.Actor;
using Pluliter.Blackboard;


namespace Pluliter.Archive
{
    /// <summary>
    /// 存档
    /// </summary>
    [Serializable]
    public class Archive
    {
        /// <summary>
        /// 存档名
        /// </summary>
        public string chapterName;

        /// <summary>
        /// 存档时间
        /// </summary>
        public string saveTime;

        /// <summary>
        /// 章节索引
        /// </summary>
        public int chapterIndex;

        /// <summary>
        /// 节点索引
        /// </summary>
        public int nodeIndex;

        /// <summary>
        /// 演员层快照
        /// </summary>
        public ActorLayerSnapshot actorLayerSnapshot;

        /// <summary>
        /// 背景层快照
        /// </summary>
        public BackgroundLayerSnapshot backgroundLayerSnapshot;

        /// <summary>
        /// 黑板快照
        /// </summary>
        public BlackboardSnapshot blackboardSnapshot;

        public bool IsEmpty { get; internal set; }
    }
}