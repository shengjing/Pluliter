﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;

namespace Pluliter.Audio
{
    /// <summary>
    /// 背景音乐
    /// </summary>
    [System.Serializable]
    public class SoundEffect
    {
        /// <summary>
        /// 背景音乐ID
        /// </summary>
        public string SoundEffectId;

        /// <summary>
        /// 背景音乐音频片段
        /// </summary>
        public AudioClip SoundEffectClip;

        public SoundEffect(string bgmId)
        {
            SoundEffectId = bgmId;
        }
    }
}
