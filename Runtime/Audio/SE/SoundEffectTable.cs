﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;
using System.Collections.Generic;
using System;

namespace Pluliter.Audio
{
    /// <summary>
    /// 音效表
    /// </summary>
    [CreateAssetMenu(menuName = "多文Pluliter/音效表")]
    public class SoundEffectTable : ScriptableObject
    {
        public List<SoundEffect> SoundEffectList = new();

        /// <summary>
        /// 获取音效
        /// </summary>
        /// <param name="soundEffectId"></param>
        /// <returns></returns>
        public SoundEffect GetSoundEffectById(string soundEffectId)
        {
            return SoundEffectList.Find(bgm => bgm.SoundEffectId == soundEffectId);
        }

        /// <summary>
        /// 获取音效名称列表
        /// </summary>
        /// <returns></returns>
        public List<string> GetSoundEffectNames()
        {
            var list = new List<string>();
            foreach (var soundEffect in SoundEffectList)
            {
                list.Add(soundEffect.SoundEffectId);
            }
            return list;
        }
    }
}
