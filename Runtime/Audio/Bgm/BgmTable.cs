﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;
using System.Collections.Generic;
using System;

namespace Pluliter.Audio
{
    /// <summary>
    /// 背景音乐表
    /// </summary>
    [CreateAssetMenu(menuName = "多文Pluliter/背景音乐表")]
    public class BgmTable : ScriptableObject
    {
        public List<Bgm> bgmList = new();

        public Bgm GetBgmById(string bgmId)
        {
            return bgmList.Find(bgm => bgm.BgmId == bgmId);
        }

        /// <summary>
        /// 获取背景音乐名称列表
        /// </summary>
        /// <returns></returns>
        public List<string> GetBgmNames()
        {
            var list = new List<string>();
            foreach (var bgm in bgmList)
            {
                list.Add(bgm.BgmId);
            }
            return list;
        }
    }
}
