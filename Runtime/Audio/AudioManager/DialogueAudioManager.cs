﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;
using UnityEngine.Audio;

namespace Pluliter.Audio
{
    /// <summary>
    /// 对话音频管理器，负责管理对话中的音频播放，更复杂的逻辑建议自行实现
    /// </summary>
    public class DialogueAudioManager : MonoBehaviour
    {
        public static DialogueAudioManager Instance { get; private set; }

        public AudioSource bgmAudioSource;

        public AudioSource sfxAudioSource;

        public AudioSource voiceAudioSource;

        public AudioMixer mixer;

        void Awake()
        {
            Instance = this;

            if (mixer == null)
            {
                throw new System.Exception("AudioMixer丢失！");
            }
        }


        public void PlayBgm(AudioClip clip, bool loop)
        {
            if (bgmAudioSource.isPlaying)
            {
                bgmAudioSource.Stop();
            }
            bgmAudioSource.clip = clip;
            bgmAudioSource.loop = loop;
            bgmAudioSource.Play();

        }

        public void PlaySfx(AudioClip clip)
        {
            if (sfxAudioSource.isPlaying)
            {
                sfxAudioSource.Stop();
            }
            sfxAudioSource.clip = clip;
            sfxAudioSource.Play();
        }

        public void PlayVoice(AudioClip clip)
        {
            if (voiceAudioSource.isPlaying)
            {
                voiceAudioSource.Stop();
            }
            voiceAudioSource.clip = clip;
            voiceAudioSource.Play();
        }

        public void StopBgm()
        {
            if (bgmAudioSource.isPlaying)
            {
                bgmAudioSource.Stop();
            }
        }

        public void StopSfx()
        {
            if (sfxAudioSource.isPlaying)
            {
                sfxAudioSource.Stop();
            }
        }

        public void StopVoice()
        {
            if (voiceAudioSource.isPlaying)
            {
                voiceAudioSource.Stop();
            }
        }
    }

}