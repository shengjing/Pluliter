﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;
using UnityEngine;


/// <summary>
/// LWTween
/// </summary>
namespace Pluliter.LWTween
{
    /// <summary>
    /// LWTween动画类，用于创建和控制动画
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LWTween<T> : ILWTween
    {
        /// <summary>
        /// 动画开始时的值
        /// </summary>
        private T startValue;

        /// <summary>
        /// 动画结束时的值
        /// </summary>
        private T endValue;

        /// <summary>
        /// 动画持续时间
        /// </summary>
        private float duration;

        /// <summary>
        /// 插值更新回调函数
        /// </summary>
        private Action<T> onTweenUpdate;

        /// <summary>
        /// 当前经过的时间
        /// </summary>
        private float elapsedTime = 0f;

        /// <summary>
        /// 延迟经过时间
        /// </summary>
        private float delayElapsedTime = 0f;

        /// <summary>
        /// 目标循环次数
        /// </summary>
        private int targetLoopCount = 0;

        /// <summary>
        /// 是否反转（用于PingPong效果）
        /// </summary>
        private bool reverse = false;

        /// <summary>
        /// 是否PingPong循环
        /// </summary>
        private bool pingPongLoop = false;


        /// <summary>
        /// 当前循环次数，默认为1，表示只播放一次，如果设置为-1，则表示无限循环
        /// </summary>
        private int currentLoopCount = 1;

        /// <summary>
        /// 百分比阈值
        /// </summary>
        private float percentThreshold = -1f;

        /// <summary>
        /// 当每次更新完成时调用的回调函数
        /// </summary>
        private Action onUpdate;

        /// <summary>
        /// 当达到百分比阈值时调用的回调函数
        /// </summary>
        private Action onPercentCompleted;

        /// <summary>
        /// 变换类型
        /// </summary>
        private EaseType easeType = EaseType.Linear;

        /// <summary>
        /// Tween
        /// </summary>
        /// <param name="target"></param>
        /// <param name="identifier"></param>
        /// <param name="startValue"></param>
        /// <param name="endValue"></param>
        /// <param name="duration"></param>
        /// <param name="onTweenUpdate"></param>
        public LWTween(object target, string identifier, T startValue, T endValue, float duration, Action<T> onTweenUpdate)
        {
            TargetObject = target;
            TweenId = identifier;
            this.startValue = startValue;
            this.endValue = endValue;
            this.duration = duration;
            this.onTweenUpdate = onTweenUpdate;
            LWTweenManager.Instance.RegistTween(this);
        }

        /// <summary>
        /// Tween标识符
        /// </summary>
        public string TweenId { get; private set; }

        /// <summary>
        /// 执行Tween的目标
        /// </summary>
        public object TargetObject { get; private set; }

        /// <summary>
        /// Tween是否完成
        /// </summary>
        public bool IsComplete { get; private set; }

        /// <summary>
        /// Tween是否被杀死
        /// </summary>
        public bool IsKilled { get; private set; }

        /// <summary>
        /// Tween是否暂停
        /// </summary>
        public bool IsPaused { get; private set; }

        /// <summary>
        /// Tween是否忽略时间缩放
        /// </summary>
        public bool IgnoreTimeScale { get; private set; }


        /// <summary>
        /// 延迟时间
        /// </summary>
        public float DelayTime { get; private set; }

        /// <summary>
        /// 当Tween完成时调用的回调函数
        /// </summary>
        public Action onComplete { get; set; }

        /// <summary>
        /// 当Tween终止时调用的回调函数
        /// </summary>
        public Action onKill { get; set; }

        /// <summary>
        /// 当前值
        /// </summary>
        private T currentValue;

        /// <summary>
        /// 强制终止
        /// </summary>
        public void Kill()
        {
            OnCompleteKill();
            IsKilled = true;
            onComplete = null;
        }

        /// <summary>
        /// 目标是否被销毁
        /// </summary>
        /// <returns></returns>
        public bool CheckTargetDestroyed()
        {
            if (TargetObject is Delegate del && del == null)
            {
                return true;
            }
            
            if (TargetObject is UnityEngine.Object obj && obj == null)
            {
                return true;
            }
            
            return false;
        }

        /// <summary>
        /// 当Tween完成时终止
        /// </summary>
        public void OnCompleteKill()
        {
            IsComplete = true;
            onUpdate = null;
            onTweenUpdate = null;
            onPercentCompleted = null;
        }

        /// <summary>
        /// 暂停
        /// </summary>
        public void Pause()
        {
            IsPaused = true;
        }

        /// <summary>
        /// 继续
        /// </summary>
        public void Resume()
        {
            IsPaused = false;
        }

        /// <summary>
        /// 更新
        /// </summary>
        public void Update()
        {
            // 如果Tween被暂停，则不更新
            if (!IsPaused)
            {
                // 延迟时间
                if (IgnoreTimeScale)
                {
                    delayElapsedTime += Time.unscaledDeltaTime;
                }
                else
                {
                    delayElapsedTime += Time.deltaTime;
                }

                if (delayElapsedTime >= DelayTime)
                {
                    if (IsComplete)
                    {
                        return;
                    }
                    // 如果目标已经被销毁就销毁Tween
                    if (CheckTargetDestroyed())
                    {
                        Kill();
                        return;
                    }
                    // 更新时间
                    if (IgnoreTimeScale)
                    {
                        elapsedTime += Time.unscaledDeltaTime;
                    }
                    else
                    {
                        elapsedTime += Time.deltaTime;
                    }


                    // 进行缓动计算
                    float t = elapsedTime / duration;
                    float easedT = LWEase.Ease(easeType, t);

                    

                    // 如果需要反向循环
                    if (reverse)
                    {
                        currentValue = Interpolate(endValue, startValue, easedT);
                    }
                    else
                    {
                        currentValue = Interpolate(startValue, endValue, easedT);
                    }


                    // 执行更新回调函数
                    onUpdate?.Invoke();
                    // 执行插值更新回调函数
                    onTweenUpdate?.Invoke(currentValue);

                    // 执行百分比完成回调函数
                    if (percentThreshold >= 0f && t >= percentThreshold)
                    {
                        onPercentCompleted?.Invoke();
                        percentThreshold = -1f;
                    }

                    // 如果已经完成
                    if (elapsedTime >= duration)
                    {
                        if (!pingPongLoop)
                        {


                            //// 修复位置偏移
                            currentValue = endValue;
                            onTweenUpdate?.Invoke(currentValue);
                        }
                        targetLoopCount++;
                        elapsedTime = 0;

                        // 如果需要循环
                        if (pingPongLoop)
                        {
                            reverse = !reverse;
                        }

                        // 如果达到循环次数
                        if (currentLoopCount > 0 && targetLoopCount >= currentLoopCount)
                        {
                            OnCompleteKill();
                            IsComplete = true;
                        }
                    }
                }
            }
        }

        public T Interpolate(T start, T end, float t)
        {
            if (start is float startFloat && end is float endFloat)
            {
                return (T)(object)Mathf.LerpUnclamped(startFloat, endFloat, t);
            }
            else if (start is Vector3 startVector3 && end is Vector3 endVector3)
            {
                return (T)(object)Vector3.LerpUnclamped(startVector3, endVector3, t);
            }
            else if (start is Color startColor && end is Color endColor)
            {
                return (T)(object)Color.LerpUnclamped(startColor, endColor, t);
            }
            else
            {
                throw new NotImplementedException($"当前类型 {typeof(T)} 不支持插值");
            }
        }

        public LWTween<T> SetOnComplete(Action onComplete)
        {
            this.onComplete = onComplete;
            return this;
        }

        public LWTween<T> SetOnKill(Action onKill)
        {
            this.onKill = onKill;
            return this;
        }

        public LWTween<T> SetEase(EaseType easeType)
        {
            this.easeType = easeType;
            return this;
        }

        /// <summary>
        /// 设置乒乓球循环，到达结束值时反方向到达开始值循环播放
        /// </summary>
        /// <param name="loopCount">循环次数，为-1时无限循环</param>
        /// <returns></returns>
        public LWTween<T> SetPingPongLoop(int loopCount = 1)
        {
            this.currentLoopCount = loopCount;
            this.pingPongLoop = true;
            return this;
        }

        /// <summary>
        /// 设置简单循环，到达结束值时跳回开始值循环播放
        /// </summary>
        /// <param name="loopCount">循环次数，为-1时无限循环</param>
        /// <returns></returns>
        public LWTween<T> SetEasyLoop(int loopCount = 1)
        {
            this.currentLoopCount = loopCount;
            return this;
        }

        /// <summary>
        /// 设置忽略时间缩放
        /// </summary>
        /// <returns></returns>
        public LWTween<T> SetIgnoreTimeScale(bool ignoreTimeScale)
        {
            this.IgnoreTimeScale = ignoreTimeScale;
            return this;
        }


        public LWTween<T> SetOnUpdate(Action onUpdate)
        {
            this.onUpdate = onUpdate;
            return this;
        }

        public LWTween<T> SetOnPercentComplete(float percentComplete, Action onPercentComplete)
        {
            percentThreshold = Mathf.Clamp01(percentComplete);
            this.onPercentCompleted = onPercentComplete;
            return this;
        }

        public LWTween<T> SetStartDelay(float delayTime)
        {
            this.DelayTime = delayTime;
            return this;
        }



    }
}
