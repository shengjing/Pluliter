using UnityEngine;
using UnityEngine.UI;

namespace Pluliter.LWTween
{
    public static class ImageExtension
    {
        public static LWTween<float> FadeColor(this Image image, float targetAlpha, float duration)
        {
            string identifier = $"FadeColor_{image.GetInstanceID()}";
            return new LWTween<float>(image, identifier, image.color.a, targetAlpha, duration, value =>
            {
                image.color = new Color(image.color.r, image.color.g, image.color.b, value);
            });
        }
    }
}
