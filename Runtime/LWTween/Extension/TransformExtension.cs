﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;


namespace Pluliter.LWTween
{
    /// <summary>
    /// Transform组件的LWTween扩展方法
    /// </summary>
    public static class TransformExtension
    {
        public static LWTween<Vector3> MoveTo(this Transform transform, Vector3 target, float duration)
        {
            string identifier = $"MoveTo_{transform.GetInstanceID()}";
            return new LWTween<Vector3>(transform, identifier, transform.position, target, duration, value =>
            {
                transform.position = value;
            });
        }

        public static LWTween<Vector3> LocalMoveTo(this Transform transform, Vector3 target, float duration)
        {
            string identifier = $"LocalMoveTo_{transform.GetInstanceID()}";
            return new LWTween<Vector3>(transform, identifier, transform.localPosition, target, duration, value =>
            {
                transform.localPosition = value;
            });
        }

        public static LWTween<Vector3> ScaleTo(this Transform transform, Vector3 target, float duration)
        {
            string identifier = $"ScaleTo_{transform.GetInstanceID()}";
            return new LWTween<Vector3>(transform, identifier, transform.localScale, target, duration, value =>
            {
                transform.localScale = value;
            });
        }

        public static LWTween<Vector3> RotateTo(this Transform transform, Vector3 target, float duration)
        {
            string identifier = $"RotateTo_{transform.GetInstanceID()}";
            return new LWTween<Vector3>(transform, identifier, transform.eulerAngles, target, duration, value =>
            {
                transform.eulerAngles = value;
            });
        }
    }
}
