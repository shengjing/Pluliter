﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;

namespace Pluliter.LWTween
{
    public static class SpriteRendererExtension
    {
        public static LWTween<float> FadeColor(this SpriteRenderer spriteRenderer, float targetAlpha, float duration)
        {
            string identifier = $"FadeColor_{spriteRenderer.GetInstanceID()}";
            return new LWTween<float>(spriteRenderer, identifier, spriteRenderer.color.a, targetAlpha, duration, value =>
            {
                spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, value);
            });
        }
    }
}
