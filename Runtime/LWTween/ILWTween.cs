﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;

namespace Pluliter.LWTween
{
    /// <summary>
    /// Tween接口
    /// </summary>
    public interface ILWTween
    {
        void Update();

        void OnCompleteKill();

        void Kill();

        bool CheckTargetDestroyed();

        void Pause();

        void Resume();

        object TargetObject { get; }

        bool IsComplete { get; }

        bool IsKilled { get; }

        bool IsPaused { get; }

        bool IgnoreTimeScale { get; }

        string TweenId { get; }

        float DelayTime { get; }

        Action onComplete { get; set; }

        Action onKill { get; set; }

    }
}
