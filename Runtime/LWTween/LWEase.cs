﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;
using Mathf = UnityEngine.Mathf;

namespace Pluliter.LWTween
{
    #region EaseType
    /// <summary>
    /// 动画Ease类型
    /// </summary>
    public enum EaseType
    {
        /// <summary>
        /// 线性
        /// </summary>
        Linear,
        /// <summary>
        /// 缓入
        /// </summary>
        InSine,
        /// <summary>
        /// 缓出
        /// </summary>
        OutSine,
        /// <summary>
        /// 缓入缓出
        /// </summary>
        InOutSine,
        /// <summary>
        /// 平方缓入
        /// </summary>
        InQuad,
        /// <summary>
        /// 平方缓出
        /// </summary>
        OutQuad,
        /// <summary>
        /// 平方缓入缓出
        /// </summary>
        InOutQuad,
        /// <summary>
        /// 立方缓入
        /// </summary>
        InCubic,
        /// <summary>
        /// 立方缓出
        /// </summary>
        OutCubic,
        /// <summary>
        /// 立方缓入缓出
        /// </summary>
        InOutCubic,
        /// <summary>
        /// 四次方缓入
        /// </summary>
        InQuart,
        /// <summary>
        /// 四次方缓出
        /// </summary>
        OutQuart,
        /// <summary>
        /// 四次方缓入缓出
        /// </summary>
        InOutQuart,
        /// <summary>
        /// 五次方缓入
        /// </summary>
        InQuint,
        /// <summary>
        /// 五次方缓出
        /// </summary>
        OutQuint,
        /// <summary>
        /// 五次方缓入缓出
        /// </summary>
        InOutQuint,
        /// <summary>
        /// 指数缓入
        /// </summary>
        InExpo,
        /// <summary>
        /// 指数缓出
        /// </summary>
        OutExpo,
        /// <summary>
        /// 指数缓入缓出
        /// </summary>
        InOutExpo,
        /// <summary>
        /// 圆周率缓入
        /// </summary>
        InCirc,
        /// <summary>
        /// 圆周率缓出
        /// </summary>
        OutCirc,
        /// <summary>
        /// 圆周率缓入缓出
        /// </summary>
        InOutCirc,
        /// <summary>
        /// 回弹缓入
        /// </summary>
        InBack,
        /// <summary>
        /// 回弹缓出
        /// </summary>
        OutBack,
        /// <summary>
        /// 回弹缓入缓出
        /// </summary>
        InOutBack,
        /// <summary>
        /// 弹性缓入
        /// </summary>
        InElastic,
        /// <summary>
        /// 弹性缓出
        /// </summary>
        OutElastic,
        /// <summary>
        /// 弹性缓入缓出
        /// </summary>
        InOutElastic,
        /// <summary>
        /// 弹跳缓入
        /// </summary>
        InBounce,
        /// <summary>
        /// 弹跳缓出
        /// </summary>
        OutBounce,
        /// <summary>
        /// 弹跳缓入缓出
        /// </summary>
        InOutBounce
    }
    #endregion

    /// <summary>
    /// 动画Ease
    /// </summary>
    public static class LWEase
    {
        /// <summary>
        /// Ease函数
        /// </summary>
        /// <param name="easeType">Ease类型</param>
        /// <param name="t"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException">未知类型</exception>
        public static float Ease(EaseType easeType, float t)
        {
            switch (easeType)
            {
                case EaseType.Linear:
                    return Linear(t);
                case EaseType.InSine:
                    return InSine(t);
                case EaseType.OutSine:
                    return OutSine(t);
                case EaseType.InOutSine:
                    return InOutSine(t);
                case EaseType.InQuad:
                    return InQuad(t);
                case EaseType.OutQuad:
                    return OutQuad(t);
                case EaseType.InOutQuad:
                    return InOutQuad(t);
                case EaseType.InCubic:
                    return InCubic(t);
                case EaseType.OutCubic:
                    return OutCubic(t);
                case EaseType.InOutCubic:
                    return InOutCubic(t);
                case EaseType.InQuart:
                    return InQuart(t);
                case EaseType.OutQuart:
                    return OutQuart(t);
                case EaseType.InOutQuart:
                    return InOutQuart(t);
                case EaseType.InQuint:
                    return InQuint(t);
                case EaseType.OutQuint:
                    return OutQuint(t);
                case EaseType.InOutQuint:
                    return InOutQuint(t);
                case EaseType.InExpo:
                    return InExpo(t);
                case EaseType.OutExpo:
                    return OutExpo(t);
                case EaseType.InOutExpo:
                    return InOutExpo(t);
                case EaseType.InCirc:
                    return InCirc(t);
                case EaseType.OutCirc:
                    return OutCirc(t);
                case EaseType.InOutCirc:
                    return InOutCirc(t);
                case EaseType.InBack:
                    return InBack(t);
                case EaseType.OutBack:
                    return OutBack(t);
                case EaseType.InOutBack:
                    return InOutBack(t);
                case EaseType.InElastic:
                    return InElastic(t);
                case EaseType.OutElastic:
                    return OutElastic(t);
                case EaseType.InOutElastic:
                    return InOutElastic(t);
                case EaseType.InBounce:
                    return InBounce(t);
                case EaseType.OutBounce:
                    return OutBounce(t);
                case EaseType.InOutBounce:
                    return InOutBounce(t);
                default:
                    throw new System.NotImplementedException("未知类型");
            }
        }

        /// <summary>
        /// 线性
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float Linear(float x)
        {
            return x;
        }

        /// <summary>
        /// 缓入
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeInSine"/>
        /// <returns></returns>
        public static float InSine(float x)
        {
            return 1 - Mathf.Cos((x * Mathf.PI) / 2);
        }

        /// <summary>
        /// 缓出
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeOutSine"/>
        /// <returns></returns>
        public static float OutSine(float x)
        {
            return Mathf.Sin((x * Mathf.PI) / 2);
        }

        /// <summary>
        /// 缓入缓出
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeInOutSine"/>
        /// <returns></returns>
        public static float InOutSine(float x)
        {
            return -(Mathf.Cos(x * Mathf.PI) - 1) / 2;
        }

        /// <summary>
        /// 平方缓入
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeInQuad"/>
        /// <returns></returns>
        public static float InQuad(float x)
        {
            return x * x;
        }

        /// <summary>
        /// 平方缓出
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeOutQuad"/>
        /// <returns></returns>
        public static float OutQuad(float x)
        {
            return 1 - (1 - x) * (1 - x);
        }

        /// <summary>
        /// 平方缓入缓出
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeInOutQuad"/>
        /// <returns></returns>
        public static float InOutQuad(float x)
        {
            return x < 0.5 ? 2 * x * x : 1 - Mathf.Pow(-2 * x + 2, 2) / 2;
        }

        /// <summary>
        /// 立方缓入
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeInCubic"/>
        /// <returns></returns>
        public static float InCubic(float x)
        {
            return x * x * x;
        }

        /// <summary>
        /// 立方缓出
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeOutCubic"/>
        /// <returns></returns>
        public static float OutCubic(float x)
        {
            return 1 - Mathf.Pow(1 - x, 3);
        }

        /// <summary>
        /// 立方缓入缓出
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeInOutCubic"/>
        /// <returns></returns>
        public static float InOutCubic(float x)
        {
            return x < 0.5 ? 4 * x * x * x : 1 - Mathf.Pow(-2 * x + 2, 3) / 2;
        }

        /// <summary>
        /// 四次方缓入
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeInQuart"/>
        /// <returns></returns>
        public static float InQuart(float x)
        {
            return x * x * x * x;
        }

        /// <summary>
        /// 四次方缓出
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeOutQuart"/>
        /// <returns></returns>
        public static float OutQuart(float x)
        {
            return 1 - Mathf.Pow(1 - x, 4);
        }

        /// <summary>
        /// 四次方缓入缓出
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeInOutQuart"/>
        /// <returns></returns>
        public static float InOutQuart(float x)
        {
            return x < 0.5 ? 8 * x * x * x * x : 1 - Mathf.Pow(-2 * x + 2, 4) / 2;
        }

        /// <summary>
        /// 五次方缓入
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeInQuint"/>
        /// <returns></returns>
        public static float InQuint(float x)
        {
            return x * x * x * x * x;
        }

        /// <summary>
        /// 五次方缓出
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeOutQuint"/>
        /// <returns></returns>
        public static float OutQuint(float x)
        {
            return 1 - Mathf.Pow(1 - x, 5);
        }

        /// <summary>
        /// 五次方缓入缓出
        /// </summary>
        /// <param name="x"></param>
        /// <see cref="https://easings.net/zh-cn#easeInOutQuint"/>
        /// <returns></returns>
        public static float InOutQuint(float x)
        {
            return x < 0.5 ? 16 * x * x * x * x * x : 1 - Mathf.Pow(-2 * x + 2, 5) / 2;
        }

        /// <summary>
        /// 指数缓入
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float InExpo(float x)
        {
            return x == 0 ? 0 : Mathf.Pow(2, 10 * x - 10);
        }

        /// <summary>
        /// 指数缓出
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float OutExpo(float x)
        {
            return x == 1 ? 1 : 1 - Mathf.Pow(2, -10 * x);
        }

        /// <summary>
        /// 指数缓入缓出
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float InOutExpo(float x)
        {
            if (x == 0)
            {
                return 0;
            }
            else if (x == 1)
            {
                return 1;
            }
            else if (x < 0.5)
            {
                return Mathf.Pow(2, 20 * x - 10) / 2;
            }
            else
            {
                return (2 - Mathf.Pow(2, -20 * x + 10)) / 2;
            }
        }

        /// <summary>
        /// 圆周率缓入
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float InCirc(float x)
        {
            return 1 - Mathf.Sqrt(1 - Mathf.Pow(x, 2));
        }

        /// <summary>
        /// 圆周率缓出
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float OutCirc(float x)
        {
            return Mathf.Sqrt(1 - Mathf.Pow(x - 1, 2));
        }

        /// <summary>
        /// 圆周率缓入缓出
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float InOutCirc(float x)
        {
            if (x < 0.5)
            {
                return (1 - Mathf.Sqrt(1 - Mathf.Pow(2 * x, 2))) / 2;
            }
            else
            {
                return (Mathf.Sqrt(1 - Mathf.Pow(-2 * x + 2, 2)) + 1) / 2;
            }
        }

        /// <summary>
        /// 回弹缓入
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float InBack(float x)
        {
            const float c1 = 1.70158f;
            const float c2 = c1 + 1;

            return c2 * x * x * x - c1 * x * x;
        }

        /// <summary>
        /// 回弹缓出
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float OutBack(float x)
        {
            const float c1 = 1.70158f;
            const float c2 = c1 + 1;

            return 1 + c2 * Mathf.Pow(x - 1, 3) + c1 * Mathf.Pow(x - 1, 2);
        }

        /// <summary>
        /// 回弹缓入缓出
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float InOutBack(float x)
        {
            const float c1 = 1.70158f;
            const float c2 = c1 * 1.525f;

            if (x < 0.5)
            {
                return (Mathf.Pow(2 * x, 2) * ((c2 + 1) * 2 * x - c2)) / 2;
            }
            else
            {
                return (Mathf.Pow(2 * x - 2, 2) * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2;
            }
        }

        /// <summary>
        /// 弹性缓入
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float InElastic(float x)
        {
            const float c = (2 * Mathf.PI) / 3;

            if (x == 0)
            {
                return 0;
            }
            else if (x == 1)
            {
                return 1;
            }
            else
            {
                return -Mathf.Pow(2, 10 * x - 10) * Mathf.Sin((x * 10 - 10.75f) * c);
            }
        }

        /// <summary>
        /// 弹性缓出
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float OutElastic(float x)
        {
            const float c = (2 * Mathf.PI) / 3;

            if (x == 0)
            {
                return 0;
            }
            else if (x == 1)
            {
                return 1;
            }
            else if (x < 0.5)
            {
                return Mathf.Pow(2, -10 * x) * Mathf.Sin((x * 10 - 0.75f) * c) + 1;
            }
            else
            {
                return Mathf.Pow(2, -10 * x) * Mathf.Sin((x * 10 - 0.75f) * c) + 1;
            }
        }

        /// <summary>
        /// 弹性缓入缓出
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float InOutElastic(float x)
        {
            const float c = (2f * Mathf.PI) / 4.5f;

            if (x == 0)
            {
                return 0;
            }
            else if (x == 1)
            {
                return 1;
            }
            else if (x < 0.5)
            {
                return -(Mathf.Pow(2, 20 * x - 10) * Mathf.Sin((20 * x - 11.125f) * c)) / 2;
            }
            else
            {
                return (Mathf.Pow(2, -20 * x + 10) * Mathf.Sin((20 * x - 11.125f) * c)) / 2 + 1;
            }
        }

        /// <summary>
        /// 弹跳缓入
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float InBounce(float x)
        {
            return 1 - OutBounce(1 - x);
        }

        /// <summary>
        /// 弹跳缓出
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float OutBounce(float x)
        {
            const float n1 = 7.5625f;
            const float d1 = 2.75f;

            if (x < 1 / d1)
            {
                return n1 * x * x;
            }
            else if (x < 2 / d1)
            {
                return n1 * (x -= 1.5f / d1) * x + 0.75f;
            }
            else if (x < 2.5 / d1)
            {
                return n1 * (x -= 2.25f / d1) * x + 0.9375f;
            }
            else
            {
                return n1 * (x -= 2.625f / d1) * x + 0.984375f;
            }
        }

        /// <summary>
        /// 弹跳缓入缓出
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static float InOutBounce(float x)
        {
            if (x < 0.5)
            {
                return (1 - OutBounce(1 - 2 * x)) / 2;
            }
            else
            {
                return (1 + OutBounce(2 * x - 1)) / 2;
            }
        }
        

    }
}
