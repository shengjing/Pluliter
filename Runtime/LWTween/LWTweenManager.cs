﻿//Copyright 2024 ZhanleHall

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace Pluliter.LWTween
{
    /// <summary>
    /// Tween管理器
    /// </summary>
    public class LWTweenManager : MonoBehaviour
    {
        private static LWTweenManager instance;
        public static LWTweenManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindFirstObjectByType<LWTweenManager>();
                    if (instance == null)
                    {
                        GameObject go = new GameObject("LWTweenManager");
                        instance = go.AddComponent<LWTweenManager>();
                    }
                }
                return instance;
            }
        }

        private Dictionary<string, ILWTween> activeTweens = new Dictionary<string, ILWTween>();

        /// <summary>
        /// 注册Tween
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tween"></param>
        public void RegistTween<T>(LWTween<T> tween)
        {
            if (activeTweens.ContainsKey(tween.TweenId))
            {
                activeTweens[tween.TweenId].OnCompleteKill();
            }
            activeTweens[tween.TweenId] = tween;
        }

        /// <summary>
        /// 移除Tween
        /// </summary>
        /// <param name="tweenIdentifier"></param>
        public void RemoveTween(string tweenIdentifier)
        {
            if (activeTweens.ContainsKey(tweenIdentifier))
            {
                activeTweens.Remove(tweenIdentifier);
            }
        }

        private void Update()
        {
            foreach (var pair in activeTweens.ToList())
            {
                ILWTween tween = pair.Value;
                tween.Update();
                if (tween.IsComplete && !tween.IsKilled)
                {
                    if (tween.onComplete != null)
                    {
                        tween.onComplete.Invoke();
                        tween.onComplete = null;
                    }
                    if (tween.onKill != null)
                    {
                        tween.onKill.Invoke();
                        tween.onKill = null;
                    }
                    RemoveTween(pair.Key);
                }
                if (tween.IsKilled)
                {
                    if (tween.onKill != null)
                    {
                        tween.onKill.Invoke();
                        tween.onKill = null;
                    }
                    RemoveTween(pair.Key);
                }
            }
        }
    }
}
