﻿using Pluliter.Dialogue;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.UI;
using UnityEngine.Rendering.Universal;

namespace Pluliter
{
    /// <summary>
    /// 多文视觉小说管理器
    /// </summary>
    [RequireComponent(typeof(EventSystem))]
    [RequireComponent(typeof(InputSystemUIInputModule))]
    public class PluliterVisualNovelManager : MonoBehaviour
    {
        private Camera mainCamera;

        private Camera uiCamera;
        private Camera actCamera;

        private EventSystem eventSystem;
        private InputSystemUIInputModule inputSystemUIInputModule;

        private DialogueManager dialogueManager;

        private void Awake()
        {
            AddEventSystem();
            AddCameraStack();
            InstantiateDialogueManager();

        }

        private void AddEventSystem()
        {
            eventSystem = GetComponent<EventSystem>();
            if (eventSystem == null)
            {
                gameObject.AddComponent<EventSystem>();
                eventSystem = GetComponent<EventSystem>();
            }
            inputSystemUIInputModule = GetComponent<InputSystemUIInputModule>();
            if (inputSystemUIInputModule == null)
            {
                gameObject.AddComponent<InputSystemUIInputModule>();
                inputSystemUIInputModule = GetComponent<InputSystemUIInputModule>();
            }
        }

        private void AddCameraStack()
        {
            // 在子物体中找到
            uiCamera = transform.Find("UICamera").GetComponent<Camera>();
            actCamera = transform.Find("ActCamera").GetComponent<Camera>();
            if (actCamera == null)
            {
                Debug.LogError("找不到ActCamera，请检查场景中的PluliterManager");
            }
            if (uiCamera == null)
            {
                Debug.LogError("找不到UICamera，请检查场景中的PluliterManager");
            }

            mainCamera = Camera.main;
            if (mainCamera == null)
            {
                throw new System.NullReferenceException("找不到主摄像机，请检查场景中的PluliterManager");
            }
            // 添加堆叠
            if (!mainCamera.gameObject.GetComponent<UniversalAdditionalCameraData>().cameraStack.Contains(actCamera))
            {
                mainCamera.gameObject.GetComponent<UniversalAdditionalCameraData>().cameraStack.Add(actCamera);
            }
            if (!mainCamera.gameObject.GetComponent<UniversalAdditionalCameraData>().cameraStack.Contains(uiCamera))
            {
                mainCamera.gameObject.GetComponent<UniversalAdditionalCameraData>().cameraStack.Add(uiCamera);
            }
        }

        private void InstantiateDialogueManager()
        {
            dialogueManager = DialogueManager.Instance;
            dialogueManager.InitDialogueManager();
            dialogueManager.StartDialogue();
        }


        private void OnDestroy()
        {
            if (mainCamera != null)
            {
                mainCamera.gameObject.GetComponent<UniversalAdditionalCameraData>().cameraStack.Remove(uiCamera);
                mainCamera.gameObject.GetComponent<UniversalAdditionalCameraData>().cameraStack.Remove(actCamera);
            }
        }
    }
}
