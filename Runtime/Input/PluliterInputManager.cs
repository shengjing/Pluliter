﻿//Copyright 2025 ZhanleHall
//Copyright 2025 DSOE1024

//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using UnityEngine;
using UnityEngine.EventSystems;

namespace Pluliter.Input
{
    /// <summary>
    /// 输入管理器
    /// </summary>
    public class PluliterInputManager : MonoBehaviour
    {
        private bool isClickHandled;
        private Vector2 clickPosition;

        public event System.Action<Vector2> OnClickEvent;

        [Header("输入设置")]
        [SerializeField] private bool allowMultiTouch = false;


        private void Update()
        {
            isClickHandled = false;
            clickPosition = Vector2.zero;

            HandleTouchInput();
            if (!isClickHandled) HandleMouseInput();

            // 触发事件
            if (isClickHandled)
            {
                OnClickEvent?.Invoke(clickPosition);
            }
        }

        private void HandleTouchInput()
        {
            if (!Application.isMobilePlatform) return;

            for (int i = 0; i < UnityEngine.Input.touchCount; i++)
            {
                Touch touch = UnityEngine.Input.GetTouch(i);

                if (!allowMultiTouch && i > 0) break;

                if (touch.phase == TouchPhase.Began && !IsPointerOverUI(touch.fingerId))
                {
                    isClickHandled = true;
                    clickPosition = touch.position;
                    return;
                }
            }
        }

        private void HandleMouseInput()
        {
            if (!UnityEngine.Input.GetMouseButtonDown(0)) return;

            if (!IsPointerOverUI())
            {
                isClickHandled = true;
                clickPosition = UnityEngine.Input.mousePosition;
            }
        }

        public bool TryGetClick(out Vector2 screenPosition)
        {
            screenPosition = clickPosition;
            return isClickHandled;
        }

        private bool IsPointerOverUI(int pointerId = -1)
        {
            if (EventSystem.current == null) return false;

            if (Application.isMobilePlatform)
            {
                return EventSystem.current.IsPointerOverGameObject(pointerId);
            }
            else
            {
                return EventSystem.current.IsPointerOverGameObject();
            }
        }


        //private bool IsPointerOverUI()
        //{
        //    PointerEventData eventData = new PointerEventData(EventSystem.current);
        //    eventData.position = Application.isMobilePlatform && Input.touchCount > 0 
        //        ? Input.GetTouch(0).position 
        //        : (Vector2)Input.mousePosition;

        //    System.Collections.Generic.List<RaycastResult> results = new System.Collections.Generic.List<RaycastResult>();
        //    EventSystem.current.RaycastAll(eventData, results);

        //    return results.Count > 0;
        //}
    }
}